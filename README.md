# Moviebase (in development) #

Moviebase is an ongoing personal project used to test concepts and new technologies.  The current project is a rewrite of the site [www.j3ko.com](http://www.j3ko.com) which was written as a pure ASP.NET MVC project.  It was initially started as a way to experiment with .NET MVC beta and has since evolved to what it is today.  Although the site itself is only 4 pages, it covers many common concerns a site of any size would encounter: security, scalability, concurrency, etc.

### Requirements ###

* Visual Studio 2013+
* SQL Server 2012+
* [TypeScript 1.4](https://visualstudiogallery.msdn.microsoft.com/2d42d8dc-e085-45eb-a30b-3f7d50d55304)
* [Web Essentials 2013](https://visualstudiogallery.msdn.microsoft.com/56633663-6799-41d7-9df7-0f2a504ca361)
* Optional: [MongoDB](https://www.mongodb.org/)
* Optional: [Node.js Tools for Visual Studio](https://nodejstools.codeplex.com/)

### Project breakdown ###

* **J3ko.Common** - A utility library of useful extension methods
* **J3ko.Common.Algorithm** - A utility library of useful data structures and algorithms
* **J3ko.Infrastructure** - Base infrastructure library for CQS/CQRS based system
* **J3ko.Infrastructure.EntityFramework** - Entity Framework specific infrastructure concerns
* **J3ko.Infrastructure.Mock** - [Effort](https://effort.codeplex.com/) based mocking library for EF
* **J3ko.Infrastructure.MongoDB** - MongoDB specific infrastructure concerns
* **J3ko.Infrastructure.MVC** - .NET MVC specific infrastructure concerns
* **J3ko.Infrastructure.SignalR** - SignalR specific infrastructure concerns
* **J3ko.Infrastrucure.SocketIO** - SocketIO specific infrastructure concerns
* **J3ko.Moviebase.API** - Moviebase SignalR/odata enabled WebAPI backend secured by Identity2 token authentication
* **J3ko.Moviebase.Client** - Moviebase Typescript/Angular Material/AngularJS based frontend
* **J3ko.Moviebase.CQL** - "Command Query Layer" a CQS/CQRS based logic layer for the backend API
* **J3ko.Moviebase.DAL** - Data Access Layer
* **J3ko.Moviebase.DBP** - Database Project
* **J3ko.Moviebase.Domain** - Domain object layer
* **J3ko.Moviebase.Test** - Unit Test project
* **J3ko.Web.Movie** - Wrapper library for external web API calls
* **J3ko.Moviebase.Node** - Experiment with NodeJS/AngularJS/Express frontend (deprecated)

### Environment Setup ###

* Coming Soon
* For convenince database backup can be found in the [downloads](https://bitbucket.org/j3ko/moviebase/downloads) section