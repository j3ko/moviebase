﻿using J3ko.Infrastructure.DAL;
using J3ko.Infrastructure.DAL.MongoDB;
using J3ko.Moviebase.DAL.MongoDB;
using J3ko.Moviebase.Domain;
using J3ko.Moviebase.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL
{
    public class MovieViewContext : MongoDatabaseContext, IMovieViewContext
    {
        public MovieViewContext(string connectionStringOrName)
            : base(connectionStringOrName, typeof(TitleDtoConfiguration))
        {

        }

        public IRepository<TitleDto> Titles { get { return new MongoRepository<TitleDto>(this); } }
    }
}
