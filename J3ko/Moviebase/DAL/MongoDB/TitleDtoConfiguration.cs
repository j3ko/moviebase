﻿using J3ko.Infrastructure.DAL.MongoDB;
using J3ko.Moviebase.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL.MongoDB
{
    public class TitleDtoConfiguration : MongoTypeConfiguration<TitleDto>
    {
        public TitleDtoConfiguration()
        {
            MapIdProperty(x => x.TitleId);
            AutoMap();
        }
    }
}
