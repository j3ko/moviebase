﻿using J3ko.Infrastructure.Domain;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL
{
    public class MoviebaseSecurityContext : IdentityDbContext, ISecurityContext
    {
        public MoviebaseSecurityContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        { }

        public string Username
        {
            get { return Thread.CurrentPrincipal.Identity.Name; }
        }
    }
}
