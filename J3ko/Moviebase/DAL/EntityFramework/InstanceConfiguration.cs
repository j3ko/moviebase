﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL.EntityFramework
{
    public class InstanceConfiguration : EfTypeConfiguration<Instance>
    {
        public InstanceConfiguration()
        {
            HasKey(x => x.InstanceId);
            Property(x => x.InstanceId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
        }
    }
}
