﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL.EntityFramework
{
    public class GenreTitleConfiguration : EfTypeConfiguration<GenreTitle>
    {
        public GenreTitleConfiguration()
        {
            HasKey(x => new
            {
                x.GenreId,
                x.TitleId
            });
        }
    }
}
