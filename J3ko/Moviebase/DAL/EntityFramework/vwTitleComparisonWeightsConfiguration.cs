﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL.EntityFramework
{
    public class vwTitleComparisonWeightsConfiguration: EfTypeConfiguration<vwTitleComparisonWeights>
    {
        public vwTitleComparisonWeightsConfiguration()
        {
            HasKey(x => x.TitleId);
        }
    }
}
