﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL.EntityFramework
{
    public class DocumentConfiguration : EfTypeConfiguration<Document>
    {
        public DocumentConfiguration()
        {
            HasKey(x => x.StreamId);
            Property(x => x.StreamId)
                .HasColumnName("stream_id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            Property(x => x.FileStream)
                .HasColumnName("file_stream")
                .IsOptional();

            Property(x => x.Name)
                .HasColumnName("name")
                .HasMaxLength(255)
                .IsRequired();

        }
    }
}
