﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace J3ko.Moviebase.DAL.EntityFramework
{
    public class TitleConfiguration : EfTypeConfiguration<Title>
    {
        public TitleConfiguration()
        {
            HasKey(x => x.TitleId);
            Property(x => x.TitleId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            //HasMany(x => x.Genres)
            //    .WithMany()
            //    .Map(x =>
            //    {
            //        x.ToTable("GenreTitle");
            //        x.MapLeftKey("TitleId");
            //        x.MapRightKey("GenreId");
            //    });

            HasOptional(x => x.Cover)
                .WithMany()
                .HasForeignKey(x => x.CoverId);

            HasOptional(x => x.SmallCover)
                .WithMany()
                .HasForeignKey(x => x.SmallCoverId);

            HasMany(x => x.TitleSuggestions)
                .WithRequired()
                .HasForeignKey(x => x.TitleId);

        }
    }
}
