﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL.EntityFramework
{
    public class ContainerConfiguration : EfTypeConfiguration<Container>
    {
        public ContainerConfiguration()
        {
            HasKey(x => x.ContainerId);
            Property(x => x.ContainerId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();
        }
    }
}
