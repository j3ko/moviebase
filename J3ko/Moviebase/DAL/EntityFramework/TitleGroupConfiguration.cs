﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL.EntityFramework
{
    public class TitleGroupConfiguration : EfTypeConfiguration<TitleGroup>
    {
        public TitleGroupConfiguration()
        {
            HasKey(x => x.TitleGroupId);
            Property(x => x.TitleGroupId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

        }
    }
}
