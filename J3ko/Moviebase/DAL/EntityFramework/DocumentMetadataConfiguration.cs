﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL.EntityFramework
{
    public class DocumentMetadataConfiguration : EfTypeConfiguration<DocumentMetadata>
    {
        public DocumentMetadataConfiguration()
        {
            HasKey(x => x.StreamId);
            Property(x => x.StreamId)
                .IsRequired();

            HasRequired(x => x.Document)
                .WithOptional(x => x.DocumentMetadata);                
        }
    }
}
