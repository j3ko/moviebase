﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL.EntityFramework
{
    public class TitleSuggestionConfiguration : EfTypeConfiguration<TitleSuggestion>
    {
        public TitleSuggestionConfiguration()
        {
            HasKey(x => new
            {
                x.TitleId,
                x.SuggestionId
            });

            HasRequired(x => x.Title)
                .WithMany()
                .HasForeignKey(x => x.TitleId);

            HasRequired(x => x.Suggestion)
                .WithMany()
                .HasForeignKey(x => x.SuggestionId);
        }
    }
}
