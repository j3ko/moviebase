﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL.EntityFramework
{
    public class PersonConfiguration : EfTypeConfiguration<Person>
    {
        public PersonConfiguration()
        {
            HasKey(x => x.PersonId);
            Property(x => x.PersonId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
                .IsRequired();

            HasOptional(x => x.Thumbnail)
                .WithMany()
                .HasForeignKey(x => x.ThumbnailId);

        }
    }
}
