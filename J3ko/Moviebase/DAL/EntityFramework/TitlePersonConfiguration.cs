﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.DAL.EntityFramework
{
    public class TitlePersonConfiguration : EfTypeConfiguration<TitlePerson>
    {
        public TitlePersonConfiguration()
        {
            HasKey(x => new
            {
                x.TitleId,
                x.PersonId,
                x.Ordinal
            });

            HasRequired(x => x.TitleRole)
                .WithMany()
                .HasForeignKey(x => x.TitleRoleId);

        }
    }
}
