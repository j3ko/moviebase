﻿using J3ko.Infrastructure.DAL;
using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Infrastructure.Domain;
using J3ko.Moviebase.DAL.EntityFramework;
using J3ko.Moviebase.Domain;

namespace J3ko.Moviebase.DAL
{
    public class MoviebaseContext : EfDatabaseContext, IMoviebaseContext
    {
        public MoviebaseContext(string nameOrConnectionString, ISecurityContext security)
            : base(nameOrConnectionString, security, typeof(TitleConfiguration))
        {
            this.Database.CommandTimeout = 30000;
        }

        public IRepository<Title> Titles { get { return new EfRepository<Title>(this); } }
        public IRepository<Category> Categories { get { return new EfRepository<Category>(this); } }
        public IRepository<Genre> Genres { get { return new EfRepository<Genre>(this); } }
        public IRepository<TitleGroup> TitleGroups { get { return new EfRepository<TitleGroup>(this); } }
        public IRepository<TitleRole> TitleRoles { get { return new EfRepository<TitleRole>(this); } }
        public IRepository<Episode> Episodes { get { return new EfRepository<Episode>(this); } }
        public IRepository<TitlePerson> TitlePeople { get { return new EfRepository<TitlePerson>(this); } }
        public IRepository<Person> People { get { return new EfRepository<Person>(this); } }
        public IRepository<Document> Documents { get { return new EfRepository<Document>(this); } }
        public IRepository<DocumentMetadata> DocumentMetadata { get { return new EfRepository<DocumentMetadata>(this); } }
        public IRepository<GenreTmdb> GenreTmdbs { get { return new EfRepository<GenreTmdb>(this); } }
        public IRepository<GenreTitle> GenreTitles { get { return new EfRepository<GenreTitle>(this); } }
        public IRepository<Media> Media { get { return new EfRepository<Media>(this); } }
        public IRepository<Slot> Slots { get { return new EfRepository<Slot>(this); } }
        public IRepository<Container> Containers { get { return new EfRepository<Container>(this); } }
        public IRepository<Instance> Instances { get { return new EfRepository<Instance>(this); } }
        public IRepository<TitleSuggestion> TitleSuggestions { get { return new EfRepository<TitleSuggestion>(this); } }

        public IRepository<vwTitleComparisonWeights> vwTitleComparisonWeights { get { return new EfRepository<vwTitleComparisonWeights>(this); } }
    }
}
