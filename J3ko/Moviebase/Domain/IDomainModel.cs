﻿using J3ko.Infrastructure.CQL;
using J3ko.Infrastructure.Domain;
using J3ko.Web.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public interface IDomainModel : IDisposable
    {
        string HostRoot { get; }
        string ImageVirtualPath { get; }
        string ImageMirrorPath { get; }
        IWebMovieContext WebMovieContext { get; }
        IMoviebaseContext MoviebaseContext { get; }
        ISecurityContext SecurityContext { get; }
        ICommandContext<IDomainModel> CommandContext { get; }
        IMovieViewContext MovieViewContext { get; }
    }
}
