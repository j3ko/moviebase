﻿using J3ko.Infrastructure.DAL;
using J3ko.Moviebase.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public interface IMovieViewContext : IDbContext, IDisposable
    {
        IRepository<TitleDto> Titles { get; }
    }
}
