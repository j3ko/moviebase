﻿using J3ko.Infrastructure.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public interface IMoviebaseContext : IDbContext, IDisposable
    {
        IRepository<Title> Titles { get; }
        IRepository<Category> Categories { get; }
        IRepository<Genre> Genres { get; }
        IRepository<TitleGroup> TitleGroups { get; }
        IRepository<TitleRole> TitleRoles { get; }
        IRepository<Episode> Episodes { get; }
        IRepository<TitlePerson> TitlePeople { get; }
        IRepository<Person> People { get; }
        IRepository<Document> Documents { get; }
        IRepository<DocumentMetadata> DocumentMetadata { get; }
        IRepository<GenreTmdb> GenreTmdbs { get; }
        IRepository<GenreTitle> GenreTitles { get; }
        IRepository<Media> Media { get; }
        IRepository<Slot> Slots { get; }
        IRepository<Container> Containers { get; }
        IRepository<Instance> Instances { get; }
        IRepository<TitleSuggestion> TitleSuggestions { get; }

        IRepository<vwTitleComparisonWeights> vwTitleComparisonWeights { get; }
    }
}
