﻿using J3ko.Infrastructure.CQL;
using J3ko.Infrastructure.Domain;
using J3ko.Web.Movie;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class DomainModel : IDomainModel
    {
        private IMoviebaseContext _moviebaseContext;
        private ISecurityContext _securityContext;
        private ICommandContext<IDomainModel> _commandContext;
        private IWebMovieContext _webMovieContext;
        private IMovieViewContext _movieviewContext;
        private bool _disposed;

        public DomainModel(
            ISecurityContext securityContext, 
            IMoviebaseContext moviebaseContext, 
            IWebMovieContext webMovieContext,
            IMovieViewContext movieViewContext,
            params Func<ICommandListener>[] listeners)
        {
            _securityContext = securityContext;
            _moviebaseContext = moviebaseContext;
            _webMovieContext = webMovieContext;
            _movieviewContext = movieViewContext;
            HostRoot = ConfigurationManager.AppSettings["TempHostRoot"];
            ImageVirtualPath = ConfigurationManager.AppSettings["ImageVirtualPath"];
            ImageMirrorPath = ConfigurationManager.AppSettings["ImageMirrorPath"];

            _commandContext = new CommandContext<IDomainModel>(this);

            ConfigureCommandListeners(_commandContext, listeners);
        }


        private void ConfigureCommandListeners(ICommandContext<IDomainModel> commandContext, params Func<ICommandListener>[] listeners)
        {
            if (listeners != null)
            {
                foreach (var l in listeners)
                {
                    commandContext.Subscribe(l());
                }
            }
        }

        public string HostRoot { get; private set; }
        public string ImageVirtualPath { get; private set; }
        public string ImageMirrorPath { get; private set; }
        public ISecurityContext SecurityContext { get { return _securityContext; } }
        public ICommandContext<IDomainModel> CommandContext { get { return _commandContext; } }
        public IMoviebaseContext MoviebaseContext { get { return _moviebaseContext; } }
        public IWebMovieContext WebMovieContext { get { return _webMovieContext; } }
        public IMovieViewContext MovieViewContext { get { return _movieviewContext;  } }

        #region IDisposable Members
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (this._moviebaseContext != null)
                        this._moviebaseContext.Dispose();
                    if (this._commandContext != null)
                        this._commandContext.Dispose();
                    if (this._securityContext != null)
                        this._securityContext.Dispose();
                    if (this._movieviewContext != null)
                        this._movieviewContext.Dispose();
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                _disposed = true;

            }
        }
    }
}
