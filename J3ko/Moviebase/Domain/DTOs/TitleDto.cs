﻿using J3ko.Infrastructure.CQL;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain.DTOs
{
    public class TitleDto : IDto
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public int CategoryId { get; set; }
        public string Plot { get; set; }
        public string CoverUrl { get; set; }
        public string SmallCoverUrl { get; set; }
        public string ImdbId { get; set; }
        public int Released { get; set; }
        public DateTime? DateAdded { get; set; }
        public IEnumerable<TitleGroupDto> TitleGroups { get; set; }
        public IEnumerable<GenreDto> Genres { get; set; }
        public IEnumerable<TitlePersonDto> TitlePeople { get; set; }
        public IEnumerable<string> AltTitleNames { get; set; }
    }
}
