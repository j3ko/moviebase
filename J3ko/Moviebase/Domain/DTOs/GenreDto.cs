﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain.DTOs
{
    public class GenreDto : IDto
    {
        public int GenreId { get; set; }
        public string GenreName { get; set; }
        public bool IsUsed { get; set; }
    }
}
