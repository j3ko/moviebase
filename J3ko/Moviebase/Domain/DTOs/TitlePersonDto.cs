﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain.DTOs
{
    public class TitlePersonDto : IDto
    {
        public int PersonId { get; set; }
        public string FullName { get; set; }
        public int Ordinal { get; set; }
        public string CharacterName { get; set; }
        public int TitleRoleId { get; set; }
        public string ThumbnailUrl { get; set; }
    }
}
