﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain.DTOs
{
    public class TitleGroupDto : IDto
    {
        public int TitleGroupId { get; set; }
        public string Description { get; set; }
        public int MediaId { get; set; }
        public int SlotId { get; set; }
        public string SlotLabel { get; set; }
        public int ContainerId { get; set; }
        public string ContainerName { get; set; }
        public IEnumerable<EpisodeDto> Episodes { get; set; }
    }
}
