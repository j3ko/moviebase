﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain.DTOs
{
    public class ApplicationMetadataDto
    {
        public string TitleImagePath { get; set; }
    }
}
