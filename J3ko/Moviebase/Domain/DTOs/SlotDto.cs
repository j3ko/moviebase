﻿using J3ko.Infrastructure.CQL;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain.DTOs
{
    public class SlotDto : IDto
    {
        public int SlotId { get; set; }
        public string SlotLabel { get; set; }
        public int ContainerId { get; set; }
        public bool IsUsed { get; set; }
    }
}
