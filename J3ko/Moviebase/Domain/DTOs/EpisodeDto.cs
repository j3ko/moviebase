﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain.DTOs
{
    public class EpisodeDto : IDto
    {
        public int InstanceId { get; set; }
        public string InstanceName { get; set; }
        public string EpisodeName { get; set; }
    }
}
