﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class GenreTmdb
    {
        public int GenreId { get; set; }
        public int TmdbId { get; set; }
    }
}
