﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class Slot
    {
        public int SlotId { get; set; }
        public string SlotLabel { get; set; }
        public int ContainerId { get; set; }
        public virtual Container Container { get; set; }
    }
}
