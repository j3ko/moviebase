﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class Instance
    {
        public int InstanceId { get; set; }
        public string InstanceName { get; set; }
    }
}
