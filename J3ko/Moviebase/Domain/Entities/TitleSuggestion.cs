﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class TitleSuggestion
    {
        public int TitleId { get; set; }
        public virtual Title Title { get; set; }
        public int SuggestionId { get; set; }
        public virtual Title Suggestion { get; set; }
        public double Distance { get; set; }
    }
}
