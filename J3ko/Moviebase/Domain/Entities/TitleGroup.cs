﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class TitleGroup : IModifiable, IConcurrencyControllable
    {
        public TitleGroup()
        {
            Episodes = new List<Episode>();
        }
        public int TitleGroupId { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime DateBurned { get; set; }
        public int MediaId { get; set; }
        public virtual Media Media { get; set; }
        public int SlotId { get; set; }
        public virtual Slot Slot { get; set; }
        public int TitleId { get; set; }
        public virtual Title Title { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] RowVersion { get; set; }
        public ICollection<Episode> Episodes { get; set; }
    }
}
