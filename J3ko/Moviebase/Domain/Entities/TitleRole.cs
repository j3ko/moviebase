﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class TitleRole
    {
        public int TitleRoleId { get; set; }
        public string Description { get; set; }
        public static int Other { get { return 1; } }
    }
}
