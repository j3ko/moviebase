﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class Title : IModifiable, IConcurrencyControllable
    {
        public Title()
        {
            Episodes = new List<Episode>();
            TitleGroups = new List<TitleGroup>();
            GenreTitles = new List<GenreTitle>();
            AltTitleNames = new List<AltTitleName>();
            TitlePeople = new List<TitlePerson>();
            TitleSuggestions = new List<TitleSuggestion>();
        }

        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }
        public string Plot { get; set; }
        public Guid? CoverId { get; set; }
        public virtual Document Cover { get; set; }
        public Guid? SmallCoverId { get; set; }
        public virtual Document SmallCover { get; set; }
        public string ImdbId { get; set; }
        public DateTime? Synced { get; set; }
        public DateTime Released { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] RowVersion { get; set; }
        public virtual ICollection<Episode> Episodes { get; set; }
        public virtual ICollection<TitleGroup> TitleGroups { get; set; }
        public virtual ICollection<GenreTitle> GenreTitles { get; set; }
        public virtual ICollection<AltTitleName> AltTitleNames { get; set; }
        public virtual ICollection<TitlePerson> TitlePeople { get; set; }
        public virtual ICollection<TitleSuggestion> TitleSuggestions { get; set; }

        public bool UsesCoverFrom(string url)
        {
            return Cover != null && 
                Cover.DocumentMetadata != null && 
                Cover.DocumentMetadata.Source == url;
        }

        public bool UsesSmallCoverFrom(string url)
        {
            return SmallCover != null && 
                SmallCover.DocumentMetadata != null && 
                SmallCover.DocumentMetadata.Source == url;
        }

        public void Initialize(IDomainModel domainModel)
        {
            Container container = domainModel.MoviebaseContext.Containers.GetAll().FirstOrDefault() ?? new Container();
            Slot firstSlot = container.AvailableSlots(domainModel).FirstOrDefault() ?? new Slot();
            Media media = domainModel.MoviebaseContext.Media.GetAll().FirstOrDefault() ?? new Media();
            Category category = domainModel.MoviebaseContext.Categories.GetAll().FirstOrDefault() ?? new Category();

            Category = category;
            CategoryId = category.CategoryId;

            TitleGroups.Add(new TitleGroup()
            {
                Description = "Default",
                Slot = firstSlot,
                SlotId = firstSlot.SlotId,
                Media = media,
                MediaId = media.MediaId,
                Episodes = new List<Episode> {
                    new Episode {
                        InstanceId = 1,
                        EpisodeName = "hello"
                    }
                }
            });
        }
    }
}
