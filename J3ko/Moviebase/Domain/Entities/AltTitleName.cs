﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class AltTitleName : IConcurrencyControllable
    {
        public int TitleId { get; set; }
        public string Name { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
