﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class Person : IModifiable, IConcurrencyControllable
    {
        public int PersonId { get; set; }
        public string FullName { get; set; }
        public Guid? ThumbnailId { get; set; }
        public Document Thumbnail { get; set; }
        public int TmdbId { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] RowVersion { get; set; }

        public bool UsesThumbnailFrom(string url)
        {
            return Thumbnail != null &&
                Thumbnail.DocumentMetadata != null &&
                Thumbnail.DocumentMetadata.Source == url;
        }
    }
}
