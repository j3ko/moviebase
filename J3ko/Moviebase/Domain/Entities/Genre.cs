﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class Genre
    {
        public Genre()
        {
            TmdbIds = new List<GenreTmdb>();
        }
        public int GenreId { get; set; }
        public string GenreName { get; set; }
        public virtual ICollection<GenreTmdb> TmdbIds { get; set; }
    }
}
