﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class MediaFormat
    {
        public int MediaFormatId { get; set; }
        public string Description { get; set; }
    }
}
