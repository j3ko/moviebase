﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class Container
    {
        public int ContainerId { get; set; }
        public string ContainerName { get; set; }

        public IEnumerable<Slot> AvailableSlots(IDomainModel domainModel)
        {
            return domainModel.MoviebaseContext.Slots.Find(x => x.ContainerId == ContainerId).OrderBy(x => x.SlotId);
        }
    }
}
