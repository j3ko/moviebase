﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class TitlePerson : IConcurrencyControllable
    {
        public int TitleId { get; set; }
        public int PersonId { get; set; }
        public virtual Person Person { get; set; }
        public int Ordinal { get; set; }
        public string CharacterName { get; set; }
        public int TitleRoleId { get; set; }
        public virtual TitleRole TitleRole { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
