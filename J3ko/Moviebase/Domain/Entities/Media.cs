﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class Media
    {
        public int MediaId { get; set; }
        public string MediaName { get; set; }
        public int MediaFormatId { get; set; }
        public virtual MediaFormat MediaFormat { get; set; }
    }
}
