﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class Episode : IModifiable, IConcurrencyControllable
    {
        public int TitleId { get; set; }
        public int InstanceId { get; set; }
        public virtual Instance Instance { get; set; }
        public string EpisodeName { get; set; }
        public bool IsWishlist { get; set; }
        public int TitleGroupId { get; set; }
        public DateTime Modified { get; set; }
        public string ModifiedBy { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
