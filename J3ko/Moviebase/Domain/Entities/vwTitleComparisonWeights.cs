﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class vwTitleComparisonWeights
    {
        public vwTitleComparisonWeights()
        {
        }

        public int TitleId { get; set; }
        public string Weights { get; set; }

        public IEnumerable<string> WeightArray
        {
            get
            {
                if (Weights == null)
                {
                    return new List<string>();
                }
                return Weights.Split(new char[] { ',' });
            }
        }
    }
}
