﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Domain
{
    public class Document
    {
        public Guid StreamId { get; set; }
        public byte[] FileStream { get; set; }
        public string Name { get; set; }
        public DocumentMetadata DocumentMetadata { get; set; }
    }
}
