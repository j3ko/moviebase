﻿using J3ko.Infrastructure.MVC;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace J3ko.Moviebase.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
#if DEBUG
            HostingEnvironment.RegisterVirtualPathProvider(
                new NetworkVirtualPathProvider(
                    "~/" + ConfigurationManager.AppSettings["ImageVirtualPath"], 
                    ConfigurationManager.AppSettings["ImageNetworkPath"]));
#endif

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Moviebase configs
            GlobalConfiguration.Configure(MoviebaseConfig.Initialize);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;
        }
    }
}
