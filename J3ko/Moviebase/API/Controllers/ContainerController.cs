﻿using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace J3ko.Moviebase.API.Controllers
{
    public class ContainerController : BaseMoviebaseController
    {
        public PageResult Get(ODataQueryOptions<Container> options)
        {
            return Assemble(DomainModel.MoviebaseContext.Containers.GetAll(), options);
        }
        public SingleResult<Container> Get(int id)
        {
            return AssembleSingle(DomainModel.MoviebaseContext.Containers.FindSingle(x => x.ContainerId == id));
        }
    }
}
