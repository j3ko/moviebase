﻿using J3ko.Moviebase.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace J3ko.Moviebase.API.Controllers
{
    public class ApplicationMetadataController : BaseMoviebaseController
    {
        public SingleResult<ApplicationMetadataDto> Get()
        {
            ApplicationMetadataDto result = new ApplicationMetadataDto
            {
                TitleImagePath = DomainModel.ImageVirtualPath
            };

            return SingleResult.Create<ApplicationMetadataDto>(new List<ApplicationMetadataDto> { result }.AsQueryable());
        }
    }
}