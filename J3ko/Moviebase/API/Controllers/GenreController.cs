﻿using J3ko.Moviebase.Domain.DTOs;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace J3ko.Moviebase.API.Controllers
{
    public class GenreController : BaseMoviebaseDtoController<GenreDto>
    {
        public SingleResult<GenreDto> Get(int id)
        {
            return AssembleSingle<GenreDto>(x => x.GenreId == id);
        }
    }
}
