﻿using J3ko.Web.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace J3ko.Moviebase.API.Controllers
{
    public class WebMovieController : BaseMoviebaseController
    {
        public PageResult Get(ODataQueryOptions<WebMovieListing> options, string id)
        {
            return Assemble(DomainModel.WebMovieContext.TmdbRepository.Search(id).AsQueryable(), options);
        }
    }
}
