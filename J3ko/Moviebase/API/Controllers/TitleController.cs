﻿using J3ko.Moviebase.Domain;
using J3ko.Moviebase.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using J3ko.Moviebase.CQL.Assemblers;
using System.Web.Http.OData.Query;
using System.Web.Http.OData;

namespace J3ko.Moviebase.API.Controllers
{
    public class TitleController : BaseMoviebaseDtoController<TitleDto>
    {
        public SingleResult<TitleDto> Get(int id)
        {
            return AssembleSingle<TitleDto>(x => x.TitleId == id);
        }

        [HttpGet]
        [Route("api/Title/{id:int}/Suggestion")]
        public PageResult Suggestions(int id, ODataQueryOptions<TitleDto> options)
        {
            int[] suggestionIds = DomainModel.MoviebaseContext.TitleSuggestions
                .Find(x => x.TitleId == id)
                .OrderByDescending(x => x.Distance)
                .Select(x => x.SuggestionId)
                .ToArray();

            IQueryable<TitleDto> titles = this.Query<TitleDto>()
                .Where(x => suggestionIds.Contains(x.TitleId))
                .AsEnumerable()
                .OrderBy(x => Array.IndexOf(suggestionIds, x.TitleId))
                .AsQueryable();

            return Assemble<TitleDto>(titles, options);
        }
    }
}
