﻿using J3ko.Infrastructure.CQL;
using J3ko.Infrastructure.MVC;
using J3ko.Moviebase.CQL;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace J3ko.Moviebase.API.Controllers
{
    public class BaseMoviebaseController : BaseDomainController<IDomainModel>
    {
        public BaseMoviebaseController()
            : base(DomainModelFactory.GetDomainModel())
        { }
    }
}
