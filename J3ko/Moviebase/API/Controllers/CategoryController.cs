﻿using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace J3ko.Moviebase.API.Controllers
{
    public class CategoryController : BaseMoviebaseController
    {
        public PageResult Get(ODataQueryOptions<Category> options)
        {
            return Assemble(DomainModel.MoviebaseContext.Categories.GetAll(), options);
        }
        public SingleResult<Category> Get(int id)
        {
            return AssembleSingle<Category>(DomainModel.MoviebaseContext.Categories.FindSingle(x => x.CategoryId == id));
        }
    }
}
