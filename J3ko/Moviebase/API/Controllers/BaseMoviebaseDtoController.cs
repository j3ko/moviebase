﻿using J3ko.Infrastructure.CQL;
using J3ko.Infrastructure.MVC;
using J3ko.Moviebase.CQL;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace J3ko.Moviebase.API.Controllers
{
    public class BaseMoviebaseDtoController<TEntity> : BaseAssembledController<TEntity, IDomainModel>
        where TEntity : class, IDto
    {
        public BaseMoviebaseDtoController()
            : base(DomainModelFactory.GetDomainModel())
        { }

    }
}
