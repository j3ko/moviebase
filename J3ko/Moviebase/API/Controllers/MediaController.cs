﻿using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace J3ko.Moviebase.API.Controllers
{
    public class MediaController : BaseMoviebaseController
    {
        public PageResult Get(ODataQueryOptions<Media> options)
        {
            return Assemble(DomainModel.MoviebaseContext.Media.GetAll(), options);
        }
        public SingleResult<Media> Get(int id)
        {
            return AssembleSingle(DomainModel.MoviebaseContext.Media.FindSingle(x => x.MediaId == id));
        }
    }
}
