﻿using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace J3ko.Moviebase.API.Controllers
{
    public class InstanceController : BaseMoviebaseController
    {
        public PageResult Get(ODataQueryOptions<Instance> options)
        {
            return Assemble(DomainModel.MoviebaseContext.Instances.GetAll(), options);
        }
        public SingleResult<Instance> Get(int id)
        {
            return AssembleSingle(DomainModel.MoviebaseContext.Instances.FindSingle(x => x.InstanceId == id));
        }
    }
}
