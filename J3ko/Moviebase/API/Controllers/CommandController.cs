﻿using J3ko.Infrastructure.CQL;
using J3ko.Infrastructure.MVC;
using J3ko.Moviebase.CQL;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace J3ko.Moviebase.API.Controllers
{
    [Authorize]
    public class CommandController : ApiController
    {
        public IHttpActionResult Post(ICommand command)
        {
            try 
            {
                if (command != null)
                {
                    // TODO: use background worker here?
                    var t = Task.Run(async delegate
                    {
                        using (IDomainModel domainModel = DomainModelFactory.GetDomainModel())
                        {
                            await domainModel.CommandContext.ExecuteAsync(command);
                        }
                    });
                    return Ok<Guid>(command.CommandId);
                }
            }
            catch (Exception ex) 
            {
                return InternalServerError(ex);
            }

            return BadRequest("Invalid command.");
        }
    }
}
