﻿using J3ko.Moviebase.Domain;
using J3ko.Moviebase.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using J3ko.Infrastructure;
using J3ko.Moviebase.CQL.Assemblers;

namespace J3ko.Moviebase.API.Controllers
{
    public class SlotController : BaseMoviebaseDtoController<SlotDto>
    {
        public SingleResult<SlotDto> Get(int id)
        {
            return AssembleSingle<SlotDto>(x => x.SlotId == id);
        }
    }
}
