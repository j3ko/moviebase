﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using J3ko.Infrastructure.MVC;
using System.Web.Http.Cors;
using System.Configuration;

namespace J3ko.Moviebase.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var clientAppUrl = ConfigurationManager.AppSettings["ClientAppUrl"];
            if (!string.IsNullOrWhiteSpace(clientAppUrl))
            {
                var cors = new EnableCorsAttribute(clientAppUrl, "*", "*") { SupportsCredentials = true };
                config.EnableCors(cors);
            }
            else
            {
                var cors = new EnableCorsAttribute("*", "*", "*");
                config.EnableCors(cors);
            }

            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
