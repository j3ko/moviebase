﻿CREATE TABLE [dbo].[Container] (
    [ContainerId] INT            IDENTITY (1, 1) NOT NULL,
    [ContainerName]        NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Container] PRIMARY KEY CLUSTERED ([ContainerId] ASC)
);

