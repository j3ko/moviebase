﻿CREATE TABLE [dbo].[TitlePerson] (
    [TitleId]       INT            NOT NULL,
    [PersonId]      INT            NOT NULL,
    [Ordinal]       INT            NOT NULL,
    [CharacterName] NVARCHAR (255) NULL,
    [TitleRoleId]   INT            NOT NULL,
    [RowVersion]    ROWVERSION     NOT NULL,
    CONSTRAINT [PK_TitlePerson] PRIMARY KEY CLUSTERED ([TitleId] ASC, [PersonId] ASC, [Ordinal] ASC),
    CONSTRAINT [FK_TitlePerson_Person] FOREIGN KEY ([PersonId]) REFERENCES [dbo].[Person] ([PersonId]),
    CONSTRAINT [FK_TitlePerson_Title] FOREIGN KEY ([TitleId]) REFERENCES [dbo].[Title] ([TitleId]),
    CONSTRAINT [FK_TitlePerson_TitleRole] FOREIGN KEY ([TitleRoleId]) REFERENCES [dbo].[TitleRole] ([TitleRoleId])
);

