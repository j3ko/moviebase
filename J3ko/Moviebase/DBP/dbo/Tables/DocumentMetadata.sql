﻿CREATE TABLE [dbo].[DocumentMetadata] (
    [StreamId]   UNIQUEIDENTIFIER NOT NULL,
    [Source]     NVARCHAR (2083)  NULL,
    [Modified]   DATETIME         NOT NULL,
    [ModifiedBy] NVARCHAR (255)   NOT NULL,
    CONSTRAINT [PK_DocumentMetadata] PRIMARY KEY CLUSTERED ([StreamId] ASC),
    CONSTRAINT [FK_DocumentMetadata_Document] FOREIGN KEY ([StreamId]) REFERENCES [dbo].[Document] ([stream_id])
);





