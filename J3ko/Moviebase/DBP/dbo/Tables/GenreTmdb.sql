﻿CREATE TABLE [dbo].[GenreTmdb] (
    [GenreId] INT NOT NULL,
    [TmdbId]  INT NOT NULL,
    CONSTRAINT [PK_GenreTmdb] PRIMARY KEY CLUSTERED ([GenreId] ASC, [TmdbId] ASC),
    CONSTRAINT [FK_GenreTmdb_Genre] FOREIGN KEY ([GenreId]) REFERENCES [dbo].[Genre] ([GenreId])
);

