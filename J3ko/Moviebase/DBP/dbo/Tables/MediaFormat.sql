﻿CREATE TABLE [dbo].[MediaFormat] (
    [MediaFormatId] INT            IDENTITY (1, 1) NOT NULL,
    [Description]   NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_MediaFormat] PRIMARY KEY CLUSTERED ([MediaFormatId] ASC)
);

