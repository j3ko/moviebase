﻿CREATE TABLE [dbo].[Episode] (
    [TitleId]      INT            NOT NULL,
    [InstanceId]   INT            NOT NULL,
    [EpisodeName]  NVARCHAR (255) NOT NULL,
    [IsWishlist]   BIT            NOT NULL,
    [TitleGroupId] INT            NULL,
    [Modified]     DATETIME       NOT NULL,
    [ModifiedBy]   NVARCHAR (255) NOT NULL,
    [RowVersion]   ROWVERSION     NOT NULL,
    CONSTRAINT [PK_Episode] PRIMARY KEY CLUSTERED ([TitleId] ASC, [InstanceId] ASC),
    CONSTRAINT [FK_Episode_Instance] FOREIGN KEY ([InstanceId]) REFERENCES [dbo].[Instance] ([InstanceId]),
    CONSTRAINT [FK_Episode_Title] FOREIGN KEY ([TitleId]) REFERENCES [dbo].[Title] ([TitleId]),
    CONSTRAINT [FK_Episode_TitleGroup] FOREIGN KEY ([TitleGroupId]) REFERENCES [dbo].[TitleGroup] ([TitleGroupId])
);

