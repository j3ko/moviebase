﻿CREATE TABLE [dbo].[Person] (
    [PersonId]    INT              IDENTITY (1, 1) NOT NULL,
    [FullName]    NVARCHAR (255)   NOT NULL,
    [ThumbnailId] UNIQUEIDENTIFIER NULL,
    [TmdbId]      INT              NULL,
    [Modified]    DATETIME         NOT NULL,
    [ModifiedBy]  NVARCHAR (255)   NOT NULL,
    [RowVersion]  ROWVERSION       NOT NULL,
    CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED ([PersonId] ASC),
    CONSTRAINT [FK_Person_Document] FOREIGN KEY ([ThumbnailId]) REFERENCES [dbo].[Document] ([stream_id])
);








GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TmdbId_NotNull]
    ON [dbo].[Person]([TmdbId] ASC) WHERE ([TmdbId] IS NOT NULL);

