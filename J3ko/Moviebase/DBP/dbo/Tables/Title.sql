﻿CREATE TABLE [dbo].[Title] (
    [TitleId]      INT              IDENTITY (1, 1) NOT NULL,
    [TitleName]    NVARCHAR (255)   NOT NULL,
    [CategoryId]   INT              NOT NULL,
    [Plot]         NVARCHAR (MAX)   NULL,
    [CoverId]      UNIQUEIDENTIFIER NULL,
    [SmallCoverId] UNIQUEIDENTIFIER NULL,
    [ImdbId]       NVARCHAR (255)   NULL,
    [Synced]       DATE             NULL,
    [Released]     DATE             NOT NULL,
    [Modified]     DATETIME         NOT NULL,
    [ModifiedBy]   NVARCHAR (255)   NOT NULL,
    [RowVersion]   ROWVERSION       NOT NULL,
    CONSTRAINT [PK_Title] PRIMARY KEY CLUSTERED ([TitleId] ASC),
    CONSTRAINT [FK_Title_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Category] ([CategoryId]),
    CONSTRAINT [FK_Title_Document] FOREIGN KEY ([CoverId]) REFERENCES [dbo].[Document] ([stream_id]),
    CONSTRAINT [FK_Title_Document1] FOREIGN KEY ([SmallCoverId]) REFERENCES [dbo].[Document] ([stream_id])
);







