﻿CREATE TABLE [dbo].[TitleSuggestion] (
    [TitleId]      INT        NOT NULL,
    [SuggestionId] INT        NOT NULL,
    [Distance]     FLOAT (53) NOT NULL,
    CONSTRAINT [PK_TitleSuggestion] PRIMARY KEY CLUSTERED ([TitleId] ASC, [SuggestionId] ASC),
    CONSTRAINT [FK_TitleSuggestion_Title] FOREIGN KEY ([TitleId]) REFERENCES [dbo].[Title] ([TitleId]),
    CONSTRAINT [FK_TitleSuggestion_Title1] FOREIGN KEY ([SuggestionId]) REFERENCES [dbo].[Title] ([TitleId])
);

