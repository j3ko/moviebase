﻿CREATE TABLE [dbo].[SchemaProperty] (
    [PropertyName]  NVARCHAR (255) NOT NULL,
    [PropertyValue] NVARCHAR (255) NULL,
    CONSTRAINT [PK_SchemaProperty] PRIMARY KEY CLUSTERED ([PropertyName] ASC)
);

