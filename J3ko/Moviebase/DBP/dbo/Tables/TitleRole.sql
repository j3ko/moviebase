﻿CREATE TABLE [dbo].[TitleRole] (
    [TitleRoleId] INT            IDENTITY (1, 1) NOT NULL,
    [Description] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_TitleRole] PRIMARY KEY CLUSTERED ([TitleRoleId] ASC)
);

