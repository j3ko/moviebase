﻿CREATE TABLE [dbo].[Slot] (
    [SlotId]      INT            IDENTITY (1, 1) NOT NULL,
    [SlotLabel]   NVARCHAR (255) NOT NULL,
    [ContainerId] INT            NOT NULL,
    CONSTRAINT [PK_Slot] PRIMARY KEY CLUSTERED ([SlotId] ASC),
    CONSTRAINT [FK_Slot_Container] FOREIGN KEY ([ContainerId]) REFERENCES [dbo].[Container] ([ContainerId])
);

