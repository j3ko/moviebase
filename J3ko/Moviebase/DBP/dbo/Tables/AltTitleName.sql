﻿CREATE TABLE [dbo].[AltTitleName] (
    [TitleId]    INT            NOT NULL,
    [Name]       NVARCHAR (255) NOT NULL,
    [RowVersion] ROWVERSION     NOT NULL,
    CONSTRAINT [PK_AltTitleName] PRIMARY KEY CLUSTERED ([TitleId] ASC, [Name] ASC),
    CONSTRAINT [FK_AltTitleName_Title] FOREIGN KEY ([TitleId]) REFERENCES [dbo].[Title] ([TitleId])
);

