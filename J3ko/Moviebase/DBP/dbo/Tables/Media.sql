﻿CREATE TABLE [dbo].[Media] (
    [MediaId]       INT            IDENTITY (1, 1) NOT NULL,
    [MediaName]     NVARCHAR (255) NOT NULL,
    [MediaFormatId] INT            NOT NULL,
    CONSTRAINT [PK_Media] PRIMARY KEY CLUSTERED ([MediaId] ASC),
    CONSTRAINT [FK_Media_MediaFormat] FOREIGN KEY ([MediaFormatId]) REFERENCES [dbo].[MediaFormat] ([MediaFormatId])
);

