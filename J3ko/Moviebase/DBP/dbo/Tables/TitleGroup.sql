﻿CREATE TABLE [dbo].[TitleGroup] (
    [TitleGroupId] INT            IDENTITY (1, 1) NOT NULL,
    [Description]  NVARCHAR (255) NOT NULL,
    [DateAdded]    DATE           NOT NULL,
    [DateBurned]   DATE           NOT NULL,
    [MediaId]      INT            NOT NULL,
    [SlotId]       INT            NOT NULL,
    [TitleId]      INT            NOT NULL,
    [Modified]     DATETIME       NOT NULL,
    [ModifiedBy]   NVARCHAR (255) NOT NULL,
    [RowVersion]   ROWVERSION     NOT NULL,
    CONSTRAINT [PK_TitleGroup] PRIMARY KEY CLUSTERED ([TitleGroupId] ASC),
    CONSTRAINT [FK_TitleGroup_Media] FOREIGN KEY ([MediaId]) REFERENCES [dbo].[Media] ([MediaId]),
    CONSTRAINT [FK_TitleGroup_Slot] FOREIGN KEY ([SlotId]) REFERENCES [dbo].[Slot] ([SlotId]),
    CONSTRAINT [FK_TitleGroup_Title] FOREIGN KEY ([TitleId]) REFERENCES [dbo].[Title] ([TitleId])
);

