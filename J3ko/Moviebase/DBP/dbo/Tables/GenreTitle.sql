﻿CREATE TABLE [dbo].[GenreTitle] (
    [TitleId] INT NOT NULL,
    [GenreId] INT NOT NULL,
    CONSTRAINT [PK_GenreTitle] PRIMARY KEY CLUSTERED ([TitleId] ASC, [GenreId] ASC),
    CONSTRAINT [FK_GenreTitle_Genre] FOREIGN KEY ([GenreId]) REFERENCES [dbo].[Genre] ([GenreId]),
    CONSTRAINT [FK_GenreTitle_Title] FOREIGN KEY ([TitleId]) REFERENCES [dbo].[Title] ([TitleId])
);



