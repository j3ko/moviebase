﻿CREATE TABLE [dbo].[Instance] (
    [InstanceId]   INT            IDENTITY (1, 1) NOT NULL,
    [InstanceName] NVARCHAR (255) NOT NULL,
    CONSTRAINT [PK_Instance] PRIMARY KEY CLUSTERED ([InstanceId] ASC)
);

