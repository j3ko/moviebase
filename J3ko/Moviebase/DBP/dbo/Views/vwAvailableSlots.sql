﻿

CREATE VIEW [dbo].[vwAvailableSlots]
AS
SELECT
	[dbo].[Slot].[SlotId]
	,[dbo].[Slot].[SlotLabel]
	,[dbo].[Slot].[ContainerId]
	,CASE
		WHEN [dbo].[TitleGroup].[TitleGroupId] IS NULL THEN CONVERT(BIT, 'TRUE')
		ELSE CONVERT(BIT, 'FALSE')
	END [IsAvailable]
FROM
	[dbo].[Slot]
LEFT OUTER JOIN
	[dbo].[TitleGroup] ON [dbo].[Slot].[SlotId] = [dbo].[TitleGroup].[SlotId]