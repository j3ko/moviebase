﻿
CREATE VIEW [dbo].[vwAvailableGenres]
AS
SELECT     GenreId, GenreName
FROM         [dbo].[Genre]
WHERE     (GenreId IN
                          (SELECT DISTINCT GenreId
                            FROM          [dbo].[GenreTitle]))