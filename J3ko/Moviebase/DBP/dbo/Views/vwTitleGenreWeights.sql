﻿


CREATE VIEW [dbo].[vwTitleGenreWeights]
AS
	SELECT [t1].[TitleId],
	stuff( (
		SELECT ',' + COALESCE([v].[value], '0') [value] 
		FROM   [dbo].[Genre] [g]
			   CROSS JOIN [dbo].[Title] [t]
			   CROSS APPLY ( SELECT 
								CASE WHEN EXISTS(	
										SELECT [gt].[GenreId] 
										FROM [dbo].[GenreTitle] [gt] 
										WHERE [gt].[TitleId] = [t].[TitleId]
										AND [gt].[GenreId] = [g].[GenreId] ) THEN '2'
								ELSE '0'
								END [value] ) v
		WHERE [t1].[TitleId] = [t].[TitleId]
		ORDER BY [g].[GenreId]
		FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,'') AS [Weights]
	FROM [dbo].[Title] [t1]
	GROUP BY [t1].[TitleId]