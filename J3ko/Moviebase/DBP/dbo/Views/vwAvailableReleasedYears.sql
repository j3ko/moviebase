﻿
CREATE VIEW [dbo].[vwAvailableReleasedYears]
AS
SELECT DISTINCT TOP (100) PERCENT DATEPART(year, Released) AS ReleasedYear
FROM         [dbo].[Title]
ORDER BY ReleasedYear