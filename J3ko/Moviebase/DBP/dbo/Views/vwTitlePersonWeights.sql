﻿


CREATE VIEW [dbo].[vwTitlePersonWeights]
AS

	SELECT [t1].[TitleId],
	stuff( (
		SELECT ',' + COALESCE([v].[value], '0') [value]
		FROM   [dbo].[Person] [p]
			   CROSS JOIN [dbo].[Title] [t]
			   CROSS APPLY ( SELECT CASE WHEN EXISTS (
   									SELECT [tp].[Ordinal]
									FROM [dbo].[TitlePerson] [tp] 
									WHERE [tp].[TitleId] = [t].[TitleId]
									AND [tp].[PersonId] = [p].[PersonId]
									AND ([tp].[TitleRoleId] = 2 AND [tp].[Ordinal] < 6)
							) THEN ( SELECT TOP 1 CONVERT(VARCHAR, 1 + (CONVERT(FLOAT, 1) / (COALESCE([tp].[Ordinal], 0) + 1)))
									 FROM [dbo].[TitlePerson] [tp] 
									 WHERE [tp].[TitleId] = [t].[TitleId]
									 AND [tp].[PersonId] = [p].[PersonId]
									 ORDER BY [tp].[Ordinal] ASC )
							ELSE '0'
							END [value] ) v
		WHERE [t1].[TitleId] = [t].[TitleId]
		ORDER BY [p].[PersonId]
		FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,'') AS [Weights]
	FROM [dbo].[Title] [t1]
	GROUP BY [t1].[TitleId]