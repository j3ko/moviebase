﻿

CREATE VIEW [dbo].[vwGenres]
AS
SELECT
	a.[GenreId]
	, a.[GenreName]
	, CONVERT(BIT, COALESCE(NULLIF(b.GenreId, 0), 0)) [is_used]
FROM
	[dbo].Genre a
LEFT OUTER JOIN
	( 
		SELECT DISTINCT GenreId
			FROM [dbo].GenreTitle 
	) b	ON a.GenreId = b.GenreId