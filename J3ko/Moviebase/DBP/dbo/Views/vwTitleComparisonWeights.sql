﻿

CREATE VIEW [dbo].[vwTitleComparisonWeights]
AS

SELECT [tgw].[TitleId], [tgw].[Weights] + ',' + [tpw].[Weights] [Weights]
FROM [dbo].[vwTitleGenreWeights] [tgw]
INNER JOIN [dbo].[vwTitlePersonWeights] [tpw] ON [tgw].[TitleId] = [tpw].[TitleId]