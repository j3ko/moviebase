﻿------------------------------------------------------------------------------
-- Description:	Reference data sync for: dbo.Media
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
-- 2012.05.31	Jeffrey Ko		Issue fixed for columns with SQL keywords
-- 2013.12.19	Jeffrey Ko		Fixed problem with foreign key columns
--
-- Modification History:
-- 2013.12.19	JEFFREY\j3ko			Generated from JEFFREY\SQLEXPRESS [MoviebaseDEV2]
------------------------------------------------------------------------------

SET XACT_ABORT ON
PRINT 'Starting [dbo].[Media] Synchronization'
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N'xxxSYNCxxMEDIA')
			BEGIN
				DROP TABLE [dbo].[xxxSYNCxxMEDIA]
			END
	CREATE TABLE [dbo].[xxxSYNCxxMEDIA]
	([MediaId] int, [MediaName] nvarchar(255), [MediaFormatId] int)
	SET NOCOUNT ON

	INSERT [dbo].[xxxSYNCxxMEDIA]
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				([MediaId], [MediaName], [MediaFormatId]) VALUES

				(N'1', N'Unknown (DVD-R)', N'1')
				,(N'2', N'Unknown (DVD+R)', N'2')
				,(N'3', N'Unknown (DVD-RW)', N'3')
				,(N'4', N'Unknown (DVD+RW)', N'4')
				,(N'5', N'Unknown (BD)', N'5')
				,(N'6', N'TY Value 8x (1)', N'1')
				,(N'7', N'TY Value 8x (2)', N'1')

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE dbo.Media NOCHECK CONSTRAINT ALL
	SET IDENTITY_INSERT [dbo].[Media] ON

	MERGE [dbo].[Media] AS Target
	USING [dbo].[xxxSYNCxxMEDIA] AS Source ON (Target.[MediaId] = Source.[MediaId])
				WHEN MATCHED 
	AND ((Target.[MediaName] <> Source.[MediaName]
			OR (COALESCE(Target.MediaName, Source.MediaName) IS NOT NULL
			AND Target.MediaName + Source.MediaName IS NULL))
	OR (Target.[MediaFormatId] <> Source.[MediaFormatId]
			OR (COALESCE(Target.MediaFormatId, Source.MediaFormatId) IS NOT NULL
			AND Target.MediaFormatId + Source.MediaFormatId IS NULL)))
					THEN
						UPDATE
						SET 
						Target.[MediaName] = Source.[MediaName]
					,Target.[MediaFormatId] = Source.[MediaFormatId]

				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      ([MediaId], [MediaName], [MediaFormatId])
						VALUES      ([MediaId], [MediaName], [MediaFormatId])

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/

				--WHEN NOT MATCHED BY SOURCE 
				--	THEN
				--		DELETE

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/
	;
	
	SET IDENTITY_INSERT [dbo].[Media] OFF
	ALTER TABLE [dbo].[Media] CHECK CONSTRAINT ALL

	DROP TABLE [dbo].[xxxSYNCxxMEDIA];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT 'Done [dbo].[Media] Synchronization'
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT 'Failed Synchronizing [dbo].[Media]'
		END
END CATCH
