﻿------------------------------------------------------------------------------
-- Description:	Reference data sync for: dbo.TitleRole
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
-- 2012.05.31	Jeffrey Ko		Issue fixed for columns with SQL keywords
-- 2012.10.01	Jeffrey Ko		Added noVals parameter
--
-- Modification History:
-- 2013.12.19	JEFFREY\j3ko			Generated from JEFFREY\SQLEXPRESS [MoviebaseDEV2]
------------------------------------------------------------------------------

SET XACT_ABORT ON
GO
PRINT 'Starting [dbo].[TitleRole] Synchronization'
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N'xxxSYNCxxTITLEROLE')
			BEGIN
				DROP TABLE [dbo].[xxxSYNCxxTITLEROLE]
			END
	CREATE TABLE [dbo].[xxxSYNCxxTITLEROLE]
	([TitleRoleId] int, [Description] nvarchar(255))
	SET NOCOUNT ON

	INSERT [dbo].[xxxSYNCxxTITLEROLE]
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				([TitleRoleId], [Description]) VALUES

				(N'1', N'Other')
				,(N'2', N'Actor')
				,(N'3', N'Director')
				,(N'4', N'Producer')

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE dbo.TitleRole NOCHECK CONSTRAINT ALL
	SET IDENTITY_INSERT [dbo].[TitleRole] ON

	MERGE [dbo].[TitleRole] AS Target
	USING [dbo].[xxxSYNCxxTITLEROLE] AS Source ON (Target.[TitleRoleId] = Source.[TitleRoleId])
				WHEN MATCHED 
	AND ((Target.[Description] <> Source.[Description]
			OR (COALESCE(Target.Description, Source.Description) IS NOT NULL
			AND Target.Description + Source.Description IS NULL)))					THEN
						UPDATE
						SET 
						Target.[Description] = Source.[Description]

				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      ([TitleRoleId], [Description])
						VALUES      ([TitleRoleId], [Description])

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/

				WHEN NOT MATCHED BY SOURCE 
					THEN
						DELETE

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/
	;
	
	SET IDENTITY_INSERT [dbo].[TitleRole] OFF
	ALTER TABLE [dbo].[TitleRole] CHECK CONSTRAINT ALL

	DROP TABLE [dbo].[xxxSYNCxxTITLEROLE];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT 'Done [dbo].[TitleRole] Synchronization'
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT 'Failed Synchronizing [dbo].[TitleRole]'
		END
END CATCH
