﻿------------------------------------------------------------------------------
-- Description:	Reference data sync for: dbo.Category
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
-- 2012.05.31	Jeffrey Ko		Issue fixed for columns with SQL keywords
-- 2012.10.01	Jeffrey Ko		Added noVals parameter
--
-- Modification History:
-- 2013.12.19	JEFFREY\j3ko			Generated from JEFFREY\SQLEXPRESS [MoviebaseDEV]
------------------------------------------------------------------------------

SET XACT_ABORT ON
GO
PRINT 'Starting [dbo].[Category] Synchronization'
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N'xxxSYNCxxCATEGORY')
			BEGIN
				DROP TABLE [dbo].[xxxSYNCxxCATEGORY]
			END
	CREATE TABLE [dbo].[xxxSYNCxxCATEGORY]
	([CategoryId] int, [CategoryName] nvarchar(255))
	SET NOCOUNT ON

	INSERT [dbo].[xxxSYNCxxCATEGORY]
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				([CategoryId], [CategoryName]) VALUES

				(N'1', N'Movie')
				,(N'2', N'TV Show')
				,(N'3', N'Mini Series')

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE dbo.Category NOCHECK CONSTRAINT ALL
	SET IDENTITY_INSERT [dbo].[Category] ON

	MERGE [dbo].[Category] AS Target
	USING [dbo].[xxxSYNCxxCATEGORY] AS Source ON (Target.[CategoryId] = Source.[CategoryId])
				WHEN MATCHED 
	AND ((Target.[CategoryName] <> Source.[CategoryName]
			OR (COALESCE(Target.CategoryName, Source.CategoryName) IS NOT NULL
			AND Target.CategoryName + Source.CategoryName IS NULL)))					THEN
						UPDATE
						SET 
						Target.[CategoryName] = Source.[CategoryName]

				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      ([CategoryId], [CategoryName])
						VALUES      ([CategoryId], [CategoryName])

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/

				WHEN NOT MATCHED BY SOURCE 
					THEN
						DELETE

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/
	;
	
	SET IDENTITY_INSERT [dbo].[Category] OFF
	ALTER TABLE [dbo].[Category] CHECK CONSTRAINT ALL

	DROP TABLE [dbo].[xxxSYNCxxCATEGORY];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT 'Done [dbo].[Category] Synchronization'
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT 'Failed Synchronizing [dbo].[Category]'
		END
END CATCH
