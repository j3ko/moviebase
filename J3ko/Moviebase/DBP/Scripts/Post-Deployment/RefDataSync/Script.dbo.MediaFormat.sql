﻿------------------------------------------------------------------------------
-- Description:	Reference data sync for: dbo.MediaFormat
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
-- 2012.05.31	Jeffrey Ko		Issue fixed for columns with SQL keywords
-- 2012.10.01	Jeffrey Ko		Added noVals parameter
--
-- Modification History:
-- 2013.12.19	JEFFREY\j3ko			Generated from JEFFREY\SQLEXPRESS [MoviebaseDEV2]
------------------------------------------------------------------------------

SET XACT_ABORT ON
PRINT 'Starting [dbo].[MediaFormat] Synchronization'
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N'xxxSYNCxxMEDIAFORMAT')
			BEGIN
				DROP TABLE [dbo].[xxxSYNCxxMEDIAFORMAT]
			END
	CREATE TABLE [dbo].[xxxSYNCxxMEDIAFORMAT]
	([MediaFormatId] int, [Description] nvarchar(255))
	SET NOCOUNT ON

	INSERT [dbo].[xxxSYNCxxMEDIAFORMAT]
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				([MediaFormatId], [Description]) VALUES

				(N'1', N'DVD-R')
				,(N'2', N'DVD+R')
				,(N'3', N'DVD-RW')
				,(N'4', N'DVD+RW')
				,(N'5', N'BD')

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE dbo.MediaFormat NOCHECK CONSTRAINT ALL
	SET IDENTITY_INSERT [dbo].[MediaFormat] ON

	MERGE [dbo].[MediaFormat] AS Target
	USING [dbo].[xxxSYNCxxMEDIAFORMAT] AS Source ON (Target.[MediaFormatId] = Source.[MediaFormatId])
				WHEN MATCHED 
	AND ((Target.[Description] <> Source.[Description]
			OR (COALESCE(Target.Description, Source.Description) IS NOT NULL
			AND Target.Description + Source.Description IS NULL)))					THEN
						UPDATE
						SET 
						Target.[Description] = Source.[Description]

				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      ([MediaFormatId], [Description])
						VALUES      ([MediaFormatId], [Description])

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/

				WHEN NOT MATCHED BY SOURCE 
					THEN
						DELETE

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/
	;
	
	SET IDENTITY_INSERT [dbo].[MediaFormat] OFF
	ALTER TABLE [dbo].[MediaFormat] CHECK CONSTRAINT ALL

	DROP TABLE [dbo].[xxxSYNCxxMEDIAFORMAT];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT 'Done [dbo].[MediaFormat] Synchronization'
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT 'Failed Synchronizing [dbo].[MediaFormat]'
		END
END CATCH
