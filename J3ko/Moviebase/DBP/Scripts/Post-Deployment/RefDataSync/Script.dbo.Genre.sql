﻿------------------------------------------------------------------------------
-- Description:	Reference data sync for: dbo.Genre
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
-- 2012.05.31	Jeffrey Ko		Issue fixed for columns with SQL keywords
-- 2012.10.01	Jeffrey Ko		Added noVals parameter
--
-- Modification History:
-- 2013.12.19	JEFFREY\j3ko			Generated from JEFFREY\SQLEXPRESS [MoviebaseDEV2]
------------------------------------------------------------------------------

SET XACT_ABORT ON
PRINT 'Starting [dbo].[Genre] Synchronization'
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N'xxxSYNCxxGENRE')
			BEGIN
				DROP TABLE [dbo].[xxxSYNCxxGENRE]
			END
	CREATE TABLE [dbo].[xxxSYNCxxGENRE]
	([GenreId] int, [GenreName] nvarchar(255))
	SET NOCOUNT ON

	INSERT [dbo].[xxxSYNCxxGENRE]
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				([GenreId], [GenreName]) VALUES

				(N'1', N'None')
				,(N'2', N'Action')
				,(N'3', N'Adventure')
				,(N'4', N'Animation')
				,(N'5', N'Biography')
				,(N'6', N'Comedy')
				,(N'7', N'Crime')
				,(N'8', N'Documentary')
				,(N'9', N'Drama')
				,(N'10', N'Family')
				,(N'11', N'Fantasy')
				,(N'12', N'Film-Noir')
				,(N'13', N'Game Show')
				,(N'14', N'History')
				,(N'15', N'Horror')
				,(N'16', N'Music')
				,(N'17', N'Musical')
				,(N'18', N'Mystery')
				,(N'19', N'News')
				,(N'20', N'Reality')
				,(N'21', N'Romance')
				,(N'22', N'Sci-Fi')
				,(N'23', N'Shorts')
				,(N'24', N'Sports')
				,(N'25', N'Talk-Show')
				,(N'26', N'Thriller')
				,(N'27', N'War')
				,(N'28', N'Western')

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE dbo.Genre NOCHECK CONSTRAINT ALL
	SET IDENTITY_INSERT [dbo].[Genre] ON

	MERGE [dbo].[Genre] AS Target
	USING [dbo].[xxxSYNCxxGENRE] AS Source ON (Target.[GenreId] = Source.[GenreId])
				WHEN MATCHED 
	AND ((Target.[GenreName] <> Source.[GenreName]
			OR (COALESCE(Target.GenreName, Source.GenreName) IS NOT NULL
			AND Target.GenreName + Source.GenreName IS NULL)))					THEN
						UPDATE
						SET 
						Target.[GenreName] = Source.[GenreName]

				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      ([GenreId], [GenreName])
						VALUES      ([GenreId], [GenreName])

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/

				WHEN NOT MATCHED BY SOURCE 
					THEN
						DELETE

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/
	;
	
	SET IDENTITY_INSERT [dbo].[Genre] OFF
	ALTER TABLE [dbo].[Genre] CHECK CONSTRAINT ALL

	DROP TABLE [dbo].[xxxSYNCxxGENRE];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT 'Done [dbo].[Genre] Synchronization'
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT 'Failed Synchronizing [dbo].[Genre]'
		END
END CATCH
