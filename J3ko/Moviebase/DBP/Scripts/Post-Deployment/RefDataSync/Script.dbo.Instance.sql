﻿------------------------------------------------------------------------------
-- Description:	Reference data sync for: dbo.Instance
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
-- 2012.05.31	Jeffrey Ko		Issue fixed for columns with SQL keywords
-- 2012.10.01	Jeffrey Ko		Added noVals parameter
--
-- Modification History:
-- 2013.12.19	JEFFREY\j3ko			Generated from JEFFREY\SQLEXPRESS [MoviebaseDEV2]
------------------------------------------------------------------------------

SET XACT_ABORT ON
PRINT 'Starting [dbo].[Instance] Synchronization'
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N'xxxSYNCxxINSTANCE')
			BEGIN
				DROP TABLE [dbo].[xxxSYNCxxINSTANCE]
			END
	CREATE TABLE [dbo].[xxxSYNCxxINSTANCE]
	([InstanceId] int, [InstanceName] nvarchar(255))
	SET NOCOUNT ON

	INSERT [dbo].[xxxSYNCxxINSTANCE]
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				([InstanceId], [InstanceName]) VALUES

				(N'1', N'Part 1')
				,(N'2', N'Part 2')
				,(N'3', N'Part 3')
				,(N'4', N'Part 4')
				,(N'5', N'Part 5')
				,(N'6', N'Part 6')
				,(N'7', N'Part 7')
				,(N'8', N'Part 8')
				,(N'9', N'Part 9')
				,(N'10', N'Part 10')
				,(N'11', N'Part 11')
				,(N'12', N'Part 12')
				,(N'13', N'Part 13')
				,(N'14', N'Part 14')
				,(N'15', N'Part 15')
				,(N'16', N'Part 16')
				,(N'17', N'Part 17')
				,(N'18', N'Part 18')
				,(N'19', N'Part 19')
				,(N'20', N'Part 20')
				,(N'21', N'Part 21')
				,(N'22', N'Part 22')
				,(N'23', N'Part 23')
				,(N'24', N'Part 24')
				,(N'25', N'Part 25')
				,(N'26', N'Part 26')
				,(N'27', N'Part 27')
				,(N'28', N'Part 28')
				,(N'29', N'Part 29')
				,(N'30', N'Part 30')
				,(N'31', N'Part 31')
				,(N'32', N'Part 32')
				,(N'33', N'Part 33')
				,(N'34', N'Part 34')
				,(N'35', N'Part 35')
				,(N'36', N'Part 36')
				,(N'37', N'Part 37')
				,(N'38', N'Part 38')
				,(N'39', N'Part 39')
				,(N'40', N'Part 40')
				,(N'41', N'Part 41')
				,(N'42', N'Part 42')
				,(N'43', N'Part 43')
				,(N'44', N'Part 44')
				,(N'45', N'Part 45')
				,(N'46', N'Part 46')
				,(N'47', N'Part 47')
				,(N'48', N'Part 48')
				,(N'49', N'Part 49')
				,(N'50', N'Part 50')

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE dbo.Instance NOCHECK CONSTRAINT ALL
	SET IDENTITY_INSERT [dbo].[Instance] ON

	MERGE [dbo].[Instance] AS Target
	USING [dbo].[xxxSYNCxxINSTANCE] AS Source ON (Target.[InstanceId] = Source.[InstanceId])
				WHEN MATCHED 
	AND ((Target.[InstanceName] <> Source.[InstanceName]
			OR (COALESCE(Target.InstanceName, Source.InstanceName) IS NOT NULL
			AND Target.InstanceName + Source.InstanceName IS NULL)))					THEN
						UPDATE
						SET 
						Target.[InstanceName] = Source.[InstanceName]

				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      ([InstanceId], [InstanceName])
						VALUES      ([InstanceId], [InstanceName])

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/

				WHEN NOT MATCHED BY SOURCE 
					THEN
						DELETE

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/
	;
	
	SET IDENTITY_INSERT [dbo].[Instance] OFF
	ALTER TABLE [dbo].[Instance] CHECK CONSTRAINT ALL

	DROP TABLE [dbo].[xxxSYNCxxINSTANCE];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT 'Done [dbo].[Instance] Synchronization'
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT 'Failed Synchronizing [dbo].[Instance]'
		END
END CATCH
