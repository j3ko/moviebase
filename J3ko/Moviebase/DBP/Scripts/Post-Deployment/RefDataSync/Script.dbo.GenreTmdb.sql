﻿------------------------------------------------------------------------------
-- Description:	Reference data sync for: dbo.GenreTmdb
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
-- 2012.05.31	Jeffrey Ko		Issue fixed for columns with SQL keywords
-- 2013.12.19	Jeffrey Ko		Fixed problem with foreign key columns
--
-- Modification History:
-- 2013.12.19	JEFFREY\j3ko			Generated from JEFFREY\SQLEXPRESS [MoviebaseDEV2]
------------------------------------------------------------------------------

SET XACT_ABORT ON
PRINT 'Starting [dbo].[GenreTmdb] Synchronization'
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N'xxxSYNCxxGENRETMDB')
			BEGIN
				DROP TABLE [dbo].[xxxSYNCxxGENRETMDB]
			END
	CREATE TABLE [dbo].[xxxSYNCxxGENRETMDB]
	([GenreId] int, [TmdbId] int)
	SET NOCOUNT ON

	INSERT [dbo].[xxxSYNCxxGENRETMDB]
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				([GenreId], [TmdbId]) VALUES

				(N'2', N'28')
				,(N'2', N'10759')
				,(N'3', N'12')
				,(N'3', N'10759')
				,(N'4', N'16')
				,(N'6', N'35')
				,(N'7', N'50')
				,(N'8', N'99')
				,(N'9', N'18')
				,(N'10', N'10751')
				,(N'11', N'14')
				,(N'11', N'10765')
				,(N'12', N'10753')
				,(N'12', N'10754')
				,(N'14', N'36')
				,(N'15', N'27')
				,(N'16', N'10402')
				,(N'17', N'22')
				,(N'17', N'10402')
				,(N'18', N'9648')
				,(N'19', N'10763')
				,(N'20', N'10764')
				,(N'21', N'10749')
				,(N'22', N'878')
				,(N'22', N'10765')
				,(N'23', N'10755')
				,(N'24', N'9805')
				,(N'24', N'10757')
				,(N'24', N'10758')
				,(N'25', N'10767')
				,(N'26', N'53')
				,(N'26', N'10748')
				,(N'27', N'10752')
				,(N'27', N'10768')
				,(N'28', N'37')

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE dbo.GenreTmdb NOCHECK CONSTRAINT ALL

	MERGE [dbo].[GenreTmdb] AS Target
	USING [dbo].[xxxSYNCxxGENRETMDB] AS Source ON (Target.[GenreId] = Source.[GenreId] AND Target.[TmdbId] = Source.[TmdbId])

				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      ([GenreId], [TmdbId])
						VALUES      ([GenreId], [TmdbId])

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/

				--WHEN NOT MATCHED BY SOURCE 
				--	THEN
				--		DELETE

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/
	;
	
	ALTER TABLE [dbo].[GenreTmdb] CHECK CONSTRAINT ALL

	DROP TABLE [dbo].[xxxSYNCxxGENRETMDB];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT 'Done [dbo].[GenreTmdb] Synchronization'
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT 'Failed Synchronizing [dbo].[GenreTmdb]'
		END
END CATCH
