﻿------------------------------------------------------------------------------
-- Description:	Reference data sync for: dbo.Container
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko      Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko      Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
-- 2012.05.31	Jeffrey Ko		Issue fixed for columns with SQL keywords
-- 2013.12.19	Jeffrey Ko		Fixed problem with foreign key columns
--
-- Modification History:
-- 2014.01.03	JEFFREY\j3ko			Generated from JEFFREY\SQLEXPRESS [MoviebaseDEV2]
------------------------------------------------------------------------------

SET XACT_ABORT ON
PRINT 'Starting [dbo].[Container] Synchronization'
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N'xxxSYNCxxCONTAINER')
			BEGIN
				DROP TABLE [dbo].[xxxSYNCxxCONTAINER]
			END
	CREATE TABLE [dbo].[xxxSYNCxxCONTAINER]
	([ContainerId] int, [ContainerName] nvarchar(255))
	SET NOCOUNT ON

	INSERT [dbo].[xxxSYNCxxCONTAINER]
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				([ContainerId], [ContainerName]) VALUES

				(N'1', N'Big Box')

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE dbo.Container NOCHECK CONSTRAINT ALL
	SET IDENTITY_INSERT [dbo].[Container] ON

	MERGE [dbo].[Container] AS Target
	USING [dbo].[xxxSYNCxxCONTAINER] AS Source ON (Target.[ContainerId] = Source.[ContainerId])
				WHEN MATCHED 
	AND ((Target.[ContainerName] <> Source.[ContainerName]
			OR (COALESCE(Target.ContainerName, Source.ContainerName) IS NOT NULL
			AND Target.ContainerName + Source.ContainerName IS NULL)))
					THEN
						UPDATE
						SET 
						Target.[ContainerName] = Source.[ContainerName]

				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      ([ContainerId], [ContainerName])
						VALUES      ([ContainerId], [ContainerName])

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/

				WHEN NOT MATCHED BY SOURCE 
					THEN
						DELETE

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/
	;
	
	SET IDENTITY_INSERT [dbo].[Container] OFF
	ALTER TABLE [dbo].[Container] CHECK CONSTRAINT ALL

	DROP TABLE [dbo].[xxxSYNCxxCONTAINER];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT 'Done [dbo].[Container] Synchronization'
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT 'Failed Synchronizing [dbo].[Container]'
		END
END CATCH
