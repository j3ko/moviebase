﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
PRINT 'STARTING POST-DEPLOYMENT SCRIPT'
-- REFERNCE DATA SYNC
:r .\RefDataSync\Script.dbo.Category.sql
:r .\RefDataSync\Script.dbo.Container.sql
:r .\RefDataSync\Script.dbo.Slot.sql
:r .\RefDataSync\Script.dbo.Genre.sql
:r .\RefDataSync\Script.dbo.GenreTmdb.sql
:r .\RefDataSync\Script.dbo.Instance.sql
:r .\RefDataSync\Script.dbo.MediaFormat.sql
:r .\RefDataSync\Script.dbo.Media.sql
:r .\RefDataSync\Script.dbo.TitleRole.sql

-- DATAMOTION
:r .\DataMotion\Post_Version_0.sql

PRINT 'FINISHED POST-DEPLOYMENT SCRIPT'
