﻿ -- _____          _      _____           _       _   
 --|  __ \        | |    / ____|         (_)     | |  
 --| |__) |__  ___| |_  | (___   ___ _ __ _ _ __ | |_ 
 --|  ___/ _ \/ __| __|  \___ \ / __| '__| | '_ \| __|
 --| |  | (_) \__ \ |_   ____) | (__| |  | | |_) | |_ 
 --|_|   \___/|___/\__| |_____/ \___|_|  |_| .__/ \__|
 --                                        | |        
 --                                        |_|        

DECLARE @sql NVARCHAR(MAX), @param NVARCHAR(MAX), @result NVARCHAR(MAX), @current_version INT

/****************************************************************************/
/**** 1. Set current_version to the current schema version				 ****/
/**** ie. if this script is Post_Version_1.sql then @current_version = 1 ****/
/****************************************************************************/
SET @current_version = 0


SET @sql = 'SELECT @prop = [PropertyValue] 
FROM [dbo].[SchemaProperty]
WHERE [PropertyName] = N''Version''
AND [PropertyValue] >= ' + CONVERT(VARCHAR(10), @current_version)
SET @param = N'@prop nvarchar(1000) OUTPUT';

IF EXISTS 
(
	SELECT 1 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SchemaProperty'
) 
BEGIN
	EXEC sp_executesql @sql, @param, @prop = @result output
END
IF NOT EXISTS
(
	SELECT 1 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SchemaProperty'
)
OR
(
	COALESCE(@result, '') = ''
)
BEGIN
	PRINT 'Starting Version ' + CONVERT(VARCHAR(10), @current_version) + ' Post-Deployment Script'

	SET NOCOUNT ON

	MERGE INTO [dbo].[SchemaProperty] AS Target
	USING (SELECT N'Version', @current_version) AS Source (PropertyName, PropertyValue)
	ON (Target.[PropertyName] = Source.[PropertyName])
	WHEN MATCHED THEN
		UPDATE SET Target.[PropertyValue] = Source.[PropertyValue]
	WHEN NOT MATCHED THEN
		INSERT ([PropertyName], [PropertyValue])
		VALUES ([PropertyName], [PropertyValue]);

	PRINT 'Done Version ' + CONVERT(VARCHAR(10), @current_version) + ' Post-Deployment Script'
END
GO