﻿DECLARE @sql2005 BIT, @noVals BIT, @schema VARCHAR(255), @table VARCHAR(255)

/**********************************************************************/
/*****						PARAMETERS							  *****/
/**********************************************************************/

set @noVals = 0
set @sql2005 = 0
set @schema = 'dbo'
set @table = 'Media'

/**********************************************************************/
/*****						PARAMETERS							  *****/
/**********************************************************************/


SET NOCOUNT ON
-- #TABLECOLS
IF OBJECT_ID('tempdb..#TABLECOLS') IS NOT NULL
BEGIN
    DROP TABLE #TABLECOLS
END
SELECT DISTINCT
c.ordinal_position
,c.column_name
,c.data_type
,c.character_maximum_length
,prime.CONSTRAINT_TYPE [is_prime]
INTO #TABLECOLS
FROM 
	information_schema.columns c
LEFT OUTER JOIN
	information_schema.key_column_usage kcu 
ON 
	kcu.table_catalog = c.table_catalog 
	and kcu.table_schema = c.table_schema 
	and kcu.table_name = c.table_name 
	and kcu.column_name = c.column_name
OUTER APPLY (
	SELECT TOP 1 * 
	FROM information_schema.table_constraints tc 
	WHERE kcu.table_catalog = tc.table_catalog 
		and kcu.table_schema = tc.table_schema 
		and kcu.table_name = tc.table_name 
		and tc.constraint_type = 'PRIMARY KEY') prime
WHERE 
	c.table_schema = @schema 
AND 
	c.table_name = @table
AND
	c.data_type <> 'timestamp'
ORDER BY c.ordinal_position

IF ((SELECT COUNT(*) FROM #TABLECOLS) <= 0)
BEGIN
	DECLARE @ERRORMSG1 VARCHAR(MAX)
	SET @ERRORMSG1 = 'Error: Unable to locate table [' + DB_NAME() + '].[' + @schema + '].[' + @table + ']'
	RAISERROR (@ERRORMSG1, 10, 1)
END

DECLARE @result VARCHAR(MAX), @cols VARCHAR(MAX), @colstype VARCHAR(MAX), @values VARCHAR(MAX), @temp NVARCHAR(MAX), @temp2 NVARCHAR(MAX), @primecols VARCHAR(MAX), @matchcols VARCHAR(MAX), @updatecols VARCHAR(MAX), @targetprimenulls VARCHAR(MAX), @sourceprimenulls VARCHAR(MAX), @sourcecols VARCHAR(MAX)

DECLARE @NewLineChar CHAR(2)
SET @NewLineChar = CHAR(13) + CHAR(10)

SELECT @cols = COALESCE(@cols + ', ', '') + '[' + column_name + ']' FROM #TABLECOLS
SELECT @sourcecols = COALESCE(@sourcecols + ', ', '') + 'Source.' + '[' + column_name + ']' FROM #TABLECOLS
SELECT @colstype = COALESCE(@colstype + ', ', '') + '[' + column_name + '] ' + data_type + CASE COALESCE(character_maximum_length, '') WHEN '' THEN '' WHEN '-1' THEN '(MAX)' ELSE '(' + CONVERT(VARCHAR, character_maximum_length) + ')' END FROM #TABLECOLS
SELECT @primecols = COALESCE(@primecols + ' AND ', '') + 'Target.[' + column_name + '] = Source.[' + column_name + ']' FROM #TABLECOLS WHERE [is_prime] IS NOT NULL
SELECT @targetprimenulls = COALESCE(@targetprimenulls + ' AND ', '') + 'Target.[' + column_name + '] IS NULL' FROM #TABLECOLS WHERE [is_prime] IS NOT NULL
SELECT @sourceprimenulls = COALESCE(@sourceprimenulls + ' AND ', '') + 'Source.[' + column_name + '] IS NULL' FROM #TABLECOLS WHERE [is_prime] IS NOT NULL

SELECT @matchcols = 
	CASE WHEN data_type IN ('bit', 'date') THEN
	COALESCE(@matchcols + @NewLineChar + CHAR(9) + 'OR ', '') + 'Target.[' + column_name + '] <> Source.[' + column_name + ']' + @NewLineChar + CHAR(9) + CHAR(9) + CHAR(9) + 
	'OR (COALESCE('  + 'Target.' + column_name + ', Source.' + column_name + ') IS NOT NULL' + @NewLineChar + CHAR(9) + CHAR(9) + CHAR(9) + 'AND (CONVERT(VARCHAR,Target.' + column_name + ') + CONVERT(VARCHAR,Source.' + column_name + ') IS NULL))' 
ELSE
	COALESCE(@matchcols + @NewLineChar + CHAR(9) + 'OR ', '') + '(Target.[' + column_name + '] <> Source.[' + column_name + ']' + @NewLineChar + CHAR(9) + CHAR(9) + CHAR(9) + 
	'OR (COALESCE('  + 'Target.' + column_name + ', Source.' + column_name + ') IS NOT NULL' + @NewLineChar + CHAR(9) + CHAR(9) + CHAR(9) + 'AND Target.' + column_name + ' + Source.' + column_name + ' IS NULL))' 
END
FROM #TABLECOLS
WHERE [is_prime] IS NULL

SELECT @updatecols = COALESCE(@updatecols + @NewLineChar + CHAR(9) + CHAR(9) + CHAR(9) + CHAR(9) + CHAR(9) + ',', '') + 'Target.[' + column_name + '] = Source.[' + column_name + ']' FROM #TABLECOLS WHERE [is_prime] IS NULL

SELECT @temp = COALESCE(@temp + ', ', '') + '''+ case when ['+ column_name + '] is null then ''null'' else ''N''''''+replace(convert(varchar(max),[' + column_name + ']),'''''''','''''''''''') + '''''''' end +''' FROM #TABLECOLS
SET @temp2 = 'declare @NewLineChar AS CHAR(6)
set @NewLineChar = CHAR(13) + CHAR(10) + CHAR(9) + CHAR(9) + CHAR(9) + CHAR(9)
declare @values varchar(max)

'
IF (@sql2005 = 1)
BEGIN
	SET @temp2 = @temp2 + 'select @values = coalesce(@values + '' UNION ALL '' + @NewLineChar, '''') + ''SELECT ' + @temp + ''' from ' + @schema + '.' + @table + ' select @values'
END
ELSE
BEGIN
	SET @temp2 = @temp2 + 'select @values = coalesce(@values + @NewLineChar + '','', '''') + ''(' + @temp + ')'' from ' + @schema + '.' + @table + ' select @values'
END

IF OBJECT_ID('tempdb..#valueTable') IS NOT NULL
BEGIN
    DROP TABLE #valueTable
END
create table #valueTable
(
	value varchar(max)
)
IF (@noVals = 1 AND @sql2005 = 0)
	BEGIN
		INSERT #valueTable
		SELECT '(' + @cols + ')'
	END
ELSE IF (@noVals = 1 AND @sql2005 = 1)
	BEGIN
		INSERT #valueTable
		SELECT 'SELECT ' + @cols
	END
ELSE
	BEGIN
		insert #valueTable
		exec sp_executesql @temp2
	END

SET @result = '------------------------------------------------------------------------------
-- Description:	Reference data sync for: ' + @schema + '.' + @table + '
-- Parameters: none
--
-- Template Modification History:
-- 2011.08.25	Jeffrey Ko		Initial version
-- 2011.09.15   Jeffrey Ko		Issue fixed for tables w/o identity columns
-- 2011.09.15   Jeffrey Ko		Issue fixed for null values
-- 2011.09.28	Jeffrey Ko		SQL 2005 compatibility
-- 2011.09.29	Jeffrey Ko		Merged 2005/2008 scripts
-- 2012.05.31	Jeffrey Ko		Issue fixed for columns with SQL keywords
-- 2012.10.01	Jeffrey Ko		Added noVals parameter
-- 2014.08.28	Jeffrey Ko		Issue fixed for bit columns
-- 2014.10.20	Jeffrey Ko		Include foreign key columns
-- 2014.11.17	Jeffrey Ko		Fix for foreign key columns
-- 2015.01.13	Jeffrey Ko		Issue fixed for date columns
-- Modification History:
-- ' + convert(varchar,Getdate(),102) + '	' + system_user + '			Generated from ' + @@ServerName  + ' [' + DB_NAME() + ']
------------------------------------------------------------------------------

SET XACT_ABORT ON
PRINT ''Starting [' + @schema + '].[' + @table + '] Synchronization''
GO
BEGIN TRY
	BEGIN TRAN REFDATASYNC_TRANSACTION
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES
					  WHERE TABLE_NAME = N''xxxSYNCxx' + UPPER(@table) + ''')
			BEGIN
				DROP TABLE [' + @schema + '].[xxxSYNCxx' + UPPER(@table) + ']
			END
	CREATE TABLE [' + @schema + '].[xxxSYNCxx' + UPPER(@table) + ']
	'

	+ '(' + @colstype + ')'

	+ '
	SET NOCOUNT ON

	INSERT [' + @schema + '].[xxxSYNCxx' + UPPER(@table) + ']
	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/

				(' + @cols + ')'

IF (@sql2005 <> 1)
BEGIN
	SET @result = @result + ' VALUES'
END

SET @result = @result +	'

				' + (SELECT value FROM #valueTable)
                             
	+ '

	/****************************************************************************/
	/*****								DATA								*****/
	/****************************************************************************/
	SET NOCOUNT OFF
	ALTER TABLE ' + @schema + '.' + @table + ' NOCHECK CONSTRAINT ALL'

	IF EXISTS(SELECT * FROM sys.columns WHERE [object_id] = object_id(@schema + '.' + @table) and is_identity = 1)
	 BEGIN
		SET @result = @result + '
	SET IDENTITY_INSERT [' + @schema + '].[' + @table + '] ON'
	 END

	IF (@sql2005 = 1)
	BEGIN
	SET @result = @result + '

	UPDATE
		Target
	SET 
		' + @updatecols + '
	FROM
		[' + @schema + '].[' + @table + '] AS Target
	INNER JOIN 
		[' + @schema + '].[xxxSYNCxx' + UPPER(@table) + '] AS Source
	ON
		' + @primecols + '
	WHERE
	' + @matchcols + '

	INSERT INTO
		[' + @schema + '].[' + @table + ']
		(' + @cols + ')
	SELECT
		' + @sourcecols + '
	FROM
		[' + @schema + '].[xxxSYNCxx' + UPPER(@table) + '] AS Source
	LEFT OUTER JOIN
		[' + @schema + '].[' + @table + '] AS Target
	ON
		' + @primecols + '
	WHERE
	' + @targetprimenulls + '

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/

	--DELETE
	--	[' + @schema + '].[' + @table + '] 
	--FROM
	--	[' + @schema + '].[' + @table + '] AS Target
	--LEFT OUTER JOIN
	--	[' + @schema + '].[xxxSYNCxx' + UPPER(@table) + '] AS Source
	--ON
	--	' + @primecols + '
	--WHERE
	--' + @sourceprimenulls + '
	
	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/
	'
	END
	ELSE
	BEGIN
	SET @result = @result + '

	MERGE [' + @schema + '].[' + @table + '] AS Target
	USING [' + @schema + '].[xxxSYNCxx' + UPPER(@table) + '] AS Source ON (' + @primecols + ')
				WHEN MATCHED 
	AND (' + CASE COALESCE(@matchcols,'') WHEN '' THEN '1=1' ELSE @matchcols END + ')'
IF @updatecols IS NOT NULL
BEGIN
SET @result = @result + '					THEN
						UPDATE
						SET 
						' +
							@updatecols
END
ELSE
BEGIN
	SET @result = @result + ' PRINT ''possible problem here.'''
END
SET @result = @result + '

				WHEN NOT MATCHED BY TARGET 
					THEN
						INSERT      (' + @cols + ')
						VALUES      (' + @cols+ ')

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/

				--WHEN NOT MATCHED BY SOURCE 
				--	THEN
				--		DELETE

	/****************************************************************************/
	/*****				Uncommment this to delete on sync					*****/
	/****************************************************************************/
	;
	'
	END					
	IF EXISTS(SELECT * FROM sys.columns WHERE [object_id] = object_id(@schema + '.' + @table) and is_identity = 1)
	 BEGIN
		SET @result = @result + '
	SET IDENTITY_INSERT [' + @schema + '].[' + @table + '] OFF'
	 END

	SET @result = @result + '
	ALTER TABLE [' + @schema + '].[' + @table + '] CHECK CONSTRAINT ALL

	DROP TABLE [' + @schema + '].[xxxSYNCxx' + UPPER(@table) + '];

	COMMIT TRAN REFDATASYNC_TRANSACTION
	PRINT ''Done [' + @schema + '].[' + @table + '] Synchronization''
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRAN REFDATASYNC_TRANSACTION
			DECLARE @ERRORMSG VARCHAR(MAX)
			SET @ERRORMSG = ERROR_MESSAGE()
			RAISERROR(@ERRORMSG, 16, 1)
			PRINT ''Failed Synchronizing [' + @schema + '].[' + @table + ']''
		END
END CATCH
GO'

IF OBJECT_ID('tempdb..#TABLECOLS') IS NOT NULL
BEGIN
    DROP TABLE #TABLECOLS
END
IF OBJECT_ID('tempdb..#valueTable') IS NOT NULL
BEGIN
    DROP TABLE #valueTable
END

declare @string nvarchar(max)

set @string = rtrim( @result )

declare @cr char(1), @lf char(1)
set @cr = char(13)
set @lf = char(10)

declare @len int, @cr_index int, @lf_index int, @crlf_index int, @has_cr_and_lf bit, @left nvarchar(4000), @reverse nvarchar(4000)
set @len = 4000

while ( len( @string ) > @len )
begin
   set @left = left( @string, @len )
   set @reverse = reverse( @left )
   set @cr_index = @len - charindex( @cr, @reverse ) + 1
   set @lf_index = @len - charindex( @lf, @reverse ) + 1
   set @crlf_index = case when @cr_index < @lf_index then @cr_index else @lf_index end
   set @has_cr_and_lf = case when @cr_index < @len and @lf_index < @len then 1 else 0 end
   print left( @string, @crlf_index - 1 )
   set @string = right( @string, len( @string ) - @crlf_index - @has_cr_and_lf )
end

print @string