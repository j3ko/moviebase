﻿ --        ___    _____             _____           _       _   
 --       / _ \  |  __ \           / ____|         (_)     | |  
 --__   _| | | | | |__) | __ ___  | (___   ___ _ __ _ _ __ | |_ 
 --\ \ / / | | | |  ___/ '__/ _ \  \___ \ / __| '__| | '_ \| __|
 -- \ V /| |_| | | |   | | |  __/  ____) | (__| |  | | |_) | |_ 
 --  \_/  \___/  |_|   |_|  \___| |_____/ \___|_|  |_| .__/ \__|
 --                                                  | |        
 --                                                  |_|        

DECLARE @sql NVARCHAR(MAX), @param NVARCHAR(MAX), @result NVARCHAR(MAX), @current_version INT

SET @sql = 'SELECT @prop = [PropertyValue] 
FROM [dbo].[SchemaProperty]
WHERE [PropertyName] = N''Version''
AND [PropertyValue] >= 0'
SET @param = N'@prop nvarchar(1000) OUTPUT';

IF EXISTS 
(
	SELECT 1 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SchemaProperty'
) 
BEGIN
	EXEC sp_executesql @sql, @param, @prop = @result output
END

IF NOT EXISTS
(
	SELECT 1 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SchemaProperty'
)
OR
(
	COALESCE(@result, '') = ''
)
BEGIN
	PRINT 'Starting Version 0 Pre-Deployment Script'
	PRINT 'Done Version 0 Pre-Deployment Script'
END
GO