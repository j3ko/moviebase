﻿ -- _____             _____           _       _   
 --|  __ \           / ____|         (_)     | |  
 --| |__) | __ ___  | (___   ___ _ __ _ _ __ | |_ 
 --|  ___/ '__/ _ \  \___ \ / __| '__| | '_ \| __|
 --| |   | | |  __/  ____) | (__| |  | | |_) | |_ 
 --|_|   |_|  \___| |_____/ \___|_|  |_| .__/ \__|
 --                                    | |        
 --                                    |_|        

/****************************************************************************/
/**** 1. Set current_version to the current schema version				 ****/
/**** ie. if this script is Pre_Version_1.sql then @current_version = 1  ****/
/****************************************************************************/
DECLARE @sql NVARCHAR(MAX), @param NVARCHAR(MAX), @result NVARCHAR(MAX), @current_version INT

SET @current_version = 1

SET @sql = 'SELECT @prop = [PropertyValue] 
FROM [dbo].[SchemaProperty]
WHERE [PropertyName] = N''Version''
AND [PropertyValue] >= ' + CONVERT(VARCHAR(10), @current_version)
SET @param = N'@prop nvarchar(1000) OUTPUT';

IF EXISTS 
(
	SELECT 1 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SchemaProperty'
) 
BEGIN
	EXEC sp_executesql @sql, @param, @prop = @result output
END
IF NOT EXISTS
(
	SELECT 1 
        FROM INFORMATION_SCHEMA.TABLES 
        WHERE TABLE_SCHEMA = 'dbo' 
        AND  TABLE_NAME = 'SchemaProperty'
)
OR
(
	COALESCE(@result, '') = ''
)
BEGIN
	PRINT 'Starting Version ' + CONVERT(VARCHAR(10), @current_version) + ' Pre-Deployment Script'

	/****************************************************************************/
	/*****					2. Make data changes here						*****/
	/****************************************************************************/

	PRINT 'Done Version ' + CONVERT(VARCHAR(10), @current_version) + ' Pre-Deployment Script'
END
GO