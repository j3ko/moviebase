﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace J3ko.Moviebase.Client.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            var apiUrl = ConfigurationManager.AppSettings["WebApiUrl"];

            if (string.IsNullOrWhiteSpace(apiUrl)) throw new ApplicationException("Web API URL not set.");

            var client = new HttpClient();
            client.BaseAddress = new Uri(apiUrl);

            var response = await client.GetAsync("api/applicationmetadata");
            try
            {
                response.EnsureSuccessStatusCode();
            }
            catch (Exception)
            {
                throw new ApplicationException("Could not connect to Web API.");
            }

            var result = await response.Content.ReadAsStringAsync();
            var data = JsonConvert.DeserializeObject<dynamic>(result);

            var applicationMetadata = new ApplicationMetadata()
            {
                WebApiUrl = apiUrl,
                TitleImagePath = data.TitleImagePath,
                IsDebugMode = HttpContext.IsDebuggingEnabled
            };

            this.ViewBag.ApplicationMetadata = JsonConvert.SerializeObject(applicationMetadata);

            return View();
        }

        private class ApplicationMetadata
        {
            public bool IsDebugMode { get; set; }
            public string WebApiUrl { get; set; }
            public string TitleImagePath { get; set; }
        }
    }
}