﻿using J3ko.Moviebase.Client.Bundles;
using System;
using System.Configuration;
using System.Web;
using System.Web.Optimization;

namespace J3ko.Moviebase.Client
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Assets/Styles")
                .Include("~/Content/Styles/angularjs/angular-csp.css", new CssRewriteUrlTransform())
                .Include("~/Content/Styles/angular-material/angular-material.css", new CssRewriteUrlTransform())
                .Include("~/Content/Styles/font-awesome/font-awesome.css", new CssRewriteUrlTransform())
                .Include("~/Content/Styles/main.css", new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/Assets/Scripts/ThirdParty").Include(
                        // JQuery
                        "~/Scripts/jquery/jquery-2.1.3.js",
                        "~/Scripts/jquery.base64/jquery.base64.js",
                        // SignalR
                        "~/Scripts/ms-signalr-client/jquery.signalR-2.2.0.js",
                        // AngularJS
                        "~/Scripts/angularjs/angular.js",
                        "~/Scripts/angularjs/angular-animate.js",
                        "~/Scripts/angularjs/angular-aria.js",
                        "~/Scripts/angularjs/angular-messages.js",
                        // Angular ui-router
                        "~/Scripts/angular-ui-router/angular-ui-router.js",
                        // Angular Material
                        "~/Scripts/angular-material/angular-material.js",
                        // uuid.js
                        "~/Scripts/uuid/uuid.js",
                        // Lodash
                        "~/Scripts/lodash/lodash.js",
                        // Moment
                        "~/Scripts/moment/moment.js"));

            bundles.Add(new ScriptBundle("~/Assets/Scripts/Infrastructure")
                .Include(
                    "~/SPA/Infrastructure/Ext/ExtArray.js",
                    "~/SPA/Infrastructure/Ext/ExtString.js",
                    "~/SPA/Infrastructure/Ext/KeyValuePair.js",
                    "~/SPA/Infrastructure/Ext/Validation.js"
                )
                .Include(
                    "~/SPA/Infrastructure/Core/AngularServices.js",
                    "~/SPA/Infrastructure/Core/DependencyConfiguration.js",
                    "~/SPA/Infrastructure/Core/ServiceConfiguration.js",
                    "~/SPA/Infrastructure/Core/FilterConfiguration.js",
                    "~/SPA/Infrastructure/Core/Page.js",
                    "~/SPA/Infrastructure/Core/PageConfiguration.js",
                    "~/SPA/Infrastructure/Core/Directive.js",
                    "~/SPA/Infrastructure/Core/DirectiveConfiguration.js",
                    "~/SPA/Infrastructure/Core/AngularModule.js",
                    "~/SPA/Infrastructure/Core/AngularModuleConfiguration.js"
                ));

            bundles.Add(new AngularTemplatesBundle("TemplateModule", "~/Assets/Scripts/TemplateModule")
                .IncludeDirectory("~/SPA", "*.htm", true)
                .IncludeDirectory("~/SPA", "*.html", true));

            bundles.Add(new ScriptBundle("~/Assets/Scripts/SharedModule")
                .IncludeDirectory("~/SPA/Application/Shared/Models", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Shared/Services", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Shared/Filters", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Shared/Directives", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Shared/ViewModels", "*js", true)
                .Include("~/SPA/Application/Shared/SharedModule.js"));

            bundles.Add(new ScriptBundle("~/Assets/Scripts/MainModule")
                .IncludeDirectory("~/SPA/Application/Main/Models", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Main/Services", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Main/Filters", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Main/Directives", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Main/ViewModels", "*js", true)
                .Include("~/SPA/Application/Main/MainModuleConfig.js")
                .Include("~/SPA/Application/Main/MainModule.js"));

            bundles.Add(new ScriptBundle("~/Assets/Scripts/AccountModule")
                .IncludeDirectory("~/SPA/Application/Account/Models", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Account/Services", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Account/Filters", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Account/Directives", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Account/ViewModels", "*js", true)
                .Include("~/SPA/Application/Account/AccountModule.js"));

            bundles.Add(new ScriptBundle("~/Assets/Scripts/DetailModule")
                .IncludeDirectory("~/SPA/Application/Detail/Models", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Detail/Services", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Detail/Filters", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Detail/Directives", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Detail/ViewModels", "*js", true)
                .Include("~/SPA/Application/Detail/DetailModule.js"));

            bundles.Add(new ScriptBundle("~/Assets/Scripts/ListingModule")
                .IncludeDirectory("~/SPA/Application/Listing/Models", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Listing/Services", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Listing/Filters", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Listing/Directives", "*.js", true)
                .IncludeDirectory("~/SPA/Application/Listing/ViewModels", "*js", true)
                .Include("~/SPA/Application/Listing/ListingModule.js"));

            // Set minification flag
            string useMinification = ConfigurationManager.AppSettings["UseMinification"];
            if (!string.IsNullOrWhiteSpace(useMinification))
            {
                BundleTable.EnableOptimizations = Convert.ToBoolean(useMinification);
            }
            else
            {
                BundleTable.EnableOptimizations = (HttpContext.Current.IsDebuggingEnabled == false);
            }
        }
    }
}
