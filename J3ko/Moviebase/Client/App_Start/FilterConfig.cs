﻿using System.Web;
using System.Web.Mvc;

namespace J3ko.Moviebase.Client
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
