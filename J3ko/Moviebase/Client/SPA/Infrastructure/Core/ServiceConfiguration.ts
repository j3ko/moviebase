﻿module Core {
    export class ServiceConfiguration {
        private _config: Array<Api.KeyValuePair<string, Function>>;

        constructor() {
            this._config = new Array<Api.KeyValuePair<string, Function>>();
        }

        public RegisterService(name: string, service: Function): ServiceConfiguration {
            if (!Ext.String.HasValue(name)) throw "Argument cannot be null [name]";
            if (service == null) throw "Argument cannot be null [service]";

            if (Ext.Array.Any(this._config,(item: Api.KeyValuePair<string, Function>) => item.Key == name))
                throw Ext.String.Format("Duplicate service registration [{0}]", name);

            this._config.push(new Api.KeyValuePair(name, service));
            return this;
        }

        public Initialize(appModule: IAngularModule): void {
            Ext.Array.ForEach(this._config,(item: Api.KeyValuePair<string, Function>) => {
                var name = item.Key;
                var service = item.Value;
                if ((<any>service).Dependencies) {
                    service[AngularServices.$inject] = (<any>service).Dependencies;
                }
                appModule.service(name, service);
            });
        }
    }
}