﻿module Core {
    export interface IAngularModule extends ng.IModule { }
    export interface IScope extends ng.IScope {
        VM: PageViewModel;
        BaseUrl: string;
    }
    export interface IElement extends JQuery { }
    export interface IAttributes extends ng.IAttributes { }

    export class AngularServices {
        public static $scope = "$scope";
        public static $inject = "$inject";
        public static $stateProvider = "$stateProvider";
        public static $urlRouterProvider = "$urlRouterProvider";
        public static $q = "$q";
        public static $log = "$log";
        public static $logProvider = "$logProvider";
        public static $http = "$http";
        public static $state = "$state";
        public static $stateParams = "$stateParams";
        public static $injector = "$injector";
        public static $location = "$location";
        public static $locationProvider = "$locationProvider";
        public static $cookieStore = "$cookieStore";
        public static $compile = "$compile";
        public static $interval = "$interval";
    }
}