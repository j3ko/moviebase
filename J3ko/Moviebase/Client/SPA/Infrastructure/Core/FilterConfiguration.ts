﻿module Core {
    export interface IFilter {
        Filter(value: any, ...params: any[]);
    }

    export class FilterConfiguration {
        private _config: Array<Api.KeyValuePair<string, Function>>;

        constructor() {
            this._config = new Array<Api.KeyValuePair<string, Function>>();
        }

        public RegisterFilter(name: string, filter: Function): FilterConfiguration {
            if (!Ext.String.HasValue(name)) throw "Argument cannot be null [name]";
            if (filter == null) throw "Argument cannot be null [filter]";

            if (Ext.Array.Any(this._config,(item: Api.KeyValuePair<string, Function>) => item.Key == name))
                throw Ext.String.Format("Duplicate filter registration [{0}]", name);

            this._config.push(new Api.KeyValuePair(name, filter));
            return this;
        }

        public Initialize(appModule: IAngularModule): void {
            Ext.Array.ForEach(this._config,(item: Api.KeyValuePair<string, Function>) => {
                var name = item.Key;
                var filter = item.Value;

                appModule.filter(name.trim(),() => { return new (<any>filter()).Filter; });
            });
        }
    }
}