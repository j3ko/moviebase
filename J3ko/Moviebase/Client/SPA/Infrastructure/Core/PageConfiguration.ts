﻿module Core {
    export class PageConfiguration {
        private _pages: IPage[];
        private _baseUrl: string = (<any>window).BaseUrl;

        public UnknownUrlRedirect: string;
        public RootViewsFolder: string;

        constructor() {
            this._pages = [];
            this.UnknownUrlRedirect = "/";
        }

        public RegisterPage(page: IPage): PageConfiguration {
            if (page == null) throw "Argument cannot be null [page]";

            this._pages.push(page);
            return this;
        }

        public Initialize(appModule: IAngularModule): void {
            var pages = this._pages;
            var unknownRedirect = this.UnknownUrlRedirect;

            Ext.Array.ForEach(pages,(page) => this.RegisterViewModelState(page, null, appModule));

            appModule.config([AngularServices.$stateProvider, AngularServices.$urlRouterProvider, 
                ($stateProvider: ng.ui.IStateProvider, $urlRouterProvider: ng.ui.IUrlRouterProvider) => {
                    $urlRouterProvider.otherwise(unknownRedirect);
                    this.ActivatePagesRecursively($stateProvider, pages, null);
                }]);
        }

        private GetPageUrl(page: IPage) {
            var viewUrl = "";

            if (Ext.String.StartsWith(page.View, "~")) {
                viewUrl = this._baseUrl + page.View.substring(1);
            }
            else if (Ext.String.HasValue(this.RootViewsFolder)) {
                viewUrl = this._baseUrl + this.RootViewsFolder + page.View;
            } else {
                viewUrl = this._baseUrl + page.View;
            }

            return viewUrl;
        }

        private RegisterState($stateProvider: ng.ui.IStateProvider, page: IPage, pageName: string) {
            var viewUrl = this.GetPageUrl(page);

            $stateProvider.state(pageName,
                {
                    url: page.Url,
                    templateUrl: viewUrl,
                    controller: page.ViewModel,
                });
        }

        private RegisterStateWithSiblings($stateProvider: ng.ui.IStateProvider, page: IPage, pageName: string) {
            var stateWithSiblings = {};
            var viewUrl = this.GetPageUrl(page);

            Ext.Array.ForEach(page.SiblingPages,(sib: IPage) => {
                var sibResolve = {};

                if ((<any>sib.ViewModel).Dependencies) { 

                    sib.ViewModel[AngularServices.$inject] = (<any>sib.ViewModel).Dependencies;

                    Ext.Array.ForEach((<any>sib.ViewModel).Dependencies,(val) => {
                        if (val !== '$scope') {
                            sibResolve[val] = val;
                        }
                    });
                }

                stateWithSiblings[sib.Name] = {
                    templateUrl: this.GetPageUrl(sib),
                    controller: sib.ViewModel,
                    resolve: sibResolve,
                };
            });

            stateWithSiblings[""] = {
                templateUrl: viewUrl,
                controller: page.ViewModel
            }

            $stateProvider.state(pageName,
                {
                    url: page.Url,
                    views: stateWithSiblings
                });
        }

        private ActivatePagesRecursively($stateProvider: ng.ui.IStateProvider, pages: Array<IPage>, parentPageName: string): void {
            if (pages == null) return;
            Ext.Array.ForEach(pages, (page: IPage) => {
                var pageName = page.Name;
                
                if (parentPageName != null) pageName = parentPageName + "." + page.Name;

                if (!(<any>page.ViewModel).Dependencies) throw Ext.String.Format("Dependencies not defined for PageViewModel {0}", pageName);

                page.ViewModel[AngularServices.$inject] = (<any>page.ViewModel).Dependencies;

                if (page.SiblingPages != null && page.SiblingPages.length > 0) {
                    this.RegisterStateWithSiblings($stateProvider, page, pageName);
                }
                else {
                    this.RegisterState($stateProvider, page, pageName);
                }

                if (page.ChildPages != null && page.ChildPages.length > 0) {
                    this.ActivatePagesRecursively($stateProvider, page.ChildPages, pageName);
                }
            });
        }

        private RegisterViewModelState(page: IPage, parentPageName: string, appModule: IAngularModule): void {
            var pageName = page.Name;
            if (parentPageName != null) pageName = parentPageName + "_" + page.Name;

            if (page.ViewModelState) {
                if ((<any>page.ViewModelState).Dependencies) {
                    page.ViewModelState[AngularServices.$inject] = (<any>page.ViewModelState).Dependencies;
                }

                var viewModelStateName = pageName + "_ViewModelState";
                appModule.service(viewModelStateName, page.ViewModelState);
                (<any>page.ViewModel).Dependencies.push(viewModelStateName);
            }

            if (page.ChildPages != null && page.ChildPages.length > 0) {
                Ext.Array.ForEach(page.ChildPages,(item) => this.RegisterViewModelState(item, pageName, appModule));
            }
        }
    }
}