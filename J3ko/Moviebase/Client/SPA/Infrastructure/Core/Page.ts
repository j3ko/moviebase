﻿module Core {
    export interface IPage {
        Name: string;
        Url: string;
        ViewModel: Function;
        View: string;
        ViewModelState?: any;
        ChildPages: Array<IPage>;
        SiblingPages: Array<IPage>;
        //OnEnter?: Function;
        //OnExit?: Function;
    }

    export class Page implements IPage {
        Name: string;
        Url: string;
        ViewModel: Function;
        View: string;
        ViewModelState: any;
        ChildPages: Array<IPage>;
        SiblingPages: Array<IPage>;
        //OnEnter: Function;
        //OnExit: Function;

        constructor();
        constructor(name: string, url: string, viewModel: Function, view: string);
        constructor(name: string, url: string, viewModel: Function, view: string, viewModelState: any);
        constructor(name?: string, url?: string, viewModel?: Function, view?: string, viewModelState?: any) {
            if (name) this.Name = name;
            if (url) this.Url = url;
            if (viewModel) this.ViewModel = viewModel;
            if (view) this.View = view;

            if (viewModelState) this.ViewModelState = viewModelState;

            this.ChildPages = new Array<IPage>();
            this.SiblingPages = new Array<IPage>();
        }

        public AddChildPage(childPage: IPage): Page {
            this.ChildPages.push(childPage);
            return this;
        }

        public AddChildPages(...childPages: IPage[]): Page {
            for (var i = 0; i < childPages.length; i++) {
                this.ChildPages.push(childPages[i]);
            }

            return this;
        }

        public AddSiblingPage(siblingPage: IPage): Page {
            this.SiblingPages.push(siblingPage);
            return this;
        }

        public AddSiblingPages(...siblingPages: IPage[]): Page {
            for (var i = 0; i < siblingPages.length; i++) {
                this.SiblingPages.push(siblingPages[i]);
            }

            return this;
        }
    }

    export class PageViewModel {
        private _scope: IScope;

        public get Scope(): IScope { return this._scope; }


        constructor($scope: IScope) {
            this._scope = $scope;

            this.Scope.VM = this;
            this.Scope.BaseUrl = (<any>window).BaseUrl;

            this._scope.$on("$destroy",() => this.Destructor());
        }

        public Destructor(): void
        { }
    }

    export class PageViewModelWithParent<TParentPageViewModel> extends PageViewModel {
        private _parent: TParentPageViewModel;

        public get Parent(): TParentPageViewModel { return this._parent; }

        constructor($scope: IScope) {
            super($scope);

            //this.Scope.VM.Parent = (<IScope>this.Scope.$parent).VM;
            this._parent = <TParentPageViewModel>(<any>this.Scope.$parent).VM;
        }
    }
}