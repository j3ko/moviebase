﻿module Core {
    export class DirectiveConfiguraton {
        private _config: Array<Api.KeyValuePair<string, Function>>;
        private _baseUrl: string = (<any>window).BaseUrl;

        public RootViewsFolder: string;

        constructor() {
            this._config = Array<Api.KeyValuePair<string, Function>>();
        }

        public RegisterDirective(name: string, directive: Function): DirectiveConfiguraton {
            if (!Ext.String.HasValue(name)) throw "Argument cannot be null [name]";
            if (directive == null) throw Ext.String.Format("Argument cannot be null [directive] [name = {0}]", name);

            if (Ext.Array.Any(this._config,(item: Api.KeyValuePair<string, Function>) => item.Key == name))
                throw Ext.String.Format("Duplicate directive registration [{0}]", name);

            this._config.push(new Api.KeyValuePair(name, directive));
            return this;
        }

        public Initialize(appModule: IAngularModule): void {
            Ext.Array.ForEach(this._config,(item: Api.KeyValuePair<string, Function>) => {
                var name = item.Key;
                var directive = item.Value;

                var implementation: IDirective = new (<any>directive)();

                var stateName = null;
                if (implementation.State != null) {
                    if ((<any>implementation.State).Dependencies) {
                        implementation.State[AngularServices.$inject] = (<any>implementation.State).Dependencies;
                    }

                    stateName = name + "State";
                    appModule.service(stateName, implementation.State);
                }

                var viewModelName = null;
                if (implementation.ViewModel != null) {
                    if (stateName != null) (<any>implementation.ViewModel).Dependencies.push(stateName);
                    implementation.ViewModel[AngularServices.$inject] = (<any>implementation.ViewModel).Dependencies;
                    viewModelName = name + "ViewModel";
                    appModule.controller(viewModelName, implementation.ViewModel);
                }

                var viewUrl = null;
                if (Ext.String.HasValue(implementation.View)) {
                    if (Ext.String.StartsWith(implementation.View, "~")) {
                        viewUrl = this._baseUrl + implementation.View.substring(1);
                    }
                    else if (Ext.String.HasValue(this.RootViewsFolder)) {
                        viewUrl = this._baseUrl + this.RootViewsFolder + implementation.View;
                    } else {
                        viewUrl = this._baseUrl + implementation.View;
                    }
                }


                appModule.directive(name,() => {
                    return {
                        restrict: implementation.Restrict,
                        templateUrl: viewUrl,
                        template: implementation.Template,
                        replace: implementation.Replace,
                        transclude: implementation.Transclude,
                        scope: implementation.Scope,
                        link: implementation.Link,
                        controller: viewModelName
                    };
                });
            });
        }
    }
}