﻿module Core {
    export class AngularModule {
        //#region Private Fields
        private self = this;
        private _dependencyConfiguration: DependencyConfiguration;
        private _serviceConfiguration: ServiceConfiguration;
        private _filterConfiguration: FilterConfiguration;
        private _directiveConfiguration: DirectiveConfiguraton;
        private _pageConfiguration: PageConfiguration;
        private _appModule: IAngularModule;
        private _moduleConfiguration: Function;
        //#endregion

        public Constants: Api.KeyValuePair<string, any>[] = [];

        //#region Constructors
        constructor(moduleName: string) {
            this._dependencyConfiguration = new DependencyConfiguration();
            this._serviceConfiguration = new ServiceConfiguration();
            this._filterConfiguration = new FilterConfiguration();
            this._directiveConfiguration = new DirectiveConfiguraton();
            this._pageConfiguration = new PageConfiguration();

            this._dependencyConfiguration.RegisterDependency("ui.router");
            this._dependencyConfiguration.RegisterDependency("ngMaterial");
            this._dependencyConfiguration.RegisterDependency("ngMessages");

            this.ConfigureDependencies(this._dependencyConfiguration);
            this._appModule = this._dependencyConfiguration.Initialize(moduleName);            

            this.ConfigureServices(this._serviceConfiguration);
            this.ConfigureFilters(this._filterConfiguration);
            this.ConfigureDirectives(this._directiveConfiguration);

            this._serviceConfiguration.Initialize(this._appModule);
            this._filterConfiguration.Initialize(this._appModule);
            this._directiveConfiguration.Initialize(this._appModule);

            this.ConfigurePages(this._pageConfiguration);
            this._pageConfiguration.Initialize(this._appModule);
        }
        //#endregion

        private ConfigureModule(appModule: IAngularModule, moduleConfiguration: Function) {
            if (angular.isFunction(moduleConfiguration)) {
                moduleConfiguration[AngularServices.$inject] = (<any>moduleConfiguration).Dependencies;

                if ((<any>moduleConfiguration).RunConfiguration) {
                    appModule.run(<any>moduleConfiguration);
                }
                else {
                    appModule.config(<any>moduleConfiguration);
                }
            }
        }
        public RegisterModuleConfiguration(moduleConfiguration: Function) {
            this._moduleConfiguration = moduleConfiguration;
            this.ConfigureModule(this._appModule, this._moduleConfiguration);
        }

        private ConfigureConstants(appModule: IAngularModule, constants: Api.KeyValuePair<string, any>[]) {
            Ext.Array.ForEach(constants,(item: Api.KeyValuePair<string, any>) => {
                var name = item.Key;
                var cons = item.Value;

                appModule.constant(name, cons);
            });
        }
        public RegisterConstant(name: string, constant: any) {
            this.Constants.push(new Api.KeyValuePair(name, constant));
            this.ConfigureConstants(this._appModule, this.Constants);
        }

        //#region Public Methods (virtual)
        public ConfigureDependencies(dependencyConfiguration: Core.DependencyConfiguration): void
        { }

        public ConfigureServices(serviceConfiguration: Core.ServiceConfiguration): void
        { }

        public ConfigureFilters(filterConfiguration: Core.FilterConfiguration): void
        { }

        public ConfigureDirectives(directiveConfiguration: Core.DirectiveConfiguraton): void
        { }

        public ConfigurePages(pageConfiguration: Core.PageConfiguration): void
        { } 
        //#endregion
    }
}