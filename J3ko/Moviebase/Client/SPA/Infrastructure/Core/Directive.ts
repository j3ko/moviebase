﻿module Core {
    export interface IDirective {
        Restrict: string;
        View: string;
        Template: string;
        Replace: boolean;
        Transclude: boolean;
        Scope: any;
        ViewModel: Function;
        State: any;

        Link(scope: IScope, element: IElement, attrs: IAttributes, ctrl): void;
    }

    export class Directive implements IDirective {
        public Restrict: string;
        public View: string;
        public Template: string;
        public Replace: boolean;
        public Transclude: boolean;
        public Scope: any;
        public ViewModel: Function;
        public State: any;

        constructor(restrict: string) {
            this.Restrict = restrict;
            this.Scope = {};
        }

        public Link(scope: IScope, element: IElement, attrs: IAttributes, viewModel: DirectiveViewModel): void {
            if (viewModel != null) viewModel.Link(element, attrs);
        }
    }

    export class BehaviourDirective extends Directive {
        constructor() {
            super("A");
        }
    }

    export class ComponentDirective extends Directive {
        constructor() {
            super("E");
            this.Replace = true;
        }
    }

    export class ContainerDirective extends ComponentDirective {
        constructor() {
            super();
            this.Transclude = true;
        }
    }

    export class DirectiveViewModel extends PageViewModel {
        constructor($scope) {
            super($scope);
        }

        public Link(element: Core.IElement, attrs: Core.IAttributes): void
        { }
    }
}