﻿module Core {
    export class DependencyConfiguration {
        private _dependencyNames: Array<string>;
        private _dependencyModules: Array<Api.KeyValuePair<string, Function>>;

        constructor() {
            this._dependencyNames = new Array<string>();
            this._dependencyModules = new Array<Api.KeyValuePair<string, Function>>();
        }

        public RegisterDependency(name: string): DependencyConfiguration;
        public RegisterDependency(name: string, angularModule: Function): DependencyConfiguration;
        public RegisterDependency(name: string, angularModule?: Function): DependencyConfiguration {
            if (!Ext.String.HasValue(name)) throw "Argument cannot be null [name]";
            if (Ext.Array.Contains(this._dependencyNames, name)) throw Ext.String.Format("Duplicate dependency registration [{0}]", name);

            if (angularModule != null) this._dependencyModules.push(new Api.KeyValuePair(name, angularModule));
            else this._dependencyNames.push(name);

            return this;
        }

        public Initialize(moduleName: string): IAngularModule {
            var dependencies = [];
            Ext.Array.ForEach(this._dependencyModules,(item: Api.KeyValuePair<string, Function>) => {
                var angularModule: AngularModule = new (<any>item.Value)(item.Key);
                //angularModule.Initialize();

                dependencies.push(item.Key);
            });
            Ext.Array.ForEach(this._dependencyNames,(item) => dependencies.push(item));

            return angular.module(moduleName, dependencies);
        }
    }
}