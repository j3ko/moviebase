﻿module Ext {
    export class String {
        public static HasValue(value: string): boolean {
            if (value && value.trim().length > 0) return true;
            return false;
        }

        public static Contains(primary: string, sub: string): boolean {
            if (primary.indexOf(sub) != -1) return true;
            return false;
        }

        public static StartsWith(primary: string, sub: string): boolean {
            if (primary.indexOf(sub) == 0) return true;
            return false;
        }

        public static EndsWith(primary: string, sub: string): boolean {
            var index = primary.lastIndexOf(sub);
            if ((index + sub.length) == primary.length) return true;
            return false;
        }

        public static Format(formatString, ...params: string[]) {
            var result = formatString;
            if (result == null) return null;

            if (params == null) return result;

            for (var i = 0; i < params.length; i++) {
                var format = "{" + i.toString() + "}";
                while (String.Contains(result, format)) {
                    result = result.replace(format, params[i]);
                }
            }

            return result;
        }

        public static JustNumbers(value: string): string {
            if (String.HasValue(value)) return value.replace(/[^0-9]/g, '');
            return "";
        }
    }
}