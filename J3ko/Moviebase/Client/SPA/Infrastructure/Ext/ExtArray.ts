﻿module Ext {
    export class Array {
        public static Find(array: any[], predicate: (value: any) => boolean): any {
            for (var i = 0; i < array.length; i++) {
                if (predicate(array[i])) {
                    return array[i];
                }
            }
            return null;
        }

        public static First(array: any[]): any;
        public static First(array: any[], predicate: (value) => boolean): any;
        public static First(array: any[], predicate?: (value) => boolean): any {
            var result = null;

            if (predicate != null) {
                for (var i = 0; i < array.length; i++) {
                    if (predicate(array[i])) {
                        result = array[i];
                        break;
                    }
                }
            } else {
                for (var j = 0; j < array.length; j++) {
                    result = array[j];
                    break;
                }
            }

            if (result == null) throw "No items in collection or none that satisfy condition.";

            return result;
        }

        public static FirstOrDefault(array: any[]): any;
        public static FirstOrDefault(array: any[], predicate: (value) => boolean): any;
        public static FirstOrDefault(array: any[], predicate?: (value) => boolean): any {
            var result = null;

            if (predicate != null) {
                for (var i = 0; i < array.length; i++) {
                    if (predicate(array[i])) {
                        result = array[i];
                        break;
                    }
                }
            } else {
                for (var j = 0; j < array.length; j++) {
                    result = array[j];
                    break;
                }
            }

            return result;
        }

        public static ForEach(array: any[], action: (value: any) => void): void {
            for (var i = 0; i < array.length; i++) {
                action(array[i]);
            }
        }

        public static Any(array: any[]): boolean;
        public static Any(array: any[], predicate: (value: any) => boolean): boolean;
        public static Any(array: any[], predicate?: (value: any) => boolean): boolean {
            if (predicate == null) {
                return array.length > 0;
            }

            for (var i = 0; i < array.length; i++) {
                if (predicate(array[i])) {
                    return true;
                }
            }
            return false;
        }

        public static All(array: any[], predicate: (value: any) => boolean): boolean {
            for (var i = 0; i < array.length; i++) {
                if (predicate(array[i])) {
                    continue;
                }
                else {
                    return false;
                }
            }
            return true;;
        }

        public static Where(array: any[], predicate: (value: any) => boolean): any[] {
            var result = [];
            for (var i = 0; i < array.length; i++) {
                if (predicate(array[i])) {
                    result.push(array[i]);
                }
            }
            return result;
        }

        public static Remove(array: any[], value: any): boolean {
            var index = array.indexOf(value);
            if (index < 0) return false;

            array.splice(index, 1);
            return true;
        }

        public static OrderBy(array: any[], compareValue: (value: any) => any): any[] {
            var internalArray = [];
            for (var i = 0; i < array.length; i++) internalArray.push(array[i]);

            internalArray.sort((a, b) => {
                var valA = compareValue(a);
                var valB = compareValue(b);
                if (valA < valB) return -1;
                if (valA > valB) return 1;
                return 0;
            });

            return internalArray;
        }

        public static OrderByDescending(array: any[], compareValue: (value: any) => any): any[] {
            var internalArray = [];
            for (var i = 0; i < array.length; i++) internalArray.push(array[i]);

            internalArray.sort((a, b) => {
                var valA = compareValue(a);
                var valB = compareValue(b);
                if (valB < valA) return -1;
                if (valB > valA) return 1;
                return 0;
            });

            return internalArray;
        }

        public static Select(array: any[], selectValue: (value) => any): any[] {
            var internalArray = [];
            for (var i = 0; i < array.length; i++) internalArray.push(selectValue(array[i]));
            return internalArray;
        }

        public static SelectMany(array: any[], selectValue: (value) => any[]): any[] {
            var result = [];
            for (var i = 0; i < array.length; i++) {
                var internalArray = selectValue(array[i]);
                for (var j = 0; j < internalArray.length; j++) {
                    result.push(internalArray[j]);
                }
            }

            return result;
        }

        public static Clear(array: any[]): void {
            while (array.length > 0) {
                array.pop();
            }
        }

        public static AddRange(array: any[], rangeArray: any[]) {
            for (var i = 0; i < rangeArray.length; i++) {
                array.push(rangeArray[i]);
            }
        }

        public static Contains(array: any[], value: any) {
            for (var i = 0; i < array.length; i++) {
                if (array[i] === value) return true;
            }
            return false;
        }
    }
}