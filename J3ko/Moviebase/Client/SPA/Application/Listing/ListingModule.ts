﻿module Application.Listing {
    export class ListingModule extends Core.AngularModule {
        constructor(moduleName: string) {
            super(moduleName);
        }

        public ConfigurePages(pageConfiguration: Core.PageConfiguration): void {

            pageConfiguration.RootViewsFolder = "SPA/Application/Listing/Views/";

            var listing = new Core.Page("Main.Listing", "/Listing", ListingViewModel, "ListingView.html");
            listing.AddSiblingPage(new Core.Page("SideBar", null, ListingSideBarViewModel, "ListingSideBar.html"));

            pageConfiguration.RegisterPage(listing);

            super.ConfigurePages(pageConfiguration);
        }
    }
}