﻿module Application.Listing {
    export class ListingSideBarViewModel extends Core.PageViewModel {

        public static Dependencies: string[] = [Core.AngularServices.$scope]
        constructor($scope) {
            this.InitScope();
            this.Load();

            super($scope)
        }

        private Load(): void {
        }
        private InitScope(): void {
        }
    }
}