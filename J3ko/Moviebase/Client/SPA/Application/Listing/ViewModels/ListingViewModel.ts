﻿module Application.Listing {
    export class ListingViewModel extends Core.PageViewModel {

        private _moviebaseApi: Shared.IMoviebaseApi;
        private _titles: ListingTitleModel[];

        public get Titles(): ListingTitleModel[] { return this._titles; }

        public static Dependencies: string[] = [Core.AngularServices.$scope, "MoviebaseApi"]
        constructor($scope, moviebaseApi: Shared.IMoviebaseApi) {
            this._moviebaseApi = moviebaseApi;

            this.InitScope();
            this.Load();

            super($scope)
        }

        private Load(): void {
            this.loadTitles();
        }

        private loadTitles(): void {
            this._moviebaseApi.Query("title")
                .then((rsp) => {
                    this._titles = rsp.Items;
                });
        }

        private InitScope(): void {
            this._titles = [];
        }
    }
}