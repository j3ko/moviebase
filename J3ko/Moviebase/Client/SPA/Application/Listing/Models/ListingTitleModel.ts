﻿module Application.Listing {
    export class ListingTitleModel {
        TitleId: number;
        TitleName: string;
        CategoryId: number;
        SmallCoverUrl: string;
        Released: number;
        //Genres: GenreTitleModel;
    }
}