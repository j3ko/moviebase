﻿module Application.Shared {
    export class SharedModule extends Core.AngularModule {
        constructor(moduleName: string) {
            super(moduleName);

            var applicationMetadata = (<any>window).ApplicationMetadata;

            if (applicationMetadata == null) throw "No Application Metadata found";

            this.RegisterConstant("ApplicationMetadata", applicationMetadata);
        }

        public ConfigureFilters(filterConfiguration: Core.FilterConfiguration): void {
            super.ConfigureFilters(filterConfiguration);
        }

        public ConfigureDirectives(directiveConfiguration: Core.DirectiveConfiguraton): void {
            //directiveConfiguration.RootViewsFolder = "SPA/Application/Shared/Directives/";

            super.ConfigureDirectives(directiveConfiguration);
        }

        public ConfigureServices(serviceConfiguration: Core.ServiceConfiguration): void {
            serviceConfiguration
                .RegisterService("Base64Service", Base64Service)
                .RegisterService("UUIDService", UUIDService)
                .RegisterService("NavigationService", NavigationService)
                .RegisterService("MoviebaseSocket", MoviebaseSocket)
                .RegisterService("MoviebaseApi", MoviebaseApi)
                .RegisterService("StorageService", StorageService)
                .RegisterService("UserService", UserService);

            super.ConfigureServices(serviceConfiguration);
        }
    }
}