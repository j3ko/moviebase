﻿module Application.Shared {
    export interface IApplicationMetadata {
        IsDebugMode: boolean;
        WebApiUrl: string;
        TitleImagePath: string;
    }
}