﻿module Application.Shared {
    export interface IUserService {
        Login(username: string, password: string): void;
        Logout(): void;
    }

    export class UserService implements IUserService {
        private _applicationMetadata: IApplicationMetadata;
        private _base64Service: IBase64Service;
        private _moviebaseApi: IMoviebaseApi;
        private _q: any;
        private _log: ng.ILogService;
        private _storageService: IStorageService;

        public static Dependencies: string[] = [Core.AngularServices.$q, Core.AngularServices.$log, "MoviebaseApi", "StorageService", "ApplicationMetadata", "Base64Service"];
        constructor($q, $log: ng.ILogService, moviebaseApi: Shared.IMoviebaseApi, storageService: Shared.IStorageService, applicationMetadata: Shared.IApplicationMetadata, base64Service: Shared.IBase64Service) {
            this._applicationMetadata = applicationMetadata;
            this._base64Service = base64Service;
            this._moviebaseApi = moviebaseApi;
            this._q = $q;
            this._log = $log;
            this._storageService = storageService;
        }


        public Login(username: string, password: string): void {
            var deferred = this._q.defer();

            $.ajax(this._applicationMetadata.WebApiUrl + "Token", {
                type: "POST",
                data: {
                    grant_type: "password",
                    username: username,
                    password: password
                }
                //xhrFields: {
                //    withCredentials: true
                //}
            })
            .done((data) => {
                this._log.debug(data)

                if (data.userName && data.access_token) {
                    this._moviebaseApi.SetAuthenticationToken(data.access_token);
                    deferred.resolve("Login Success");
                }
                else {
                    deferred.reject("Login Failed");
                }
            })
            .fail((data) => {
                this._log.debug(data);

                if (data && data.responseJSON && data.responseJSON.error_description) {
                    deferred.reject(data.responseJSON.error_description);
                }
                else {
                    deferred.reject("Login Failed");
                }
            });
        }

        public Logout(): void {
            this._moviebaseApi.ClearAuthenticationToken();
        }

    }
}