﻿module Application.Shared {
    export interface INavigationService {
        Navigate(state: string): void;
        Navigate(state: string, stateParams: any): void;
        NavigateReplaceHistory(state: string): void;
        NavigateReplaceHistory(state: string, stateParams: any): void;
        Reload(): void;
        GetStateParam(name: string): string;
        GetStateParams(): ng.ui.IStateParamsService;
        GetCurrentState(): string;
    }

    export class NavigationService implements INavigationService {
        private _state: ng.ui.IStateService;
        private _stateParams: ng.ui.IStateParamsService;

        public static Dependencies = [Core.AngularServices.$state, Core.AngularServices.$stateParams];
        constructor($state, $stateParams) {
            this._state = $state;
            this._stateParams = $stateParams;
        }


        public Navigate(state: string): void;
        public Navigate(state: string, stateParams: any): void;
        public Navigate(state: string, stateParams?: any): void {
            if (stateParams) {
                this._state.go(state, stateParams);
            }
            else {
                this._state.go(state);
            }
        }

        public NavigateReplaceHistory(state: string): void;
        public NavigateReplaceHistory(state: string, stateParams: any): void;
        public NavigateReplaceHistory(state: string, stateParams?: any): void {
            if (stateParams) {
                this._state.go(state, stateParams, { location: "replace" });
            }
            else {
                this._state.go(state, null, { location: "replace" });
            }
        }

        public Reload(): void {
            (<any>this._state).reload();
        }

        public GetStateParam(name: string): string {
            //return <string>this._routeService.current.params[name];
            return this._stateParams[name];
        }

        public GetStateParams(): ng.ui.IStateParamsService {
            return angular.copy(this._stateParams);
        }

        public GetCurrentState(): string {
            return this._state.current.name;
        }
    }
}