﻿module Application.Shared {
    export interface IMoviebaseSocket {
        IsConnected: boolean;
        OnDisconnect(fn): void;
        Connect(): ng.IPromise<any>;
        Join(room): ng.IPromise<any>;
        Leave(room): ng.IPromise<any>;
        OnPrivate(eventName, callback): void;
        OnPublic(eventName, callback): void
    }

    export class MoviebaseSocket implements IMoviebaseSocket {
        private _rootScope: ng.IRootScopeService;
        private _q: ng.IQService;
        private _timeout: ng.ITimeoutService;

        private _eventHubName = "MoviebaseEventHub";
        private _publicChannelName = "public";
        private _rawApiEndPoint: string;

        private _connection;
        private _proxy;
        private _isConnecting = false;
        private _eventMap = {};

        public get IsConnected(): boolean { return this._proxy.connection.state === (<any>$).signalR.connectionState.connected; }

        public static Dependencies: string[] = ['$rootScope', '$q', '$timeout', 'ApplicationMetadata'];
        constructor($rootScope, $q, $timeout, applicationMetadata: IApplicationMetadata) {
            this._rootScope = $rootScope;
            this._q = $q;
            this._timeout = $timeout;
            this._rawApiEndPoint = applicationMetadata.WebApiUrl.replace(/\/$/, '');

            this._connection = (<any>$).hubConnection(this._rawApiEndPoint);
            this._proxy = this._connection.createHubProxy(this._eventHubName);

            this._connection.disconnected(() => {
                this._connection.log('Connection closed. Retrying...');
                this._timeout(() => { this.Connect(); }, 5000);
            });
        }


        //#region Public Methods
        public OnDisconnect(fn): void {
            this.RegisterDisconnect(fn, null);
        }


        public Connect(): ng.IPromise<any> {
            var connectionPromise = this._q.defer();

            if (!this.IsConnected && !this._isConnecting) {
                this._isConnecting = true;

                this.RegisterEvent('privateEvent',() => {
                    this._rootScope.$emit('event:moviebase-private-event', arguments);
                });

                this.RegisterEvent('publicEvent',() => {
                    this._rootScope.$emit('event:moviebase-public-event', arguments);
                });

                this._connection
                    .start()
                    .done(e => {
                    this._isConnecting = false;
                    this._proxy.invoke('Subscribe', this._publicChannelName);
                    connectionPromise.resolve(e);
                })
                    .fail(e => {
                    this._isConnecting = false;
                    console.log('error connecting to websocket');
                    connectionPromise.reject(e);
                });
            }

            return connectionPromise.promise;
        }

        public Join(room): ng.IPromise<any> {
            var deferred = this._q.defer();

            if (!this.IsConnected) {
                this.Connect()
                    .then(e => {
                    this._proxy.invoke('Subscribe', room);
                    deferred.resolve(e);
                }, e => {
                        deferred.reject(e);
                    });
            }
            else {
                this._proxy.invoke('Subscribe', room);
                deferred.resolve('subscribed');
            }

            return deferred.promise;
        }

        public Leave(room): ng.IPromise<any> {
            var deferred = this._q.defer();

            if (!this.IsConnected) {
                this.Connect()
                    .then(e => {
                    this._proxy.invoke('Unsubscribe', room);
                    deferred.resolve(e);
                }, e => {
                        deferred.reject(e);
                    });
            }
            else {
                this._proxy.invoke('Unsubscribe', room);
                deferred.resolve('unsubscribed');
            }

            return deferred.promise;
        }

        public OnPrivate(eventName, callback): void {
            var register = (e, c) => {
                this._rootScope.$on('event:moviebase-private-event',(event, data) => {
                    if (data && data[0] == e) {
                        callback.apply(callback, data);
                    }
                });
            }

            if (!this.IsConnected) {
                this.Connect()
                    .then(e => {
                    register(eventName, callback);
                }, err => {
                        // TODO: do something when failed
                    });
            }
            else {
                register(eventName, callback);
            }
        }

        public OnPublic(eventName, callback): void {
            var register = (e, c) => {
                this._rootScope.$on('event:moviebase-public-event',(event, data) => {
                    if (data && data[0] == e) {
                        callback.apply(callback, data);
                    }
                });
            }

            if (!this.IsConnected) {
                this.Connect()
                    .then((e) => {
                    register(eventName, callback);
                }, err => {
                        // TODO: do something when failed
                    });
            }
            else {
                register(eventName, callback);
            }
        }
        //#endregion


        //#region Private Methods
        private RegisterEvent(eventType, callback): void {
            var self = this;

            if (self._eventMap[eventType]) {
                self._proxy.off(eventType, this._eventMap[eventType]);
            }

            self._eventMap[eventType] = function () {
                var args = arguments;

                self._rootScope.$apply(function () {
                    if (callback) {
                        callback.apply(callback, args);
                    }
                });
            };

            self._proxy.on(eventType, this._eventMap[eventType]);
        }

        private RegisterDisconnect(fn, interval): void {
            var self = this;

            self._connection.disconnected(function () {
                if (typeof fn == 'function') {
                    self._timeout(fn, interval);
                }
            });
        }
        //#endregion

    }
}