﻿module Application.Shared {
    export interface IStorageService {
        Persist(key: string, value: any): void;
        PersistInSession(key: string, value: any): void;
        Retrieve(key: string): any;
        RetrieveFromSession(key: string): any;
        Remove(key: string): void;
        RemoveFromSession(key: string): void;
    }

    export class StorageService implements IStorageService {
        public Persist(key: string, value: any): void {
            if (value != null) {
                var storeValue = {
                    Item: value
                };
                localStorage.setItem(key, JSON.stringify(storeValue));
            }
            else localStorage.setItem(key, null);
        }

        public PersistInSession(key: string, value: any): void {
            if (value != null) {
                var storeValue = {
                    Item: value
                };
                sessionStorage.setItem(key, JSON.stringify(storeValue));
            }
            else sessionStorage.setItem(key, null);
        }

        public Retrieve(key: string): any {
            var value = localStorage.getItem(key);
            if (value == null) return null;
            var parsedValue = JSON.parse(value);
            return parsedValue.Item;
        }

        public RetrieveFromSession(key: string): any {
            var value = sessionStorage.getItem(key);
            if (value == null) return null;
            var parsedValue = JSON.parse(value);
            return parsedValue.Item;
        }

        public Remove(key: string): void {
            localStorage.removeItem(key);
        }

        public RemoveFromSession(key: string): void {
            sessionStorage.removeItem(key);
        }
    }
} 