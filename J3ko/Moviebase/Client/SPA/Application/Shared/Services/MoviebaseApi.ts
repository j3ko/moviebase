﻿module Application.Shared {
    export interface IMoviebaseApi {
        Query(path: string): ng.IPromise<any>
        Command(command: string, data): ng.IPromise<any>
        SetAuthenticationToken(accessToken: string): void
        ClearAuthenticationToken(): void
        IsLoggedIn: boolean;
    }

    export class MoviebaseApi implements IMoviebaseApi {
        private _http: ng.IHttpService;
        private _q: ng.IQService;
        private _rootScope: ng.IRootScopeService;
        private _log: ng.ILogService;
        private _uuid: IUUIDService;
        private _moviebaseSocket: IMoviebaseSocket;
        private _storageService: IStorageService;
        private _webApiUrl: string;
        private _tokenType = "Bearer";
        private _accessToken: string;

        public get IsLoggedIn(): boolean { return this._accessToken !== null; }

        public static Dependencies: string[] = ["$http", "$q", "$rootScope", "$log", "UUIDService", "MoviebaseSocket", "ApplicationMetadata", "StorageService"];
        constructor($http, $q, $rootScope, $log, uuidService: IUUIDService, moviebaseSocket: IMoviebaseSocket, applicationMetadata: IApplicationMetadata, storageService: IStorageService) {
            this._http = $http;
            this._q = $q;
            this._rootScope = $rootScope;
            this._log = $log;
            this._uuid = uuidService;
            this._moviebaseSocket = moviebaseSocket;
            this._webApiUrl = applicationMetadata.WebApiUrl.replace(/\/$/, '') + "/api";
            this._storageService = storageService;
            this._accessToken = this._storageService.Retrieve(this._tokenType);

            this.SetAuthenticationToken(this._accessToken);
        }

        public Query(path: string): ng.IPromise<any> {
            var self = this, deferred = self._q.defer();
            self._log.debug("Query: " + path);

            self._http({
                method: 'GET',
                url: self._webApiUrl + '/' + path,
                headers: {
                    'Cache-Control': 'no-cache, no-store, must-revalidate',
                    'Pragma': 'no-cache',
                    'Expires': '0'
                },
                withCredentials: self._accessToken !== null
            }).success((data, status, headers, config) => {
                if (status === 401) { // Unauthorized
                    var msg = data ? data['Message'] : "Unauthorized Access";
                    deferred.reject({ msg: msg, status: status });
                    // redirect to login page?
                }
                self._log.debug(data);

                deferred.resolve(data);
            }).error((data, status, headers, config) => {
                self._log.debug(data);
                var msg = data ? data['Message'] : "Error querying data";
                deferred.reject({ msg: msg, status: status });
            });

            return deferred.promise;
        }

        public Command(command: string, data): ng.IPromise<any> {
            var self = this,
                deferred = this._q.defer(),
                cmd = {};

            if (typeof command == 'string' || <any>command instanceof String) {
                jQuery.extend(cmd, data);
                cmd['CommandName'] = command;
            }
            else {
                jQuery.extend(cmd, command);
            }

            var uuid = self._uuid.Generate();
            cmd['CommandId'] = uuid;

            self._moviebaseSocket.OnDisconnect(deferred.reject);

            self._moviebaseSocket.OnPrivate((<any>cmd).CommandName + '.complete.' + uuid, function (commandName, data) {
                if (data.CommandId == uuid) {
                    self._log.debug(data);
                    if (data.IsValid == false) {
                        deferred.reject(data);
                        self._moviebaseSocket.Leave(uuid);
                    }
                    else {
                        deferred.resolve(data);
                        self._moviebaseSocket.Leave(uuid);
                    }
                }
            })

            self._moviebaseSocket.OnPrivate((<any>cmd).CommandName + '.error.' + uuid, function (commandName, data) {
                if (data.CommandId == uuid) {
                    self._log.debug(data);
                    deferred.reject(data);
                    self._moviebaseSocket.Leave(uuid);
                }
            })

            self._log.debug(cmd);

            self._moviebaseSocket
                .Join(uuid)
                .then((e) => {
                    self._log.debug("joined: " + uuid);
                    self.SubmitCommand(cmd, deferred);
                }, (err) => {
                    self._log.debug("Failed to join command channel");
                    self._log.debug(err);
                });

            return deferred.promise;
        }

        public SetAuthenticationToken(accessToken: string): void {
            this._accessToken = accessToken;
            this._http.defaults.headers.common.Authorization = this._tokenType + " " + this._accessToken;
            this._storageService.Persist(this._tokenType, accessToken);
        }

        public ClearAuthenticationToken(): void {
            this._accessToken = null;
            this._http.defaults.headers.common.Authorization = null;
            this._storageService.Remove(this._tokenType);
        }

        private SubmitCommand(cmd, deferred): void {
            var self = this;

            self._http({
                method: 'POST',
                url: self._webApiUrl + '/command',
                data: cmd,
                headers: {
                    'Cache-Control': 'no-cache, no-store, must-revalidate',
                    'Pragma': 'no-cache',
                    'Expires': '0'
                },
                withCredentials: self._accessToken !== null
            })
            .success((data, status, headers, config) => {
                self._log.debug(data);

                if (status == 200) {
                    // do something?
                }
                else if (status == 401) { // Unauthorized
                    var msg = data ? data['Message'] : "Unauthorized Access"
                    deferred.reject({ msg: msg, status: status, response: undefined });
                    // redirect to login page?
                }
            })
            .error((data, status, headers, config) => {
                self._log.debug(data);

                var msg = data ? data['Message'] : "Error sending command";
                if (status == 400) {
                    deferred.reject({ msg: msg, status: status, response: data, ModelState: data && data['ModelState'] ? data.ModelState : null });
                }
                else {
                    deferred.reject({ msg: msg, status: status, response: undefined });
                }
                // log it?
            });
        }
    }
}