﻿module Application.Shared {
    export interface IBase64Service {
        Encode(text: string): string;
        Decode(text: string): string;
    }

    export class Base64Service implements IBase64Service {
        private _base64: JQueryBase64Static;

        constructor() {
            this._base64 = $.base64;
        }

        public Encode(text: string): string {
            return this._base64.encode(text);
        }

        public Decode(text: string): string {
            return this._base64.decode(text);
        }
    }
} 