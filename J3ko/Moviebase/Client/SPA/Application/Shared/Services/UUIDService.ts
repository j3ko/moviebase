﻿module Application.Shared {
    export interface IUUIDService {
        Generate(): string;
    }

    export class UUIDService implements IUUIDService {
        private _uuid: UUID.UUIDStatic;

        constructor() {
            this._uuid = UUID;
        }

        public Generate(): string {
            return this._uuid.generate();
        }
    }
} 