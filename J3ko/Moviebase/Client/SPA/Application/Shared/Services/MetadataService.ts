﻿module Application.Shared {
    export interface IMetadataService {
        WebApiUrl: string;
    }

    export class MetadataService implements IMetadataService {
        private _applicationMetadata: ApplicationMetadata;

        public get WebApiUrl(): string { return this._applicationMetadata.WebApiUrl; }

        constructor() {
            this.Initialize();
        }

        private Initialize(): void {
            var applicationMetadata = (<any>window).ApplicationMetadata;

            if (applicationMetadata == null) throw "No Application Metadata found";

            this._applicationMetadata = applicationMetadata;
        }
    }

    class ApplicationMetadata {
        public WebApiUrl: string;
    }
}