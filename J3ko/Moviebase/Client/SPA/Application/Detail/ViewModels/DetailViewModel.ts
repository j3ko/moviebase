﻿module Application.Detail {
    export class DetailViewModel extends Core.PageViewModel {

        private _title: DetailModel;

        private _navigationService: Shared.INavigationService;
        private _moviebaseApi: Shared.IMoviebaseApi;

        public get Title(): DetailModel { return this._title }

        public static Dependencies: string[] = [Core.AngularServices.$scope, "UserService", "NavigationService", "MoviebaseApi"]
        constructor($scope, userService: Shared.IUserService, navigationService: Shared.INavigationService, moviebaseApi: Shared.IMoviebaseApi) {
            this._navigationService = navigationService;
            this._moviebaseApi = moviebaseApi;

            this.InitScope();
            this.Load();

            super($scope)
        }

        public InitScope(): void {
            this._title = new DetailModel();
        }

        public Load(): void {
            var id = this._navigationService.GetStateParam("titleId");

            this._moviebaseApi
                .Query("title/" + id)
                .then((rsp) => {
                this._title = rsp;
            });
        }

    }
}