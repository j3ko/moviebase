﻿module Application.Detail {
    export class DetailSideBarViewModel extends Core.PageViewModel {

        private _navigationService: Shared.INavigationService;
        private _moviebaseApi: Shared.IMoviebaseApi;
        private _suggestions: SuggestionModel[];

        public get Suggestions(): SuggestionModel[] { return this._suggestions; }

        public static Dependencies: string[] = [Core.AngularServices.$scope, "NavigationService", "MoviebaseApi"]
        constructor($scope, navigationService: Shared.INavigationService, moviebaseApi: Shared.IMoviebaseApi) {
            this._navigationService = navigationService;
            this._moviebaseApi = moviebaseApi;

            this.InitScope();
            this.Load();

            super($scope)
        }

        private InitScope(): void {
            this._suggestions = [];
        }

        private Load(): void {
            var id = this._navigationService.GetStateParam("titleId");

            this._moviebaseApi
                .Query('title/' + id + '/suggestion')
                .then((rsp) => { this._suggestions = rsp.Items; });
        }
    }
}