﻿module Application.Detail {
    export class SuggestionModel {
        TitleId: number;
        TitleName: string;
        CategoryId: number;
        SmallCoverUrl: string;
        Released: number;
        //Genres: GenreTitleModel;
    }
}