﻿module Application.Detail {
    export class DetailModel {
        TitleId: number;
        TitleName: string;
        CategoryId: number;
        CoverUrl: string;
        Released: number;
        //Genres: GenreTitleModel;
    }
}