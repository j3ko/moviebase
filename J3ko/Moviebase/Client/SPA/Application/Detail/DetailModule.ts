﻿module Application.Detail {
    export class DetailModule extends Core.AngularModule {
        constructor(moduleName: string) {
            super(moduleName);
        }

        public ConfigurePages(pageConfiguration: Core.PageConfiguration): void {

            pageConfiguration.RootViewsFolder = "SPA/Application/Detail/Views/";

            var detail = new Core.Page("Main.Detail", "/Detail/{titleId:[0-9]{1,8}}", DetailViewModel, "DetailView.html");
            detail.AddSiblingPage(new Core.Page("SideBar", null, DetailSideBarViewModel, "DetailSideBarView.html"));

            pageConfiguration.RegisterPage(detail);

            super.ConfigurePages(pageConfiguration);
        }
    }
}