﻿module Application.Account {
    export class LoginModel {
        username: string;
        password: string;
    }
}