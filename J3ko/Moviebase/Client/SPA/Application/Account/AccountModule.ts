﻿module Application.Account {
    export class AccountModule extends Core.AngularModule {
        constructor(moduleName: string) {
            super(moduleName);
        }

        public ConfigurePages(pageConfiguration: Core.PageConfiguration): void {

            pageConfiguration.RootViewsFolder = "SPA/Application/Account/Views/";

            var listing = new Core.Page("Account", "/Account", AccountViewModel, "Account.html");

            pageConfiguration.RegisterPage(listing);

            super.ConfigurePages(pageConfiguration);
        }
    }
}