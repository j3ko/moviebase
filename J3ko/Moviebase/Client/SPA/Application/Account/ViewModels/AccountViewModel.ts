﻿module Application.Account {
    export class AccountViewModel extends Core.PageViewModel {

        private _userService: Shared.IUserService;
        private _model: LoginModel = { username: null, password: null };

        public get Model(): LoginModel { return this._model; }

        public static Dependencies: string[] = [Core.AngularServices.$scope, "UserService"]
        constructor($scope, userService) {
            super($scope)

            this._userService = userService;
        }

        public Login(): void {
            this._userService.Login(this._model.username, this._model.password);
        }
    }
}