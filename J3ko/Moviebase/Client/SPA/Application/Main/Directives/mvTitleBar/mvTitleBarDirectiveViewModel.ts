﻿module Application.Main {
    export class mvTitleBarDirectiveViewModel extends Core.DirectiveViewModel {
        private _moviebaseApi: Shared.IMoviebaseApi;
        private _mdSidenav: ng.material.MDSidenavService;
        private _mdUtil: any;
        private _leftToggler: Function;

        public static Dependencies: string[] = [Core.AngularServices.$scope, '$mdUtil', '$mdSidenav', 'MoviebaseApi'];
        constructor($scope, $mdUtil, $mdSidenav: ng.material.MDSidenavService, moviebaseApi: Shared.IMoviebaseApi) {
            this._moviebaseApi = moviebaseApi;
            this._mdUtil = $mdUtil;
            this._mdSidenav = $mdSidenav;

            this._leftToggler = this.buildToggler('left');

            super($scope);
        }

        public RefreshSuggestions(): void {
            this._moviebaseApi.Command("refresh suggestion", {})
                .then((e) => {
                var t = e;
                }).catch(err => {
                var t = err;
                });
        }

        public ToggleLeft(): void {
            this._leftToggler();
        }

        private buildToggler(navID) {
            var self = this;

            var toggleSide = function () {
                self._mdSidenav(navID)
                    .toggle();
            };

            var debounceFn = self._mdUtil.debounce(toggleSide, 300);
            return debounceFn;
        }
    }
}