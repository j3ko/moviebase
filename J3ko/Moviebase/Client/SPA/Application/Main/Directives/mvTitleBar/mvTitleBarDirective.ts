﻿module Application.Main {
    export class mvTitleBarDirective extends Core.ComponentDirective {
        constructor() {
            super();

            this.View = "mvTitleBar/mvTitleBarDirectiveView.html";
            this.ViewModel = mvTitleBarDirectiveViewModel;
        }
    }
}