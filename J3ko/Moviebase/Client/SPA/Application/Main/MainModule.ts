﻿module Application.Main {
    export class MainModule extends Core.AngularModule {

        constructor(moduleName: string) {
            super(moduleName);

            this.RegisterModuleConfiguration(Main.MainModuleConfig);
        }

        public ConfigureDependencies(dependencyConfiguration: Core.DependencyConfiguration): void {
            dependencyConfiguration
                .RegisterDependency("TemplateModule")
                .RegisterDependency("Shared", Shared.SharedModule)
                .RegisterDependency("Account", Account.AccountModule)
                .RegisterDependency("Listing", Listing.ListingModule)
                .RegisterDependency("Detail", Detail.DetailModule);

            super.ConfigureDependencies(dependencyConfiguration);
        }

        public ConfigureDirectives(directiveConfiguration: Core.DirectiveConfiguraton): void {
            directiveConfiguration.RootViewsFolder = "SPA/Application/Main/Directives/";

            directiveConfiguration
                .RegisterDirective("mvTitleBar", mvTitleBarDirective)
            ;

            super.ConfigureDirectives(directiveConfiguration);
        }

        public ConfigurePages(pageConfiguration: Core.PageConfiguration): void {
            pageConfiguration.UnknownUrlRedirect = "/Listing";

            pageConfiguration.RootViewsFolder = "SPA/Application/Main/Views/";

            var main = new Core.Page("Main", "", MainViewModel, "MainView.html");

            pageConfiguration.RegisterPage(main);

            super.ConfigurePages(pageConfiguration);
        }
    }
}