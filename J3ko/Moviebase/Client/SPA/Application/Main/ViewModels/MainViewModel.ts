﻿module Application.Main {
    export class MainViewModel extends Core.PageViewModel {


        public static Dependencies: string[] = [Core.AngularServices.$scope, "MoviebaseSocket"]
        constructor($scope, moviebaseSocket: Shared.IMoviebaseSocket) {
            super($scope);

            moviebaseSocket.Connect();
        }
    }
}