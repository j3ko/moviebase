﻿module Application.Main {
    export class MainModuleConfig extends Core.AngularModuleConfiguration {

        public static Dependencies: string[] = [Core.AngularServices.$logProvider, Core.AngularServices.$locationProvider, "ApplicationMetadata"]
        constructor($logProvider: ng.ILogProvider, $locationProvider: ng.ILocationProvider, applicationMetadata: Shared.IApplicationMetadata) {
            super();

            $logProvider.debugEnabled(applicationMetadata.IsDebugMode);
            $locationProvider.html5Mode(true);                    
        }
    }
}