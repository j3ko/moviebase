﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Optimization;

namespace J3ko.Moviebase.Client.Bundles
{
    public class AngularTemplatesBundle : Bundle
    {
        public AngularTemplatesBundle(string moduleName, string virtualPath)
            : base(virtualPath, new AngularTemplatesTransform(moduleName))
        { }
    }

    public class AngularTemplatesTransform : IBundleTransform
    {
        protected static Minifier compiler = new Minifier();
        private readonly string _moduleName;
        public AngularTemplatesTransform(string moduleName)
        {
            _moduleName = moduleName;
        }

        public void Process(BundleContext context, BundleResponse response)
        {
            var strBundleResponse = new StringBuilder();
            strBundleResponse.Append("angular.element(document).ready(function () {");

            strBundleResponse.AppendFormat(
              "angular.module('{0}')", _moduleName);
            strBundleResponse.Append(
              ".run(['$templateCache', function(t) {");

            foreach (var file in response.Files)
            {
                var content = file.ApplyTransforms();
                content = content
                  .Replace("'", "\\'")
                  .Replace("\r\n", "");
                var path = file.IncludedVirtualPath
                  .Replace("~", "")
                  .Replace("\\", "/");
                strBundleResponse.AppendFormat(
                  "t.put('{0}','{1}');", path, content);
            }

            strBundleResponse.Append("}]);");

            strBundleResponse.Append("});");

            response.Files = new BundleFile[] { };
            response.Content = compiler.MinifyJavaScript(strBundleResponse.ToString());
            response.ContentType = "text/javascript";
        }
    }
}