﻿using J3ko.Moviebase.Domain.DTOs;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Assemblers
{
    public class GenreAssembler : MoviebaseAssembler<GenreDto>
    {
        public GenreAssembler(IDomainModel domainModel)
            : base(domainModel)
        {

        }
        protected override IQueryable<GenreDto> OnGetAll(IDomainModel domainModel)
        {
            return domainModel.MoviebaseContext.Genres.GetAll().GroupJoin(
                domainModel.MoviebaseContext.GenreTitles.GetAll(),
                a => a.GenreId,
                b => b.GenreId,
                (a, g) => g
                    .Select(x => new GenreDto
                    {
                        GenreId = a.GenreId,
                        GenreName = a.GenreName,
                        IsUsed = true
                    })
                    .DefaultIfEmpty(new GenreDto
                    {
                        GenreId = a.GenreId,
                        GenreName = a.GenreName,
                        IsUsed = false
                    }))
                .SelectMany(x => x)
                .Distinct();
        }
    }
}
