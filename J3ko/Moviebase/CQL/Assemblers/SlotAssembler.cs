﻿using J3ko.Moviebase.Domain.DTOs;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Assemblers
{
    public class SlotAssembler : MoviebaseAssembler<SlotDto>
    {
        public SlotAssembler(IDomainModel domainModel)
            : base(domainModel)
        {

        }
        protected override IQueryable<SlotDto> OnGetAll(IDomainModel domainModel)
        {
            return domainModel.MoviebaseContext.Slots.GetAll().GroupJoin(
                domainModel.MoviebaseContext.TitleGroups.GetAll(),
                a => a.SlotId,
                b => b.SlotId,
                (a, g) => g
                    .Select(x => new SlotDto
                    {
                        SlotId = a.SlotId,
                        SlotLabel = a.SlotLabel,
                        ContainerId = a.ContainerId,
                        IsUsed = true
                    })
                    .DefaultIfEmpty(new SlotDto
                    {
                        SlotId = a.SlotId,
                        SlotLabel = a.SlotLabel,
                        ContainerId = a.ContainerId,
                        IsUsed = false
                    }))
                .SelectMany(x => x)
                .Distinct();
        }
    }
}
