﻿using J3ko.Moviebase.Domain.DTOs;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Assemblers
{
    public class TitleAssembler : MoviebaseAssembler<TitleDto>
    {
        public TitleAssembler(IDomainModel domainModel)
            : base(domainModel)
        {
        }

        //protected override IQueryable<TitleDto> OnGetAll(IDomainModel domainModel)
        //{
        //    return domainModel.MovieViewContext.Titles.GetAll();
        //}

        protected override IQueryable<TitleDto> OnGetAll(IDomainModel domainModel)
        {
            string hostRoot = domainModel.HostRoot;
            string imageVirtualPath = domainModel.ImageVirtualPath;
            string docPath = hostRoot + imageVirtualPath + "/";

            return domainModel.MoviebaseContext.Titles.GetAll()
                .Select(x => new TitleDto
                {
                    TitleId = x.TitleId,
                    TitleName = x.TitleName,
                    Plot = x.Plot,
                    CategoryId = x.CategoryId,
                    ImdbId = x.ImdbId,
                    Genres = x.GenreTitles
                    .Select(a => a.Genre)
                    .Select(a => new GenreDto
                    {
                        GenreId = a.GenreId,
                        GenreName = a.GenreName
                    }),
                    Released = x.Released.Year,
                    CoverUrl = docPath + x.Cover.Name,
                    SmallCoverUrl = docPath + x.SmallCover.Name,
                    DateAdded = x.TitleGroups.Max(a => a.DateAdded),
                    TitleGroups = x.TitleGroups.Select(a => new TitleGroupDto
                    {
                        TitleGroupId = a.TitleGroupId,
                        Description = a.Description,
                        SlotId = a.SlotId,
                        SlotLabel = a.Slot.SlotLabel,
                        ContainerId = a.Slot.ContainerId,
                        ContainerName = a.Slot.Container.ContainerName,
                        MediaId = a.MediaId,
                        Episodes = a.Episodes.Select(b => new EpisodeDto
                        {
                            InstanceId = b.InstanceId,
                            InstanceName = b.Instance.InstanceName,
                            EpisodeName = b.EpisodeName
                        })
                    }),
                    TitlePeople = x.TitlePeople.Select(a => new TitlePersonDto
                    {
                        PersonId = a.PersonId,
                        CharacterName = a.CharacterName,
                        Ordinal = a.Ordinal,
                        TitleRoleId = a.TitleRoleId,
                        FullName = a.Person.FullName,
                        ThumbnailUrl = a.Person != null ? docPath + a.Person.Thumbnail.Name : null
                    }),
                    AltTitleNames = x.AltTitleNames.Select(a => a.Name)
                });
        }

    }
}
