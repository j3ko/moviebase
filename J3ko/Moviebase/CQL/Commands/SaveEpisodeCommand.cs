﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Commands
{
    public class SaveEpisodeCommand : MoviebaseCommand
    {
        public override string CommandName
        {
            get { return "save episode"; }
        }
        public int TitleId { get; set; }
        public int InstanceId { get; set; }
        public string EpisodeName { get; set; }
        public bool IsWishlist { get; set; }
        public int TitleGroupId { get; set; }
        public string ModifiedBy { get; set; }
    }
}
