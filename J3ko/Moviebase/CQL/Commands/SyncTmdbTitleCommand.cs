﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Commands
{
    public class SyncTmdbTitleCommand : MoviebaseCommand
    {
        public override string CommandName 
        { 
            get { return "sync tmdb title"; } 
        }
        public int TitleId { get; set; }
        public string ImdbId { get; set; }
        public string CoverUrl { get; set; }
        public string SmallCoverUrl { get; set; }
        public int CategoryId { get; set; }
        public string Plot { get; set; }
        public string TitleName { get; set; }
        public string ModifiedBy { get; set; }
        public List<SaveTitleGroupCommand> TitleGroups { get; set; }
    }
}
