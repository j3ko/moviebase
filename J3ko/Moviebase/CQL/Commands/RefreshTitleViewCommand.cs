﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Commands
{
    public class RefreshTitleViewCommand : MoviebaseCommand
    {
        public override string CommandName
        {
            get { return "refresh title view"; }
        }
        public int? TitleId { get; set; }
    }
}
