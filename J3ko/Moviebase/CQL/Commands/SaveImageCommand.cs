﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Commands
{
    public class SaveImageCommand : MoviebaseCommand
    {
        public override string CommandName
        {
            get { return "save image"; }
        }
        public Guid? DocumentId { get; set; }
        public string Url { get; set; }
        public string ModifiedBy { get; set; }
    }
}
