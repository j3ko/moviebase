﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Commands
{
    public class SaveTitleCommand : MoviebaseCommand
    {
        public SaveTitleCommand()
        {
            TitleGroups = new List<SaveTitleGroupCommand>();
            Genres = new List<int>();
            AltTitleNames = new List<string>();
            TitlePeople = new List<SaveTitlePersonCommand>();
        }
        public override string CommandName 
        { 
            get { return "save title"; } 
        }
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public int CategoryId { get; set; }
        public string Plot { get; set; }
        public Guid? CoverId { get; set; }
        public Guid? SmallCoverId { get; set; }
        public string ImdbId { get; set; }
        public DateTime? Synced { get; set; }
        public DateTime Released { get; set; }
        public string ModifiedBy { get; set; }
        public List<SaveTitleGroupCommand> TitleGroups { get; set; }
        public List<int> Genres { get; set; }
        public List<string> AltTitleNames { get; set; }
        public List<SaveTitlePersonCommand> TitlePeople { get; set; }
    }
}
