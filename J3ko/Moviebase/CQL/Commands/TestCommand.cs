﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Commands
{
    public class TestCommand : MoviebaseCommand
    {
        public override string CommandName
        {
            get { return "test command"; }
        }
    }
}
