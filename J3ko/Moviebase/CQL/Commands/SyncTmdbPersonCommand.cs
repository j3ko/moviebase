﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Commands
{
    public class SyncTmdbPersonCommand : MoviebaseCommand
    {
        public override string CommandName
        {
            get { return "sync tmdb person"; }
        }
        public int PersonId { get; set; }
        public string FullName { get; set; }
        public string ThumbnailUrl { get; set; }
        public int TmdbId { get; set; }
        public string ModifiedBy { get; set; }
    }
}
