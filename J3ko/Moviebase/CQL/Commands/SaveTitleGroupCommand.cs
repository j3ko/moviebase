﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Commands
{
    public class SaveTitleGroupCommand : MoviebaseCommand
    {
        public override string CommandName
        {
            get { return "save title group"; }
        }
        public int TitleGroupId { get; set; }
        public string Description { get; set; }
        public DateTime DateBurned { get; set; }
        public int MediaId { get; set; }
        public int SlotId { get; set; }
        public int TitleId { get; set; }
        public string ModifiedBy { get; set; }
        public List<SaveEpisodeCommand> Episodes { get; set; }
    }
}
