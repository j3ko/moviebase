﻿using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Commands
{
    public class SavePersonCommand : MoviebaseCommand
    {
        public override string CommandName
        {
            get { return "save person"; }
        }
        public int PersonId { get; set; }
        public string FullName { get; set; }
        public Guid? ThumbnailId { get; set; }
        public int TmdbId { get; set; }
        public string ModifiedBy { get; set; }
    }
}
