﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Commands
{
    public class SaveDocumentCommand : MoviebaseCommand
    {
        public override string CommandName
        {
            get { return "save document"; }
        }
        public Guid? DocumentId { get; set; }
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
        public string Source { get; set; }
        public string ModifiedBy { get; set; }
    }
}
