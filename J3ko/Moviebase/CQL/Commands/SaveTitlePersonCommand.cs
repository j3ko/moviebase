﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Commands
{
    public class SaveTitlePersonCommand : MoviebaseCommand
    {
        public override string CommandName 
        {
            get { return "save title person";  } 
        }
        public int TitleId { get; set; }
        public int PersonId { get; set; }
        public int Ordinal { get; set; }
        public string CharacterName { get; set; }
        public int TitleRoleId { get; set; }
    }
}
