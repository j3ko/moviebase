﻿using J3ko.Infrastructure.CQL;
using J3ko.Infrastructure.Domain;
using J3ko.Infrastructure.SignalR;
using J3ko.Moviebase.DAL;
using J3ko.Moviebase.Domain;
using J3ko.Web.Movie;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL
{
    public static class DomainModelFactory
    {
        public static IDomainModel GetDomainModel()
        {
            ISecurityContext securityContext = new MoviebaseSecurityContext("MoviebaseContext");

            IMoviebaseContext moviebaseContext = new MoviebaseContext("MoviebaseContext", securityContext);

            IMovieViewContext movieviewContext = new MovieViewContext("MovieViewContext");

            return GetDomainModel(
                securityContext,
                moviebaseContext,
                movieviewContext);
        }

        public static IDomainModel GetDomainModel(ISecurityContext securityContext, IMoviebaseContext moviebaseContext, IMovieViewContext movieViewContext)
        {
            IWebMovieContext webMovieContext = new WebMovieContext(ConfigurationManager.AppSettings["TmdbApiKey"]);

            return new DomainModel(
                securityContext,
                moviebaseContext,
                webMovieContext,
                movieViewContext,
                () => new SignalREventListener("MoviebaseEventHub"));
        }
    }
}
