﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Events
{
    public class MoviebaseCommandCompletedEvent : CommandCompletedEvent
    {
    }
}
