﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Events
{
    public class SaveTitleEvent : ICommandEvent
    {
        public Guid CommandId { get; set; }
        public string CommandName { get; set; }
        public bool IsGlobal { get; set; }

        public int TitleId { get; set; }
    }
}
