﻿using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Handlers
{
    /// <summary>
    /// Saves a person
    /// </summary>
    public class SavePersonHandler : MoviebaseCommandHandler<SavePersonCommand>
    {
        public SavePersonHandler(IDomainModel domain)
            : base(domain)
        { }

        public override void OnHandle(SavePersonCommand command)
        {
            Person person = DomainModel.MoviebaseContext.People
                .Find(x => x.PersonId == command.PersonId)
                .FirstOrDefault() ?? new Person();

            person.FullName = command.FullName;
            person.ThumbnailId = command.ThumbnailId;
            person.TmdbId = command.TmdbId;
            person.Modified = DateTime.Now;
            person.ModifiedBy = command.ModifiedBy ?? DomainModel.SecurityContext.Username;

            if (person.PersonId <= 0) 
                DomainModel.MoviebaseContext.People.Save(person);

            DomainModel.MoviebaseContext.Execute();
            
            command.PersonId = person.PersonId;
        }
    }
}
