﻿using J3ko.Infrastructure.DAL;
using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.CQL.Events;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Handlers
{
    /// <summary>
    /// Saves a title
    /// </summary>
    public class SaveTitleHandler : MoviebaseCommandHandler<SaveTitleCommand>
    {
        public SaveTitleHandler(IDomainModel domain)
            : base(domain)
        { }

        public override void OnHandle(SaveTitleCommand command)
        {
            Title title = DomainModel.MoviebaseContext.Titles
                .Find(x => x.TitleId == command.TitleId)
                .FirstOrDefault() ?? new Title();
            
            bool isCreate = title.TitleId <= 0;

            Category category = DomainModel.MoviebaseContext.Categories
                .FindSingle(x => x.CategoryId == command.CategoryId);

            List<Genre> genres = DomainModel.MoviebaseContext.Genres
                .Find(x => command.Genres.Contains(x.GenreId))
                .ToList();

            List<AltTitleName> altNames = command.AltTitleNames
                .Distinct()
                .Select(x => new AltTitleName
                {
                    Name = x
                })
                .ToList();

            IWorkUnit work = DomainModel.MoviebaseContext.CreateWorkUnit();

            title.TitleName = command.TitleName;
            title.CategoryId = command.CategoryId;
            title.Plot = command.Plot;
            title.CoverId = command.CoverId;
            title.SmallCoverId = command.SmallCoverId;
            title.ImdbId = command.ImdbId;
            title.Synced = command.Synced;
            title.Released = command.Released;
            title.Modified = DateTime.Now;
            title.ModifiedBy = command.ModifiedBy ?? DomainModel.SecurityContext.Username;

            title.AltTitleNames.Clear();
            title.AltTitleNames = altNames;

            if (isCreate)
                DomainModel.MoviebaseContext.Titles.Save(title);

            //title.TitleGroups.Clear();
            foreach (var ep in title.Episodes.ToList())
                DomainModel.MoviebaseContext.Episodes.Delete(ep);
            foreach (var tg in title.TitleGroups.ToList())
                DomainModel.MoviebaseContext.TitleGroups.Delete(tg);

            title.GenreTitles.Clear();
            title.TitlePeople.Clear();

            DomainModel.MoviebaseContext.Execute();

            title.GenreTitles = genres
                .Select(x => new GenreTitle
                {
                    TitleId = title.TitleId,
                    GenreId = x.GenreId
                })
                .ToList();

            if (command.TitleGroups != null)
            {
                foreach (SaveTitleGroupCommand group in command.TitleGroups)
                {
                    group.TitleId = title.TitleId;
                    HandleComposite(group);
                }
            }

            if (command.TitlePeople != null)
            {
                foreach (SaveTitlePersonCommand person in command.TitlePeople)
                {
                    person.TitleId = title.TitleId;
                    HandleComposite(person);
                }
            }

            //HandleComposite(new RefreshTitleViewCommand() 
            //{
            //    TitleId = title.TitleId
            //});

            Notify(new SaveTitleEvent
            {
                IsGlobal = isCreate,
                CommandId = command.CommandId,
                CommandName = command.CommandName,
                TitleId = title.TitleId
            });

            if (!IsComposite)
                work.Commit();

            command.TitleId = title.TitleId;
        }
    }
}
