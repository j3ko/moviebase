﻿using J3ko.Infrastructure.DAL;
using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.Domain;
using J3ko.Web.Movie;
using J3ko.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using J3ko.Moviebase.CQL.Events;

namespace J3ko.Moviebase.CQL.Handlers
{
    /// <summary>
    /// Syncs a Title with TMDB
    /// </summary>
    public class SyncTmdbTitleHandler : MoviebaseCommandHandler<SyncTmdbTitleCommand>
    {
        public SyncTmdbTitleHandler(IDomainModel domain)
            : base(domain)
        { }

        public override void OnHandle(SyncTmdbTitleCommand command)
        {
            IWorkUnit work = DomainModel.MoviebaseContext.CreateWorkUnit();

            Title title = DomainModel.MoviebaseContext.Titles
                .GetAll()
                .Where(x => x.TitleId == command.TitleId)
                .Include(x => x.Cover)
                .Include(x => x.SmallCover)
                .FirstOrDefault();

            ILookup<int, int> genreLookup = DomainModel.MoviebaseContext.GenreTmdbs
                .GetAll()
                .ToLookup(k => k.TmdbId, v => v.GenreId);

            WebMovie wmovie = DomainModel.WebMovieContext.TmdbRepository.Find(command.ImdbId);

            Guid? coverId = null;
            if (title != null && 
                title.Cover != null && 
                !title.UsesCoverFrom(command.CoverUrl))
            {
                coverId = title.CoverId;
            }

            if (title == null || coverId != null)
            {
                SaveImageCommand img = new SaveImageCommand() 
                {
                    DocumentId = coverId,
                    Url = command.CoverUrl, 
                    ModifiedBy = command.ModifiedBy 
                };
                HandleComposite(img);
                coverId = img.DocumentId;
            }

            Guid? smallCoverId = null;
            if (title != null && 
                title.SmallCover != null && 
                !title.UsesSmallCoverFrom(command.SmallCoverUrl))
            {
                smallCoverId = title.SmallCoverId;
            }

            if (title == null || smallCoverId != null)
            {
                SaveImageCommand img = new SaveImageCommand() 
                { 
                    DocumentId = smallCoverId,
                    Url = command.SmallCoverUrl,
                    ModifiedBy = command.ModifiedBy
                };
                HandleComposite(img);
                smallCoverId = img.DocumentId;
            }

            List<SaveTitlePersonCommand> titlePeopleCommands = new List<SaveTitlePersonCommand>();
            if (wmovie.Actors != null)
            {
                List<TitleRole> titleRoles = DomainModel.MoviebaseContext.TitleRoles.GetAll().ToList();

                foreach (WebMovieActor a in wmovie.Actors)
                {
                    SyncTmdbPersonCommand pcom = new SyncTmdbPersonCommand() 
                    {
                        TmdbId = a.TmdbId,
                        FullName = a.RealName,
                        ThumbnailUrl = a.ThumbnailUrl,
                        ModifiedBy = command.ModifiedBy
                    };
                    HandleComposite(pcom);

                    TitleRole role = titleRoles
                        .First(x => x.Description.ToUpper() == a.Role.ToString().ToUpper());

                    titlePeopleCommands.Add(new SaveTitlePersonCommand
                    {
                        CharacterName = a.CharacterName,
                        Ordinal = a.Ordinal,
                        PersonId = pcom.PersonId,
                        TitleRoleId = role != null ? role.TitleRoleId : TitleRole.Other,
                    });
                }
            }

            List<int> genres = new List<int>();
            foreach (int wgenre in wmovie.Genres)
            {
                if (genreLookup.Contains(wgenre))
                    genres.AddRange(genreLookup[wgenre]);
            }

            SaveTitleCommand titleCommand = new SaveTitleCommand
            {
                TitleId = command.TitleId,
                ImdbId = command.ImdbId,
                CategoryId = command.CategoryId,
                TitleGroups = command.TitleGroups,
                Synced = DateTime.Now,
                ModifiedBy = command.ModifiedBy,

                CoverId = coverId,
                SmallCoverId = smallCoverId,

                AltTitleNames = wmovie.AltTitles,
                Genres = genres,
                Released = wmovie.Released ?? DateTime.Now,
                Plot = command.Plot != null ? command.Plot : wmovie.Plot,
                TitleName = command.TitleName != null ? command.TitleName : wmovie.Title,
                TitlePeople = titlePeopleCommands,
            };

            HandleComposite(titleCommand);

            Notify(new SyncTmdbTitleEvent
            {
                IsGlobal = false,
                CommandId = command.CommandId,
                CommandName = command.CommandName,
                TitleId = titleCommand.TitleId
            });

            if (!IsComposite) 
                work.Commit();

            command.TitleId = titleCommand.TitleId;
        }

    }
}
