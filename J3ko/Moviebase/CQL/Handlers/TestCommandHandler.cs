﻿using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Handlers
{
    public class TestCommandHandler: MoviebaseCommandHandler<TestCommand>
    {
        public TestCommandHandler(IDomainModel domain)
            : base(domain)
        { }

        public override void OnHandle(TestCommand command)
        {
        }
    }
}
