﻿using J3ko.Common;
using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Handlers
{
    /// <summary>
    /// Saves an image
    /// </summary>
    public class SaveImageHandler : MoviebaseCommandHandler<SaveImageCommand>
    {
        public SaveImageHandler(IDomainModel domain)
            : base(domain)
        { }

        public override void OnHandle(SaveImageCommand command)
        {
            Document doc = DomainModel.MoviebaseContext.Documents
                .Find(x => x.StreamId == command.DocumentId)
                .FirstOrDefault();

            if (!string.IsNullOrWhiteSpace(command.Url))
            {
                byte[] img = WebMethods.GetImage(command.Url);

                string fileName = command.Url.GetFileName();
                string fileExt = fileName.GetFileExtension();

                var docCom = new SaveDocumentCommand()
                {
                    DocumentId = command.DocumentId,
                    FileContent = img,
                    Source = command.Url,
                    FileName = Guid.NewGuid() + fileExt,
                    ModifiedBy = command.ModifiedBy
                };

                HandleComposite(docCom);

                command.DocumentId = docCom.DocumentId;
            }


        }
    }
}
