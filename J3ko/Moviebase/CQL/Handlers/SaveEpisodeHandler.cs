﻿using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Handlers
{
    /// <summary>
    /// Saves an episode
    /// </summary>
    public class SaveEpisodeHandler : MoviebaseCommandHandler<SaveEpisodeCommand>
    {
        public SaveEpisodeHandler(IDomainModel domain)
            : base(domain)
        { }

        public override void OnHandle(SaveEpisodeCommand command)
        {
            bool titleExists = DomainModel.MoviebaseContext.Titles
                .GetAll()
                .Any(x => x.TitleId == command.TitleId);

            bool groupExists = DomainModel.MoviebaseContext.TitleGroups
                .GetAll()
                .Any(x => x.TitleGroupId == command.TitleGroupId);

            Episode episode = DomainModel.MoviebaseContext.Episodes
                .Find(x => x.TitleId == command.TitleId && 
                           x.InstanceId == command.InstanceId)
                .FirstOrDefault() ?? new Episode();

            episode.TitleId = command.TitleId;
            episode.InstanceId = command.InstanceId;
            episode.EpisodeName = command.EpisodeName;
            episode.IsWishlist = command.IsWishlist;
            episode.TitleGroupId = command.TitleGroupId;
            episode.Modified = DateTime.Now;
            episode.ModifiedBy = command.ModifiedBy ?? DomainModel.SecurityContext.Username;

            DomainModel.MoviebaseContext.Episodes.Save(episode);

            DomainModel.MoviebaseContext.Execute();
        }
    }
}
