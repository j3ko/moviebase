﻿using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Handlers
{
    /// <summary>
    /// Saves a TitlePerson
    /// </summary>
    public class SaveTitlePersonHandler : MoviebaseCommandHandler<SaveTitlePersonCommand>
    {
        public SaveTitlePersonHandler(IDomainModel domain)
            : base(domain)
        { }

        public override void OnHandle(SaveTitlePersonCommand command)
        {
            bool titleExists = DomainModel.MoviebaseContext.Titles
                .GetAll()
                .Any(x => x.TitleId == command.TitleId);

            TitlePerson tperson = DomainModel.MoviebaseContext.TitlePeople
                .Find(x => x.TitleId == command.TitleId && 
                           x.PersonId == command.PersonId && 
                           x.Ordinal == command.Ordinal)
                .FirstOrDefault() ?? new TitlePerson();

            tperson.TitleId = command.TitleId;
            tperson.PersonId = command.PersonId;
            tperson.Ordinal = command.Ordinal;
            tperson.CharacterName = command.CharacterName;
            tperson.TitleRoleId = command.TitleRoleId;

            DomainModel.MoviebaseContext.TitlePeople.Save(tperson);

            DomainModel.MoviebaseContext.Execute();
        }
    }
}
