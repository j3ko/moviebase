﻿using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Handlers
{
    /// <summary>
    /// Saves a TitleGroup
    /// </summary>
    public class SaveTitleGroupHandler : MoviebaseCommandHandler<SaveTitleGroupCommand>
    {
        public SaveTitleGroupHandler(IDomainModel domain)
            : base(domain)
        { }

        public override void OnHandle(SaveTitleGroupCommand command)
        {
            bool titleExists = DomainModel.MoviebaseContext.Titles
                .GetAll()
                .Any(x => x.TitleId == command.TitleId);

            bool isCreate = false;

            TitleGroup group = DomainModel.MoviebaseContext.TitleGroups
                .Find(x => x.TitleGroupId == command.TitleGroupId)
                .FirstOrDefault();



            if (group == default(TitleGroup))
            {
                isCreate = true;
                group = new TitleGroup();
            }

            group.TitleId = command.TitleId;
            group.Description = command.Description;
            if (isCreate)
            {
                group.DateAdded = DateTime.Now;
                group.DateBurned = DateTime.Now;
            }
            group.MediaId = command.MediaId;
            group.SlotId = command.SlotId;
            group.Modified = DateTime.Now;
            group.ModifiedBy = command.ModifiedBy ?? DomainModel.SecurityContext.Username;

            DomainModel.MoviebaseContext.TitleGroups.Save(group);

            DomainModel.MoviebaseContext.Execute();

            foreach (SaveEpisodeCommand episode in command.Episodes)
            {
                episode.TitleId = command.TitleId;
                episode.TitleGroupId = group.TitleGroupId;
                HandleComposite(episode);
            }

            command.TitleGroupId = group.TitleGroupId;
        }
    }
}
