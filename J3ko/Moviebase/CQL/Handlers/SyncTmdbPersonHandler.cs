﻿using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.Domain;
using J3ko.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Handlers
{
    /// <summary>
    /// Syncs a person with TMDB
    /// </summary>
    public class SyncTmdbPersonHandler : MoviebaseCommandHandler<SyncTmdbPersonCommand>
    {
        public SyncTmdbPersonHandler(IDomainModel domain)
            : base(domain)
        { }

        public override void OnHandle(SyncTmdbPersonCommand command)
        {
            Person person = DomainModel.MoviebaseContext.People
                .GetAll()
                .Where(x => x.TmdbId == command.TmdbId)
                .Include(x => x.Thumbnail)
                .FirstOrDefault() ?? new Person();
            SavePersonCommand pcom = new SavePersonCommand();

            Guid? existingThumbnail = null;

            if (person.Thumbnail != null && !person.UsesThumbnailFrom(command.ThumbnailUrl))
            {
                existingThumbnail = person.ThumbnailId;
            }

            if (person.Thumbnail == null && 
                !string.IsNullOrWhiteSpace(command.ThumbnailUrl) || 
                existingThumbnail != null)
            {
                SaveImageCommand img = new SaveImageCommand() 
                {
                    DocumentId = existingThumbnail,
                    Url = command.ThumbnailUrl,
                    ModifiedBy = command.ModifiedBy
                };
                HandleComposite(img);
                pcom.ThumbnailId = img.DocumentId;
            }

            pcom.PersonId = person.PersonId;
            pcom.FullName = command.FullName;
            pcom.ModifiedBy = command.ModifiedBy;
            pcom.TmdbId = command.TmdbId;

            HandleComposite(pcom);

            command.PersonId = pcom.PersonId;
        }
    }
}
