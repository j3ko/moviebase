﻿using J3ko.Moviebase.CQL.Assemblers;
using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.Domain;
using J3ko.Moviebase.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Handlers
{
    /// <summary>
    /// Refreshes mongo db view cache and copies images to a cache folder.
    /// </summary>
    public class RefreshTitleViewHandler : MoviebaseCommandHandler<RefreshTitleViewCommand>
    {
        public RefreshTitleViewHandler(IDomainModel domain)
            : base(domain)
        { }

        public override void OnHandle(RefreshTitleViewCommand command)
        {
            TitleAssembler tass = new TitleAssembler(DomainModel);

            if (command.TitleId != null)
            {
                TitleDto title = tass.FindSingle(x => x.TitleId == command.TitleId);
                DomainModel.MovieViewContext.Titles.Save(title);

                if (!string.IsNullOrWhiteSpace(DomainModel.ImageMirrorPath))
                {
                    MirrorAllImages();
                }
            }
            else
            {
                foreach (TitleDto title in tass.Find(x => true))
                {
                    DomainModel.MovieViewContext.Titles.Save(title);
                }

                if (!string.IsNullOrWhiteSpace(DomainModel.ImageMirrorPath))
                {
                    MirrorAllImages();
                }
            }

            DomainModel.MovieViewContext.Execute();
        }

        private void MirrorAllImages()
        {
            foreach (var img in DomainModel.MoviebaseContext.Documents.GetAll())
            {
                File.WriteAllBytes(string.Format("{0}/{1}", DomainModel.ImageMirrorPath, img.Name), img.FileStream);
            }
        }

        private void MirrorImages(RefreshTitleViewCommand command)
        {
            var c = from t in DomainModel.MoviebaseContext.Titles.GetAll().Where(x => x.TitleId == command.TitleId)
                    join d in DomainModel.MoviebaseContext.Documents.GetAll() on t.CoverId equals d.StreamId
                    select new
                    {
                        FileName = d.Name,
                        FileStream = d.FileStream
                    };

            var s = from t in DomainModel.MoviebaseContext.Titles.GetAll().Where(x => x.TitleId == command.TitleId)
                    join d in DomainModel.MoviebaseContext.Documents.GetAll() on t.SmallCoverId equals d.StreamId
                    select new
                    {
                        FileName = d.Name,
                        FileStream = d.FileStream
                    };

            var per = from t in DomainModel.MoviebaseContext.Titles.GetAll().Where(x => x.TitleId == command.TitleId)
                      join tp in DomainModel.MoviebaseContext.TitlePeople.GetAll() on t.TitleId equals tp.TitleId
                      join p in DomainModel.MoviebaseContext.People.GetAll() on tp.PersonId equals p.PersonId
                      join d in DomainModel.MoviebaseContext.Documents.GetAll() on p.ThumbnailId equals d.StreamId
                      select new
                      {
                          FileName = d.Name,
                          FileStream = d.FileStream
                      };

            var imglist = c.Union(s).Union(per).ToList();

            foreach (var img in imglist)
            {
                File.WriteAllBytes(string.Format("{0}/{1}", DomainModel.ImageMirrorPath, img.FileName), img.FileStream);
            }
        }
    }
}
