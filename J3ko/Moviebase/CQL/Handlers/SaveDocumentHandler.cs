﻿using J3ko.Infrastructure.DAL;
using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL.Handlers
{
    /// <summary>
    /// Saves a document
    /// </summary>
    public class SaveDocumentHandler : MoviebaseCommandHandler<SaveDocumentCommand>
    {
        public SaveDocumentHandler(IDomainModel domain)
            : base(domain)
        { }

        public override void OnHandle(SaveDocumentCommand command)
        {
            Document doc = DomainModel.MoviebaseContext.Documents
                .Find(x => x.StreamId == command.DocumentId)
                .FirstOrDefault() ?? new Document();

            IWorkUnit work = DomainModel.MoviebaseContext.CreateWorkUnit();

            doc.Name = command.FileName;
            doc.FileStream = command.FileContent;

            
            DocumentMetadata docMeta = DomainModel.MoviebaseContext.DocumentMetadata
                .Find(x => x.StreamId == command.DocumentId)
                .FirstOrDefault() ?? new DocumentMetadata();

            docMeta.Source = command.Source;
            docMeta.Modified = DateTime.Now;
            docMeta.ModifiedBy = command.ModifiedBy ?? DomainModel.SecurityContext.Username;

            if (doc.StreamId == Guid.Empty)
            {
                DomainModel.MoviebaseContext.Documents.Save(doc);
                DomainModel.MoviebaseContext.DocumentMetadata.Save(docMeta);
            }

            DomainModel.MoviebaseContext.Execute();

            if (!IsComposite)
                work.Commit();

            command.DocumentId = doc.StreamId != Guid.Empty ? doc.StreamId : (Guid?)null;
        }
    }
}
