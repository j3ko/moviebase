﻿using J3ko.Infrastructure.CQL;
using J3ko.Moviebase.CQL.Assemblers;
using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.CQL.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL
{
    public static class CqlStartup
    {
        public static void Initialize()
        {
            CommandContext.Initialize(typeof(SaveTitleCommand), typeof(SaveTitleHandler), typeof(TitleAssembler));
        }
    }
}
