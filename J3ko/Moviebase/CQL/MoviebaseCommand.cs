﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL
{
    public abstract class MoviebaseCommand : ICommand
    {
        public Guid CommandId { get; set; }
        public abstract string CommandName { get; }
    }
}
