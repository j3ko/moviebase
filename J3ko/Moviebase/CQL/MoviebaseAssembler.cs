﻿using J3ko.Infrastructure.CQL;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL
{
    public abstract class MoviebaseAssembler<TEntity>
        : BaseAssembler<TEntity, IDomainModel> where TEntity : class, IDto
    {
        public MoviebaseAssembler(IDomainModel domainModel)
            : base(domainModel)
        {
        }
    }
}
