﻿using J3ko.Infrastructure.CQL;
using J3ko.Infrastructure.DAL;
using J3ko.Moviebase.CQL.Events;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.CQL
{
    public abstract class MoviebaseCommandHandler<TCommand> : BaseCommandHandler<TCommand, IDomainModel> where TCommand : class, ICommand
    {
        private MoviebaseCommandCompletedEvent _completedEvent;

        public MoviebaseCommandHandler(IDomainModel domain)
            : base(domain)
        {
            _completedEvent = new MoviebaseCommandCompletedEvent();
        }
        public override void Handle(ICommand command)
        {
            if (!IsComposite)
            {
                using (DomainModel)
                {
                    try
                    {
                        base.Handle(command);
                    }
                    catch
                    {
                        if (DomainModel.MoviebaseContext.HasWork)
                        {
                            IWorkUnit work = DomainModel.MoviebaseContext.CreateWorkUnit();
                            work.Rollback();
                        }

                        throw;
                    }
                }
            }
            else
            {
                base.Handle(command);
            }
        }

        public override CommandCompletedEvent CommandResult
        {
            get { return _completedEvent; }
        }

    }
}
