﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using J3ko.Moviebase.Domain;
using J3ko.Infrastructure.DAL;
using J3ko.Moviebase.CQL;
using System.Collections.Generic;
using J3ko.Infrastructure;

namespace J3ko.Moviebase.Test
{
    [TestClass]
    public class SmokeTests
    {
        ///// <summary>
        /////Gets or sets the test context which provides
        /////information about and functionality for the current test run.
        /////</summary>
        //public TestContext TestContext { get; set; }

        //// Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //    IDomainModel dm = DomainModelFactory.GetDomainModel();
        //    TestContext.Properties.Add("DomainModel", dm);
        //    //TestContext.Properties.Add("WorkUnit", dm.MoviebaseContext.CreateWorkUnit());
        //}
        
        //// Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //    //((IWorkUnit)TestContext.Properties["WorkUnit"]).Commit();
        //    ((IDomainModel)TestContext.Properties["DomainModel"]).Dispose();
        //}

        //[TestMethod]
        //public void CanInsertTitle()
        //{
        //    IDomainModel dm = (IDomainModel)TestContext.Properties["DomainModel"];

        //    InsertTestTitle(dm);
        //}

        //[TestMethod]
        //public void CanQueryTitle()
        //{
        //    IDomainModel dm = (IDomainModel)TestContext.Properties["DomainModel"];

        //    InsertTestTitle(dm);

        //    Title t = dm.MoviebaseContext.Titles.GetAll().FirstOrDefault();

        //    Assert.IsNotNull(t);
        //}

        //[TestMethod]
        //public void CanUpdateTitle()
        //{
        //    IDomainModel dm = (IDomainModel)TestContext.Properties["DomainModel"];

        //    Title t = dm.MoviebaseContext.Titles.GetAll().OrderByDescending(x => x.TitleId).FirstOrDefault();

        //    t.TitleName = "TEST MODIFIED TITLE";

        //    dm.MoviebaseContext.Execute();
        //}

        //private void InsertTestTitle(IDomainModel dm)
        //{
        //    Category cat = dm.MoviebaseContext.Categories.GetAll().FirstOrDefault();

        //    Title test = new Title()
        //    {
        //        TitleName = "TEST GENERATED TITLE",
        //        CategoryId = cat.CategoryId,
        //        Released = DateTime.Now,
        //        Modified = DateTime.UtcNow,
        //        ModifiedBy = "Unit Test",
        //    };

        //    dm.MoviebaseContext.Titles.Save(test);

        //    test.AltTitleNames.Add(new AltTitleName
        //    {
        //        Name = "TEST GENERATED ALT NAME",
        //    });

        //    dm.MoviebaseContext.Execute();
        //}
    }
}
