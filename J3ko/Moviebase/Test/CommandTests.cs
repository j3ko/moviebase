﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using J3ko.Infrastructure.CQL;
using J3ko.Moviebase.CQL.Commands;
using J3ko.Moviebase.CQL.Handlers;
using J3ko.Moviebase.CQL;
using System.Security.Principal;
using System.Threading;
using J3ko.Moviebase.Domain;
using J3ko.Moviebase.Test.Harness;
using System.Linq;

namespace J3ko.Moviebase.Test
{
    [TestClass]
    public class CommandTests
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            CqlStartup.Initialize();
        }

        // Use TestInitialize to run code before running each test
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        [TestMethod]
        public void CanExecuteSaveTitleCommand()
        {
            //ICommandHandler handler = CommandContext.CreateHandlerImpl("save title");

            using (IDomainModel dom = TestDomainFactory.GetDomainModel())
            {
                dom.MoviebaseContext.Categories.Save(new Category
                {
                    CategoryId = 1,
                    CategoryName = "TestCategory"
                });

                dom.CommandContext.Execute(new SaveTitleCommand
                {
                    TitleName = "Command",
                    CategoryId = 1,
                    ModifiedBy = "Unit Test",
                    Released = DateTime.Now
                });

                Assert.IsNotNull(dom.MoviebaseContext.Titles.GetAll().FirstOrDefault());
            }
        }

        [TestMethod]
        public void CanExecuteSyncTitleCommand()
        {
            //ICommandHandler handler = CommandContext.CreateHandlerImpl("sync tmdb title");

            //handler.Handle(new SyncTmdbTitleCommand
            //{
            //    TitleName = "Be Cool",
            //    CategoryId = 1,
            //    ImdbId = "tt0377471",
                
            //});
        }

        [TestMethod]
        public void CanExecuteSyncTitleCommandTwice()
        {
            //ICommandHandler firstHandler = CommandContext.CreateHandlerImpl("sync tmdb title");

            //var first = new SyncTmdbTitleCommand
            //{
            //    TitleName = "Be Cool",
            //    CategoryId = 1,
            //    ImdbId = "tt0377471",
            //};

            //firstHandler.Handle(first);

            //ICommandHandler secondHandler = CommandContext.CreateHandlerImpl("sync tmdb title");

            //secondHandler.Handle(new SyncTmdbTitleCommand
            //{
            //    TitleId = first.TitleId,
            //    TitleName = "Be Cool 2",
            //    CategoryId = 1,
            //    ImdbId = "tt0377471",
            //});
        }
    }
}
