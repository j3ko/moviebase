﻿using J3ko.Infrastructure.Domain;
using J3ko.Infrastructure.Mock;
using J3ko.Moviebase.DAL.EntityFramework;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Test.Harness
{
    public class TestMoviebaseContext : MockDatabaseContext, IMoviebaseContext
    {
        public TestMoviebaseContext(ISecurityContext security)
            : base(security, typeof(TitleConfiguration))
        {
            //this.Database.CommandTimeout = 30000;
        }

        public Infrastructure.DAL.IRepository<Title> Titles
        {
            get { return new MockRepository<Title>(this); }
        }

        public Infrastructure.DAL.IRepository<Category> Categories
        {
            get { return new MockRepository<Category>(this); }
        }

        public Infrastructure.DAL.IRepository<Genre> Genres
        {
            get { return new MockRepository<Genre>(this); }
        }

        public Infrastructure.DAL.IRepository<TitleGroup> TitleGroups
        {
            get { return new MockRepository<TitleGroup>(this); }
        }

        public Infrastructure.DAL.IRepository<TitleRole> TitleRoles
        {
            get { return new MockRepository<TitleRole>(this); }
        }

        public Infrastructure.DAL.IRepository<Episode> Episodes
        {
            get { return new MockRepository<Episode>(this); }
        }

        public Infrastructure.DAL.IRepository<TitlePerson> TitlePeople
        {
            get { return new MockRepository<TitlePerson>(this); }
        }

        public Infrastructure.DAL.IRepository<Person> People
        {
            get { return new MockRepository<Person>(this); }
        }

        public Infrastructure.DAL.IRepository<Document> Documents
        {
            get { return new MockRepository<Document>(this); }
        }

        public Infrastructure.DAL.IRepository<DocumentMetadata> DocumentMetadata
        {
            get { return new MockRepository<DocumentMetadata>(this); }
        }

        public Infrastructure.DAL.IRepository<GenreTmdb> GenreTmdbs
        {
            get { return new MockRepository<GenreTmdb>(this); }
        }

        public Infrastructure.DAL.IRepository<GenreTitle> GenreTitles
        {
            get { return new MockRepository<GenreTitle>(this); }
        }

        public Infrastructure.DAL.IRepository<Media> Media
        {
            get { return new MockRepository<Media>(this); }
        }

        public Infrastructure.DAL.IRepository<Slot> Slots
        {
            get { return new MockRepository<Slot>(this); }
        }

        public Infrastructure.DAL.IRepository<Container> Containers
        {
            get { return new MockRepository<Container>(this); }
        }

        public Infrastructure.DAL.IRepository<Instance> Instances
        {
            get { return new MockRepository<Instance>(this); }
        }

        public Infrastructure.DAL.IRepository<TitleSuggestion> TitleSuggestions
        {
            get { return new MockRepository<TitleSuggestion>(this); }
        }

        public Infrastructure.DAL.IRepository<vwTitleComparisonWeights> vwTitleComparisonWeights
        {
            get { return new MockRepository<vwTitleComparisonWeights>(this); }
        }
    }
}
