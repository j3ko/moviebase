﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Test.Harness
{
    public class TestSecurityContext : ISecurityContext
    {
        public string Username
        {
            get { return "TestUser"; }
        }

        public void Dispose()
        {
        }
    }
}
