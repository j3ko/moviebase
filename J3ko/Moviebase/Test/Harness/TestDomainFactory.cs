﻿using J3ko.Infrastructure.Domain;
using J3ko.Moviebase.CQL;
using J3ko.Moviebase.DAL;
using J3ko.Moviebase.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Moviebase.Test.Harness
{
    public static class TestDomainFactory
    {
        public static IDomainModel GetDomainModel()
        {
            ISecurityContext securityContext = new TestSecurityContext();

            IMoviebaseContext moviebaseContext = new TestMoviebaseContext(securityContext);

            IMovieViewContext movieViewContext = new TestMovieViewContext();

            return DomainModelFactory.GetDomainModel(securityContext, moviebaseContext, movieViewContext);
        }
    }
}
