﻿app
	.controller('HomeController', ['$scope', '$route', '$timeout', '$routeParams', '$location', 'webApi',
		function ($scope, $route, $timeout, $routeParams, $location, webApi) {

			$scope.SetParam = function(params) {
				var cur = $location.search();
				_.each(_.keys(params), function(k) {
					cur[k] = params[k];
				});
				$location.search(cur);
			}

			$scope.AddParam = function(params) {
				var cur = $location.search();
				_.each(_.keys(params), function(k) {
					if (_.isArray(cur[k])) {
						cur[k].push(params[k]);
					}
					else if (!_.isUndefined(cur[k]) && !_.isArray(cur[k])) {
						cur[k] = [cur[k]];
						cur[k].push(params[k]);
					}
					else {
						cur[k] = params[k];
					}
				});
				$location.search(cur);
			}

			$scope.RemoveParam = function(params) {
				var cur = $location.search();
				_.each(_.keys(params), function(k) {
					if (_.isArray(cur[k])) {
						cur[k] = _.filter(cur[k], function(e) {
							return e != params[k];
						});
					}
					else if (cur[k] == params[k]) {
						delete cur[k];
					}
				});
				$location.search(cur);
			}

			$scope.GenreName = function(id) {
				var elem = _.find($scope.genres, function(e) {
					return e.GenreId == id;
				})

				return elem ? elem.GenreName || 'Unknown' : ' ';
			}

			$scope.YearName = function(id) {
				var elem = _.find($scope.years, function(e) {
					return e.YearId == id;
				})

				return elem ? elem.YearName || 'Unknown' : ' ';
			}

			function InitScope() {
				var g, y;

				if (!_.isUndefined($routeParams.genre) && !_.isArray($routeParams.genre)) {
					g = [$routeParams.genre];
				}
				else {
					g = $routeParams.genre;
				}
				if (!_.isUndefined($routeParams.year) && !_.isArray($routeParams.year)) {
					y = [$routeParams.year];
				}
				else {
					y = $routeParams.year;
				}

				$scope.pageCount = new Array(1);
				$scope.pageSize = 30;
				$scope.pageIndex = $routeParams.page || 0;
				$scope.genreFilter = _.uniq(g) || [];
				$scope.yearFilter = _.uniq(y) || [];
				$scope.searchFilter = $routeParams.search || '';
				$scope.orderFilter = $routeParams.order || 'DateAdded desc';
				$scope.hasNextPage = false;
				$scope.hasPrevPage = false;
				$scope.titles = [];
				$scope.genres = [];
			}

			function Load() {
				InitScope();
				LoadGenres();
				LoadYears();
				LoadOrders();
				LoadTitles();
			}

			function ConstructFilter(g, y, search) {
				var f;

				if (_.isArray(g) && g.length > 0) {
					f = oreq.filter('Genres').anyOr(g, '/GenreId');
				}
				if (_.isArray(y) && y.length > 0) {
					if (f) f = f.and().filter('Released').eqOr(y);
					else f = oreq.filter('Released').eqOr(y);
				}
				if (search) {
					if (f) f = f.and().filter('\'' + search + '\'').substringOf('TitleName');
					else f = oreq.filter('\'' + search + '\'').substringOf('TitleName');
				}

				return f;
			}

			function LoadTitles() {
				var filter = ConstructFilter(
					$scope.genreFilter,
					$scope.yearFilter,
					$scope.searchFilter
				);
				var select = [
					'TitleId',
					'TitleName',
					'SmallCoverUrl'
				];

				var request =
					oreq
						.request('title')
						.withTop($scope.pageSize)
						.withSkip($scope.pageIndex * $scope.pageSize)
						.withFilter(filter)
						.withSelect(select)
						.withOrderby($scope.orderFilter)
						.withInlineCount()
						.url();

				webApi
					.query(request)
					.then(function(data) {
						$scope.pageCount = new Array(Math.ceil(data.Count / $scope.pageSize));
						$scope.hasNextPage = (($scope.pageIndex + 1) * $scope.pageSize) < data.Count;
						$scope.hasPrevPage = $scope.pageIndex > 0
						//$scope.titles = data.Items;
						$scope.titles = _.nest(data.Items, 5);
						//console.log($scope.titles);
					})
			}

			function LoadGenres() {
				webApi
					.query('genre')
					.then(function(data) {
						$scope.genres = _.filter(data.Items, function (e) {
							return !!e.IsUsed;
						});
					})
			}

			function LoadYears() {
				var url = oreq
							.request('title')
							.withSelect('Released')
							.url();

				webApi
					.query(url)
					.then(function(data) {
						var allYears = _.map(data.Items, function(e) {

                            if (e.Released < 2000) {
                                return {
                                    YearId: '1',
                                    YearName: 'PRE-2000'
                                };
                            }

							return {
								YearId: e.Released,
								YearName: e.Released
							};
						});

						allYears = _.uniq(allYears, false, function(e) {
							return e.YearName;
						});

                        allYears = _.sortBy(allYears, function(e) {
                            return -e.YearId;
                        });

						$scope.years = allYears;
					});
			}

			function LoadOrders() {
				$scope.orders = [
					{ OrderId: 'TitleName asc', OrderName: 'Title' },
					{ OrderId: 'Released desc', OrderName: 'Released' }
				];
			}

			$scope.$on('$routeUpdate', function(){
				Load();
			});

			Load();
		}
	]);