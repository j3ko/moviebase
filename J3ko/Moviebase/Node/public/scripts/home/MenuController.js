app
	.controller('MenuController', ['$scope', '$location', 'auth',
		function ($scope, $location, auth) {

			$scope.logout = function() {
				auth.logout(function () {
					Load();
					$location.path('/');
				});
			}

			function InitScope() {
				$scope.user = auth.user;
			}

			function Load() {
				InitScope();
			}

			Load();
		}
	]);