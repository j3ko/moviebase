﻿app
.controller('DetailController', ['$scope', '$route', '$routeParams', '$timeout', 'webApi',
    function ($scope, $route, $routeParams, $timeout, webApi) {
        function Init() {
            $scope.title = {};
			$scope.suggestions = null;
        }

        function Load() {
            Init();

            LoadTitle($routeParams.titleId);
        }

        function LoadTitle(id) {
			var select = [
				'TitleId',
				'TitleName',
				'ImdbId',
				'Plot',
				'CoverUrl',

				'Genres/GenreId',
				'Genres/GenreName',

				'TitleGroups/TitleGroupId',
				'TitleGroups/Description',
				'TitleGroups/SlotId',
				'TitleGroups/SlotLabel',
				'TitleGroups/ContainerId',
				'TitleGroups/ContainerName',
				'TitleGroups/MediaId',

				'TitleGroups/Episodes/InstanceId',
				'TitleGroups/Episodes/InstanceName',
				'TitleGroups/Episodes/EpisodeName',

				'TitlePeople/PersonId',
				'TitlePeople/CharacterName',
				'TitlePeople/Ordinal',
				'TitlePeople/TitleRoleId',
				'TitlePeople/FullName',
				'TitlePeople/ThumbnailUrl',
			];

			var expand = [
				'Genres',
				'TitleGroups',
				'TitleGroups/Episodes',
				'TitlePeople'
			];

			var request =
				oreq
					.request('title/' + (id || 0))
					.withSelect(select)
					.withExpand(expand)
					.url();

            return webApi
                .query(request)
                .then(function(data) {
                    $scope.title = data;
                });
        }

        Load();
    }
]);