﻿app
.controller('RecordController', ['$scope', '$route', '$timeout', '$http', '$routeParams', '$timeout', '$q', 'webApi',
    function ($scope, $route, $timeout, $http, $routeParams, $timeout, $q, webApi) {

        $scope.refreshMatches = function(searchTerm) {
            if ($scope.loadingMatches) $timeout.cancel($scope.loadingMatches);

            $scope.loadingMatches = $timeout(function() {
                webApi
                .query('webmovie/' + searchTerm)
                .then(function (data) {
                    //_.each(data, function(e) {
                    //    e.Posters = _.nest(e.Posters, 2);
                    //});

                    //console.log(data);

                    $scope.matches = data.Items;
                    $scope.loadingMatches = null;
                })
                .catch(function (error) {
                    $scope.matches = [];
                    $scope.loadingMatches = null;
                });
            }, 1000)
        }

        $scope.refreshSlots = function() {
            if (!$scope.title.TitleGroups) return;
            
            //var takenSlots = {};

            _.each($scope.title.TitleGroups, function (group) {

                group.slots = _.filter($scope.slots, function (slot) {
                    return slot.ContainerId == group.ContainerId;
                });

                if (!group.SlotId || group.SlotId <= 0)
                    group.SlotId = _.first(group.slots).SlotId;
            });
        }

        $scope.setMatch = function(match, smallPosterUrl, posterUrl) {
            $scope.title.ImdbId = match.ImdbId;
            $scope.title.Plot = match.Plot;
            $scope.title.TitleName = match.Title;
            $scope.title.CoverUrl = posterUrl;
            $scope.title.SmallCoverUrl = smallPosterUrl;
        }

        $scope.addEpisode = function (group) {
            group.Episodes.push({});
        }

        $scope.removeEpisode = function(group, episode) {
            group.Episodes = _.reject(group.Episodes, function(e) { 
                return e === episode; 
            });
        }

        $scope.addGroup = function() {
            $scope.title.TitleGroups.push({
                Description: 'Default',
                MediaId: _.first($scope.media).MediaId,
                ContainerId: _.first($scope.containers).ContainerId,
                Episodes: [] 
            });

            $scope.refreshSlots();
        }

        $scope.removeGroup = function(group) {
            $scope.title.TitleGroups = _.reject($scope.title.TitleGroups, function (e) {
                return e === group;
            });
        }

        $scope.saveTitle = function(title) {
            var cmd = {
                TitleId: title.TitleId,
                ImdbId: title.ImdbId,
                CoverUrl: title.CoverUrl,
                SmallCoverUrl: title.SmallCoverUrl,
                CategoryId: title.CategoryId,
                Plot: title.Plot,
                TitleName: title.TitleName,
                TitleGroups: title.TitleGroups
            };

            webApi
            .command('sync tmdb title', cmd)
            .then(function (data) {
                console.log(data);
            });
        };

        function Init() {
            $scope.loadingMatches = null;
            $scope.matches = [];
            $scope.categories = [];
            $scope.media = [];
            $scope.allSlots = [];
            $scope.slots = [];
            $scope.containers = [];
            $scope.instances = [];
            $scope.title = {};
        }

        function Load() {
            Init();
            
            var promises = [
                LoadCategories(),
                LoadMedia(),
                LoadSlots(),
                LoadContainers(),
                LoadInstances()                
            ];

            $q.all(promises)
            .then(function (data) {
                LoadTitle($routeParams.titleId)
            })

            
        }

        function EmptyTitle() {
            $scope.title = { 
                TitleGroups: [],
                CategoryId: _.first($scope.categories).CategoryId
            };
            $scope.addGroup();

        }

        function LoadTitle(id) {
            return webApi
                .query('title/' + (id || 0))
                .then(function(data) {
                    $scope.title = data;
                    $scope.refreshSlots();
                })
                .catch(function (error) {
                    EmptyTitle();
                });
        }

        function LoadCategories() {
            return webApi
                .query('category')
                .then(function (data) {
                    $scope.categories = data.Items;
                });
        }

        function LoadMedia() {
            return webApi
                .query('media')
                .then(function (data) {
                    $scope.media = data.Items;
                });
        }

        function LoadSlots() {
            return webApi
                .query('slot')
                .then(function (data) {
                    $scope.slots = data.Items;
                });
        }

        function LoadContainers() {
            return webApi
                .query('container')
                .then(function (data) {
                    $scope.containers = data.Items;
                });
        }

        function LoadInstances() {
            return webApi
                .query('instance')
                .then(function (data) {
                    $scope.instances = data.Items;
                });
        }

        Load();
    }
]);