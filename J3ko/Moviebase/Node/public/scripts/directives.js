﻿app
.directive('head', ['$rootScope', '$compile',
	function ($rootScope, $compile) {
		return {
			restrict: 'E',
			link: function (scope, elem) {
				var html = '<link rel="stylesheet" ng-repeat="(routeCtrl, cssUrl) in routeStyles" ng-href="{{cssUrl}}" />';
				elem.append($compile(html)(scope));
				scope.routeStyles = {};
				$rootScope.$on('$routeChangeStart', function (e, next, current) {
					if (current && current.$$route && current.$$route.css) {
						if (!Array.isArray(current.$$route.css)) {
							current.$$route.css = [current.$$route.css];
						}
						angular.forEach(current.$$route.css, function (sheet) {
							delete scope.routeStyles[sheet];
						});
					}
					if (next && next.$$route && next.$$route.css) {
						if (!Array.isArray(next.$$route.css)) {
							next.$$route.css = [next.$$route.css];
						}
						angular.forEach(next.$$route.css, function (sheet) {
							scope.routeStyles[sheet] = sheet;
						});
					}
				});
			}
		};
	}
])
.directive('jsddm',
	function() {
		return {
			restrict: 'A',
			link: function (scope, elem, attrs) {
				var timeout = 500;
				var closetimer = 0;
				var ddmenuitem = 0;

				function jsddm_open() {
					jsddm_canceltimer();
					jsddm_close();
					ddmenuitem = $(this).find('ul').eq(0).css('visibility', 'visible');
				}

				function jsddm_close()
				{ if (ddmenuitem) ddmenuitem.css('visibility', 'hidden'); }

				function jsddm_timer()
				{ closetimer = window.setTimeout(jsddm_close, timeout); }

				function jsddm_canceltimer() {
					if (closetimer) {
						window.clearTimeout(closetimer);
						closetimer = null;
					}
				}

				elem.find('li').bind('mouseover', jsddm_open);
				elem.find('li').bind('mouseout', jsddm_timer);
			}

		}
	}
);