﻿var app = angular.module('app', ['ngRoute', 'ngCookies', 'j3ko.services']);
 
app
.config(['$sceProvider', '$locationProvider', '$routeProvider',
    function($sceProvider, $locationProvider, $routeProvider)
    {
        // Completely disable SCE to support IE7.
        $sceProvider.enabled(false);
        $locationProvider.html5Mode(true).hashPrefix('!');

		$routeProvider
			.when('/', {
				templateUrl: '/partials/home/grid',
				controller: 'HomeController',
				css: '/styles/single.css',
				reloadOnSearch: false
			})
			.when('/info', {
				templateUrl: '/partials/home/info',
				controller: 'InfoController',
				css: '/styles/single.css'
			})
			.when('/record', {
				templateUrl: '/partials/record/entry',
				controller: 'RecordController',
				css: '/styles/double.css'
			})
			.when('/record/:titleId', {
				templateUrl: '/partials/record/entry',
				controller: 'RecordController',
				css: '/styles/double.css'
			})
			.when('/detail/:titleId', {
				templateUrl: '/partials/detail/listing',
				controller: 'DetailController',
				css: '/styles/single.css'
			})
			.when('/detail', {
				templateUrl: '/partials/detail/listing',
				controller: 'DetailController',
				css: '/styles/single.css'
			})
			.when('/user', {
				templateUrl: '/partials/user/login',
				controller: 'LoginController',
				css: '/styles/single.css'
			});
    }
])
.run(['socket',
    function(socket) {
        var sock = socket.connect('/');

        sock.on('save title', function(data) {
            console.log(data)
        })
    }
]);