﻿app
.factory('auth', ['$rootScope', '$cookieStore', '$http',
    function ($rootScope, $cookieStore, $http) {
		var currentUser = $cookieStore.get('user') || {};

		$cookieStore.remove('user');

		function changeUser(user) {
			angular.extend(currentUser, user);
		};

		return {
			logout: function(success, error) {
				$http.post('/logout').success(function(){

					changeUser({ username: '' });
					if (success) success();
				}).error(error)
			},
			user: currentUser
		};
	}

]);