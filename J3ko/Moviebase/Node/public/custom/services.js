﻿angular
.module('j3ko.services', [])
.factory('socket', ['$rootScope', 
    function ($rootScope) {

        function Socket($rootScope) {
            var socket = null;
            var self = this;

            this.connect = function (url) {
                socket = io.connect(url);
                return self;
            }

            this.isConnected = function () { 
                return !!socket && socket.socket.connected;
            }

            this.on = function (eventName, callback) {
                if (!socket || !socket.socket) return;
			
                socket.on(eventName, function () {  
                    var args = arguments;

                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
            }

            this.emit = function (eventName, data, callback) {
                if (!socket || !socket.socket) return;
		
                socket.emit(eventName, data, function () {
                    var args = arguments;

                    $rootScope.$apply(function () {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                })
            }

            this.join = function (room) {
                socket.emit("subscribe", { room: room });
            }

            this.leave = function (room) {
                socket.emit("unsubscribe", { room: room });
            }

            this.set = function(settingName, data, callback) {
                if (!socket || !socket.socket) return;
		
                socket.set(settingName, data, function () {
                    var args = arguments;

                    $rootScope.$apply(function () {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                });
            }
        }; // Socket()

        return new Socket($rootScope);
    }
])
.factory('webApi', ['$http', '$q', 'socket',
    function ($http, $q, socket) {
        return {
            command: function (command, data) {
                var deferred = $q.defer();

                var cmd = {};

                if (typeof command == 'string' || command instanceof String) {
                    jQuery.extend(cmd, data);
                    cmd['CommandName'] = command;
                }
                else {
                    jQuery.extend(cmd, command);
                }

                var uuid = _.newGuid();
                cmd['CommandId'] = uuid;

                // hook up socketio, start listening
                var sock = socket.connect('/');
                sock.join(uuid);

                sock.on(cmd.CommandName +  '.' + uuid, function (data) {
                    if (cmd.CommandId == uuid) {
                        deferred.resolve(data);
                        sock.leave(uuid);
                    }
                });

                if (sock.isConnected()) {
                    submitCommand();
                }
                else {
                    sock.on('connect', function () {
                        submitCommand();
                    });
                }

                function submitCommand() {
                    $http({
                        method: 'POST',
                        url: '/api/command',
                        data: cmd
                    })
                    .success(function (data, status, headers, config) {
                        // Ok
                        if (status == 200) {
                            // do something?
                        }
                            // Unauthorized
                        else if (status == 401) {
                            var msg = data ? data['Message'] : "Error sending command"
                            deferred.reject({ msg: msg, status: status });
                            // redirect to login page?
                        }
                    })
                    .error(function (data, status, headers, config) {
                        var msg = data ? data['Message'] : "Error sending command"
                        deferred.reject({ msg: msg, status: status });
                        // log it?
                    });
                }

                return deferred.promise;
            },
        
            query: function (path) {
                var deferred = $q.defer();

                $http({
                    method: 'GET',
                    url: '/api/' + path
                })
                .success(function (data, status, headers, config) {                    
                    if (status == 200) { // Ok
                        // do something?
                    }
                    else if (status == 401) { // Unauthorized
                        var msg = data ? data['Message'] : "Error querying data"
                        deferred.reject({ msg: msg, status: status });
                        // redirect to login page?
                    }

                    deferred.resolve(data);
                })
                .error(function (data, status, headers, config) {
                    var msg = data ? data['Message'] : "Error querying data"
                    deferred.reject({ msg: msg, status: status });
                    // log it?
                });

                return deferred.promise;
            }
        }
    }
])

;