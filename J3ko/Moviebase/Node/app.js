
/**
 * Module dependencies.
 */

var express = require('express'),
    http = require('http'),
    passport = require('passport'),
    env = process.env.NODE_ENV || 'development',
    config = require('./config/config')[env]

var app = express();

// configure passport
require('./config/passport')(passport, config);

// configure express
require('./config/express')(app, config, passport);

// configure socket io
var server = http.createServer(app);
var io = require('socket.io').listen(server);	
require('./config/socket')(app, io);

// configure routes
require('./config/routes')(app, config, passport);

server.listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});
