﻿var _ = require('underscore'),
    url = require('url'),
    request = require('request');

var _config = {};

module.exports = function (app, config, passport) {
    _config = config;

	app.post('/logout', logout);
    app.post('/login', passport.authenticate('local', { failureRedirect: '/' }), logSuccess);
    app.get('/404', notfound);
    app.get('/', index);
	app.get(/^\/(partials\/.+)$/i, partials);
    app.all('/api*', api);
    app.get('*', index);
}

var logout = function(req, res){
	req.logout();
	res.redirect('/');
}

var logSuccess = function(req, res) {
    res.redirect('/');
}

//var catchall = function(req, res) {
//    var trim = req.params[0];
//    // remove leading slash
//    if (trim.indexOf('/') === 0)
//        trim = trim.substr(1);
//    //if (trim.slice(-1) === '/')
//    //    trim = trim + 'index';
//
//    res.render(trim, { user: _.pick(req.user || {}, 'userName') }, function(err, html) {
//        if (err) {
//            if (err.message.indexOf('Failed to lookup view') !== -1) {
//                return exports.notfound(req, res);
//            }
//            throw err;
//        }
//
//        res.send(html);
//    });
//}

var notfound = function(req, res) {
    res.status(404);

    // respond with html page
    if (req.accepts('html')) {
        res.render('404', { url: req.url });
        return;
    }

    // respond with json
    if (req.accepts('json')) {
        res.send({ error: 'Not found' });
        return;
    }

    // default to plain-text. send()
    res.type('txt').send('Not found');
}

var index = function(req, res) {
	var username = '';
	if(req.user) {
		username = req.user.userName;
	}

	res.cookie('user', JSON.stringify({
		'username': username
	}));
    res.render('index');
};

var api = function(req, res) {
    var opts = {
        method: req.method,
        url: url.resolve('http://' + _config.api_host, '/api' + req.params[0]),
        headers: req.headers,
        qs: req.query
    }
	opts.headers.host = _config.api_host;
    if (req.user) {
        opts.headers['Authorization'] = 'Bearer ' + req.user.access_token;
    }
    if (_.isObject(req.body)) {
        opts.json = req.body
    }
    //req.pipe(request(opts)).pipe(res)
    request(opts).pipe(res);
}

var partials = function(req, res) {
    res.render(req.params[0]);
};
