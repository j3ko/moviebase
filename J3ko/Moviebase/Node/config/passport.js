﻿var request = require('request'),
    url = require('url'),
    BearerStrategy = require('passport-http-bearer').Strategy,
    LocalStrategy = require('passport-local').Strategy;

var _config = {};

module.exports = function (passport, config) {
    _config = config;

    // Bearer Strategy
    passport.use(new BearerStrategy({},
        function(token, done) {
            User.findOne({ token: token }, function (err, user) {
                if (err) { 
                    return done(err); 
                }
                if (!user) { 
                    return done(null, false); 
                }
                return done(null, user, { scope: 'all' });
            });
        }
    ));

    // Local Strategy
    passport.use(new LocalStrategy({
            usernameField: 'UserName',
            passwordField: 'Password',
            passReqToCallback: true
        },
        function(req, username, password, done) {

            return webapi_authenticate(req, username, password, done);
        }
    ));

    passport.serializeUser(function(user, done) {
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        done(null, user);
    });
};

var webapi_authenticate = function (req, username, password, done) {
    var params = {};

    params['grant_type'] = 'password';
    params['username'] = username;
    params['password'] = password;

    request.post(url.resolve('http://' + _config.api_host, '/Token'),  { form: params },
        function (error, response, body) {
            if (error) { return done(error); }
            if (response.statusCode != 200) { return done(null, false); }

            return done(null, JSON.parse(body));
        }
    );
};