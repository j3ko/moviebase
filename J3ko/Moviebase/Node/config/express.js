﻿var express = require('express'),
    path = require('path'),
    MongoStore = require('connect-mongo')(express);

module.exports = function (app, config, passport) {
    // all environments
    app.set('port', process.env.PORT || 3000);
    app.set('views', path.join(__dirname, '../views'));
    app.set('view engine', 'jade');
    app.use(express.favicon(path.join(__dirname, '../public/images/favicon.ico')));
    app.use(express.logger('dev'));    
    app.use(express.json());
    app.use(express.urlencoded());
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    app.use(express.session({
        store: new MongoStore({
            url: config.mongo_url
        }),
        secret: config.cookie_secret
    }));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(require('stylus').middleware(path.join(__dirname, '../public')));
    app.use(express.static(path.join(__dirname, '../public')));
    app.use(app.router); // must be after 'static' because of the catch all '*' route

    // development only
    if ('development' == app.get('env')) {
      app.use(express.errorHandler());
    }
}