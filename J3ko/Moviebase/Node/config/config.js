﻿module.exports = {
  development: {
      api_host: 'localhost:56976',
      mongo_url: 'mongodb://localhost:27017/moviebase_dev',
      cookie_secret: '1234567890QWERTY'
  },
  qa: {
      api_host: 'moviebase-qa.j3ko.com',
      mongo_url: 'mongodb://localhost:27017/moviebase_qa',
      cookie_secret: '1234567890QWERTY'
  },
  production: {
      api_host: 'moviebase.j3ko.com',
      mongo_url: 'mongodb://localhost:27017/moviebase_prod',
      cookie_secret: '1234567890QWERTY'
  }
}