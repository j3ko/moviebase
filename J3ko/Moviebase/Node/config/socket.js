module.exports = function(app, io) {
	// Socket.io Communication
	io.sockets.on('connection', function (socket) {
		//console.log('>> client connected');
	});

    io.sockets.on('connection', function (socket) {
        socket.on('subscribe', function(data) { 
            socket.join(data.room); 
        })
        socket.on('unsubscribe', function(data) { 
            socket.leave(data.room); 
        })
    });
	
	app.post('/socket/:eventname/emit/:room', function(req, res) {
		var event = req.params.eventname;
        var room = req.params.room;
		var msg = req.body;
		//console.log('>> emit request received - event: %s; message: %s', event, JSON.stringify(msg));
		
        if (!room || room === 'global') {
            io.sockets.emit(event, msg);
        }
        else {
            io.sockets.in(room).emit(event, msg);
        }
		
		res.send(200);
	});
}