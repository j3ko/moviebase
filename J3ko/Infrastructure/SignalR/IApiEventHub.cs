﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.SignalR
{
    public interface IApiEventHub
    {
        Task Subscribe(string room);
        Task UnSubscribe(string room);
    }
}
