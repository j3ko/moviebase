﻿using J3ko.Infrastructure.CQL;
using J3ko.Infrastructure.Exceptions;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.SignalR
{
    public class SignalREventListener : ICommandListener
    {
        private string _hubName;

        public SignalREventListener(string hubName)
        {
            _hubName = hubName;
        }

        public void OnCompleted(ICommandEvent value)
        {
            EmitPrivate(value, string.Join(".complete.", value.CommandName, value.CommandId.ToString()), value.CommandId.ToString());
        }

        public void OnError(Exception error)
        {
            HandlerException ex = error as HandlerException;

            if (ex != null && ex.Command != null)
            {
                CommandErrorEvent value = new CommandErrorEvent(
                    commandId: ex.Command.ContainsKey("CommandId") ? new Guid(ex.Command["CommandId"].ToString()) : Guid.Empty,
                    commandName: ex.Command.ContainsKey("CommandName") ? ex.Command["CommandName"].ToString() : "unknown command",
                    message: "Error executing handler");

                EmitPrivate(value, string.Join(".error.", value.CommandName, value.CommandId.ToString()), value.CommandId.ToString());
            }
            else
            {
                throw new ArgumentException("This shouldn't happen");
            }
        }

        public void OnNext(ICommandEvent value)
        {
            EmitPrivate(value, string.Join(".", value.CommandName, value.CommandId.ToString()), value.CommandId.ToString());

            if (value.IsGlobal)
            {
                EmitPublic(value, value.CommandName, ApiEventHub.PUBLIC_NAMESPACE);
            }
        }

        private void EmitPrivate(ICommandEvent value, string eventName, string room)
        {
            try
            {
                IHubContext hub = GlobalHost.ConnectionManager.GetHubContext(_hubName);
                hub.Clients.Group(room).privateEvent(eventName, value);
            }
            catch (InvalidOperationException)
            {
                Trace.TraceWarning("SignalR Hub not found: {0}", _hubName);
            }
        }

        private void EmitPublic(ICommandEvent value, string eventName, string room)
        {
            try
            {
                IHubContext hub = GlobalHost.ConnectionManager.GetHubContext(_hubName);
                hub.Clients.Group(room).publicEvent(eventName, value);
            }
            catch (InvalidOperationException)
            {
                Trace.TraceWarning("SignalR Hub not found: {0}", _hubName);
            }
        }
    }
}
