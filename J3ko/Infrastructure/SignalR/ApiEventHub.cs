﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.SignalR
{
    public abstract class ApiEventHub : Hub, IApiEventHub
    {
        public static string PUBLIC_NAMESPACE { get { return "public"; } }

        public Task Subscribe(string room)
        {
            return Groups.Add(Context.ConnectionId, room);
        }

        public Task UnSubscribe(string room)
        {
            return Groups.Add(Context.ConnectionId, room);
        }
    }
}
