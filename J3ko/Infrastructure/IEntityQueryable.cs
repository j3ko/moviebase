﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure
{
    public interface IEntityQueryable<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause, Expression<Func<TEntity, bool>> orderClause);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause, Expression<Func<TEntity, bool>> orderClause, int page, int pageSize);
        TEntity FindSingle(Expression<Func<TEntity, bool>> whereClause);
    }
}
