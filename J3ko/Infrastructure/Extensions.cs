﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace J3ko.Infrastructure
{
    // The purpose of this class is to decouple System.Data.Entity from IQueryable<> so that we can mock IDomainContext
    // without database dependencies.
    public static class Extensions
    {
        public static IIncluder Includer = new NullIncluder();

        public static IQueryable<T> Include<T, TProperty>(this IQueryable<T> source, Expression<Func<T, TProperty>> path)
            where T : class
        {
            return Includer.Include(source, path);
        }

        public interface IIncluder
        {
            IQueryable<T> Include<T, TProperty>(IQueryable<T> source, Expression<Func<T, TProperty>> path) where T : class;
        }

        internal class NullIncluder : IIncluder
        {
            public IQueryable<T> Include<T, TProperty>(IQueryable<T> source, Expression<Func<T, TProperty>> path) where T : class
            {
                return source;
            }
        }
    }
}
