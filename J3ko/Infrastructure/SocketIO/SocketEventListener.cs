﻿using J3ko.Infrastructure.CQL;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.SocketIO
{
    public class SocketEventListener : ICommandListener
    {
        private const string GLOBAL_NAMESPACE = "global";

        private string _baseUrl;
        private IRestClient _client;
        public SocketEventListener(string baseUrl)
        {
            _baseUrl = baseUrl;
            _client = new RestClient(_baseUrl);
        }
        public void OnCompleted(ICommandEvent value)
        {
            Debug.WriteLine("on complete");
        }

        public void OnError(Exception error)
        {
            Debug.WriteLine("on error");
        }

        public void OnNext(ICommandEvent value)
        {
            Emit(value, string.Join(".", value.CommandName, value.CommandId.ToString()), value.CommandId.ToString());

            if (value.IsGlobal)
                Emit(value, value.CommandName, GLOBAL_NAMESPACE);
        }

        private void Emit(ICommandEvent value, string eventName, string room)
        {
            IRestRequest request = new RestRequest("socket/{eventname}/emit/{room}", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddUrlSegment("room", room);
            request.AddUrlSegment("eventname", eventName);
            request.AddBody(value);
            //string json = request.JsonSerializer.Serialize(data);
            //request.AddParameter("application/json", json, ParameterType.RequestBody);

            IRestResponse response = _client.Execute(request);
        }


    }
}
