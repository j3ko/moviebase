﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace J3ko.Infrastructure.MVC
{
    public class NetworkVirtualFile : VirtualFile
    {
        private NetworkVirtualPathProvider _provider;
        private string _absPath;

        public NetworkVirtualFile(string absPath, NetworkVirtualPathProvider provider)
            : base(absPath)
        {
            _provider = provider;
            _absPath = absPath;
        }
        
        public bool Exists
        {
            get 
            { 
                return (File.Exists(_absPath)); 
            }
        }

        public override Stream Open()
        {
           return File.Open(_absPath, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
        }
    }
}
