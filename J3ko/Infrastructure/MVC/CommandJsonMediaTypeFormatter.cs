﻿using J3ko.Infrastructure.CQL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.MVC
{
    public class CommandJsonMediaTypeFormatter : JsonMediaTypeFormatter
    {
        public override Task<object> ReadFromStreamAsync(Type type, System.IO.Stream stream, System.Net.Http.HttpContent content, IFormatterLogger formatterLogger)
        {
            HttpContentHeaders contentHeaders = content == null ? null : content.Headers;

            // If content length is 0 then return default value for this type
            if (type != typeof(ICommand) || contentHeaders != null && contentHeaders.ContentLength == 0)
                return base.ReadFromStreamAsync(type, stream, content, formatterLogger);
            
            // Create a serializer
            var serializer = JsonSerializer.Create(SerializerSettings);

            // Create task reading the content
            return Task.Factory.StartNew(
                () =>
                {
                    using (var streamReader = new StreamReader(stream, SelectCharacterEncoding(contentHeaders)))
                    using (var jsonTextReader = new JsonTextReader(streamReader))
                    {
                        JObject jobj = serializer.Deserialize(jsonTextReader) as JObject;

                        if (jobj == null)
                            return null;

                        string commandName = (jobj["CommandName"] ?? string.Empty).ToString();
                        Type comType = CommandContext.GetCommandType(commandName);

                        if (comType != null)
                        {
                            object result = jobj.ToObject(comType, serializer);
                            CommandContext.InitializeCommand(result as ICommand);
                            return result;
                        }

                        return null;
                    }
                });
        }
    }
}
