﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;
using System.Web.Http.OData.Extensions;

namespace J3ko.Infrastructure.MVC
{
    public abstract class BaseDomainController<TDomain> : ApiController, IDomainController<TDomain>
        where TDomain : IDisposable
    {
        private bool _disposed = false;

        public BaseDomainController(TDomain domainModel)
        {
            DomainModel = domainModel;
        }
        public TDomain DomainModel { get; private set; }

        /// <summary>
        /// Retrieve the assembler for Type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected IAssembler<T, TDomain> Assembler<T>() where T : class, IDto
        {
            Type assemblerType = CommandContext.GetAssemblerType(typeof(T));
            IAssembler<T, TDomain> assInst = Activator.CreateInstance(assemblerType, DomainModel) as IAssembler<T, TDomain>;
            return assInst;
        }
        /// <summary>
        /// Query an Assembler of type T.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IQueryable<T> Query<T>() where T : class, IDto
        {
            IAssembler<T, TDomain> assembler = Assembler<T>();

            IQueryable<T> entities = assembler.GetAll();
            return entities;
        }

        public PageResult Assemble<T>(ODataQueryOptions<T> options) where T : class, IDto
        {
            return Assemble<T>(Query<T>(), options);
        }
        public PageResult Assemble<T>(IQueryable<T> entities, ODataQueryOptions<T> options)
        {
            Type t = entities.Provider.GetType();
            if (t.Name.Equals("MongoQueryProvider", StringComparison.InvariantCultureIgnoreCase)) 
            {
                // TODO: fix this hack
                var hack = entities.ToList().AsQueryable();
                //string query = HttpUtility.UrlDecode(Request.RequestUri.Query);
                //var hack = LinqToQuerystring.Extensions.LinqToQuerystring(entities, query);
                return CreateQueryablePageResultImpl(Request, (dynamic)options.ApplyTo(hack));
            }
            return CreateQueryablePageResultImpl(Request, (dynamic)options.ApplyTo(entities));
        }
        public SingleResult<T> AssembleSingle<T>(Expression<Func<T, bool>> expression) where T : class, IDto
        {
            T entity = Query<T>().FirstOrDefault(expression);
            return AssembleSingle<T>(entity);
        }
        public SingleResult<T> AssembleSingle<T>(T entity)
        {
            IQueryable<T> entList = new List<T>().AsQueryable();

            if (entity != null)
                entList = new List<T> { entity }.AsQueryable();

            return SingleResult.Create<T>(entList);
        }
        private PageResult CreateQueryablePageResultImpl<T>(HttpRequestMessage request, 
            IQueryable<T> response)
        {
            return new PageResult<T>(
                response as IEnumerable<T>,
                request.ODataProperties().NextLink,
                request.ODataProperties().TotalCount);
        }

        protected override void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (this.DomainModel != null)
                    {
                        this.DomainModel.Dispose();
                        this.DomainModel = default(TDomain);
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                _disposed = true;
            }
        }

    }
}
