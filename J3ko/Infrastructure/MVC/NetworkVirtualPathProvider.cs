﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Hosting;

namespace J3ko.Infrastructure.MVC
{
    public class NetworkVirtualPathProvider : VirtualPathProvider
    {
        private string _folderPath;
        private string _routePath;
        public NetworkVirtualPathProvider(string routePath, string folderPath)
        {
            _routePath = routePath;
            _folderPath = folderPath;
        }

        private bool IsPathVirtual(string virtualPath)
        {
            string checkPath = VirtualPathUtility.ToAppRelative(virtualPath);
            return checkPath.StartsWith(_routePath, StringComparison.InvariantCultureIgnoreCase);
        }

        private string ToNetworkPath(string virtualPath)
        {
            string checkPath = VirtualPathUtility.ToAppRelative(virtualPath);
            string absPath = Regex.Replace(checkPath, _routePath, _folderPath, RegexOptions.IgnoreCase);
            return absPath.Replace(@"/", @"\");
        }

        public override bool FileExists(string virtualPath)
        {
            if (IsPathVirtual(virtualPath))
            {
                NetworkVirtualFile file = (NetworkVirtualFile)GetFile(virtualPath);
                return file.Exists;
            }
            else 
            {
                return Previous.FileExists(virtualPath);
            }
        }

        public override VirtualFile GetFile(string virtualPath)
        {
            if (IsPathVirtual(virtualPath))
            {
                return new NetworkVirtualFile(ToNetworkPath(virtualPath), this);
            }
            else
            {
                return Previous.GetFile(virtualPath);
            }
        }

        public override CacheDependency GetCacheDependency(string virtualPath, IEnumerable virtualPathDependencies, DateTime utcStart)
        {
            if (IsPathVirtual(virtualPath))
            {
                return null;
            }
            else
            {
                return Previous.GetCacheDependency(virtualPath, virtualPathDependencies, utcStart);
            }
        }
    }
}
