﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.MVC
{
    public interface IDomainController<TDomain> 
        where TDomain : IDisposable
    {
        TDomain DomainModel { get; }
    }
}
