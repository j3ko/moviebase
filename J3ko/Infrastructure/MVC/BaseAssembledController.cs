﻿using J3ko.Infrastructure.CQL;
using J3ko.Infrastructure.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace J3ko.Infrastructure.MVC
{
    public abstract class BaseAssembledController<TEntity, TDomain> : BaseDomainController<TDomain>
        where TEntity : class, IDto
        where TDomain : IDisposable
    {
        public BaseAssembledController(TDomain domainModel)
            : base(domainModel)
        {
        }

        public PageResult Get(ODataQueryOptions<TEntity> options)
        {
            return Assemble<TEntity>(options);
        }
    }
}
