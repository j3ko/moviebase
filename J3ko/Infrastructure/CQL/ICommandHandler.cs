﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.CQL
{
    public interface ICommandHandler : IResponseObservable<ICommandEvent>
    {
        bool IsComposite { get; }
        void Handle(ICommand command);
        void Notify(ICommandEvent cevent);
    }
}
