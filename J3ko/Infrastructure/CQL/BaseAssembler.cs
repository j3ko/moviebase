﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.CQL
{
    public abstract class BaseAssembler<TEntity, TDomain> : IAssembler<TEntity, TDomain> 
        where TEntity : class, IDto
    {
        public BaseAssembler(TDomain domainModel)
        {
            DomainModel = domainModel;
        }

        public TDomain DomainModel { get; private set; }

        public IQueryable<TEntity> GetAll()
        {
            return OnGetAll(DomainModel);
        }

        private IQueryable<TEntity> FindAll()
        {
            return OnFindAll(DomainModel);
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause)
        {
            return FindAll()
                .Where(whereClause)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, System.Linq.Expressions.Expression<Func<TEntity, bool>> orderClause)
        {
            return FindAll()
                .Where(whereClause)
                .OrderBy(orderClause)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, System.Linq.Expressions.Expression<Func<TEntity, bool>> orderClause, int page, int pageSize)
        {
            return FindAll()
                .Where(whereClause)
                .OrderBy(orderClause)
                .Skip(pageSize)
                .Take(page)
                .ToList();
        }

        public TEntity FindSingle(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause)
        {
            return FindAll()
                .SingleOrDefault(whereClause);
        }

        protected abstract IQueryable<TEntity> OnGetAll(TDomain domainModel);

        protected virtual IQueryable<TEntity> OnFindAll(TDomain domainModel)
        {
            return OnGetAll(domainModel);
        }
    
    }
}
