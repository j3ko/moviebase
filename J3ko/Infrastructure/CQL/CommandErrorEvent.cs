﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.CQL
{
    public class CommandErrorEvent : ICommandEvent
    {
        private bool _isGlobal;
        private Guid _commandId;
        private string _commandName;
        private string _message;

        public CommandErrorEvent(Guid commandId, string commandName, string message)
        {
            _isGlobal = false;
            _commandId = commandId;
            _commandName = commandName;
            _message = message;
        }

        public bool IsGlobal { get { return _isGlobal; } set { _isGlobal = value; } }

        public Guid CommandId { get { return _commandId; } set { _commandId = value; } }

        public string CommandName { get { return _commandName; } set { _commandName = value; } }

        public string Message { get { return _message; } }
    }
}
