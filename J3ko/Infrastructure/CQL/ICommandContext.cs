﻿using System;
using System.Threading.Tasks;
namespace J3ko.Infrastructure.CQL
{
    public interface ICommandContext<TDomain> : IResponseObservable<ICommandEvent>, IDisposable
    {
        void Execute(ICommand command);
        Task ExecuteAsync(ICommand command);
        IDisposable Subscribe(IResponseObserver<ICommandEvent> observer);
    }
}
