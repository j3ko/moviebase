﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using J3ko.Common;
using System.Diagnostics;
using System.Collections.Concurrent;

namespace J3ko.Infrastructure.CQL
{
    #region Static Methods
    public static class CommandContext
    {
        private static ConcurrentDictionary<string, Type> _commands = new ConcurrentDictionary<string, Type>();
        private static ConcurrentDictionary<Type, Type> _handlers = new ConcurrentDictionary<Type, Type>();
        private static ConcurrentDictionary<Type, Type> _assemblers = new ConcurrentDictionary<Type, Type>();

        public static void Initialize(params Type[] configTypes)
        {
            RegisterCommands(configTypes);
            RegisterHandlers(configTypes);
            RegisterAssemblers(configTypes);
        }
        private static void RegisterCommands(params Type[] configTypes)
        {
            IEnumerable<Type> typesToRegister = new List<Type>();

            foreach (Type example in configTypes)
            {
                IEnumerable<Type> temp = System.Reflection.Assembly.GetAssembly(example).GetTypes()
                 .Where(type => type.GetInterfaces().Contains(typeof(ICommand))
                     && type.Namespace.Equals(example.Namespace, StringComparison.Ordinal));

                typesToRegister = typesToRegister.Union(temp);
            }

            foreach (Type configurationType in typesToRegister)
            {
                ICommand configurationInstance = Activator.CreateInstance(configurationType) as ICommand;
                _commands.TryAdd(configurationInstance.CommandName, configurationType);
            }
        }
        private static void RegisterHandlers(params Type[] configTypes)
        {
            IEnumerable<Tuple<Type, Type>> typesToRegister = new List<Tuple<Type, Type>>();

            foreach (Type example in configTypes)
            {
                IEnumerable<Tuple<Type, Type>> temp = System.Reflection.Assembly.GetAssembly(example).GetTypes()
                 .Where(type => type.InheritsFrom(typeof(BaseCommandHandler<,>))
                     && type.Namespace.Equals(example.Namespace, StringComparison.Ordinal))
                 .Select(x => new Tuple<Type, Type>(x.GetParentTypes().Where(
                     y => y.IsGenericType &&
                          y.GetGenericTypeDefinition() == (typeof(BaseCommandHandler<,>))).First().GetGenericArguments()[0], x));

                typesToRegister = typesToRegister.Union(temp);
            }

            foreach (Tuple<Type, Type> configurationType in typesToRegister)
            {
                _handlers.TryAdd(configurationType.Item1, configurationType.Item2);
            }
        }
        private static void RegisterAssemblers(params Type[] configTypes)
        {
            IEnumerable<Tuple<Type, Type>> typesToRegister = new List<Tuple<Type, Type>>();

            foreach (Type example in configTypes)
            {
                IEnumerable<Tuple<Type, Type>> temp = System.Reflection.Assembly.GetAssembly(example).GetTypes()
                 .Where(type => type.InheritsFrom(typeof(IAssembler<,>))
                     && type.Namespace.Equals(example.Namespace, StringComparison.Ordinal))
                 .Select(x => new Tuple<Type, Type>(x.GetParentTypes().Where(
                     y => y.IsGenericType &&
                          y.GetGenericTypeDefinition() == (typeof(IAssembler<,>))).First().GetGenericArguments()[0], x));

                typesToRegister = typesToRegister.Union(temp);
            }

            foreach (Tuple<Type, Type> configurationType in typesToRegister)
            {
                _assemblers.TryAdd(configurationType.Item1, configurationType.Item2);
            }
        }

        internal static ICommandHandler CreateHandlerImpl(string commandName, object domain)
        {
            Type command = null;

            if (_commands.ContainsKey(commandName))
                command = _commands[commandName];

            ICommandHandler handler = null;

            if (_handlers.ContainsKey(command))
                handler = Activator.CreateInstance(_handlers[command], domain) as ICommandHandler;

            return handler;
        }
        internal static ICommandHandler CreateHandler(Type command, object domain)
        {
            ICommandHandler handler = null;

            if (_handlers.ContainsKey(command))
                handler = Activator.CreateInstance(_handlers[command], domain) as ICommandHandler;

            return handler;
        }
        public static ICommand CreateCommand(string commandName)
        {
            Type commandType = null;

            if (_commands.ContainsKey(commandName))
                commandType = _commands[commandName];

            ICommand command = null;

            if (commandType != null)
                command = Activator.CreateInstance(commandType) as ICommand;

            InitializeCommand(command);

            return command;
        }
        public static Type GetCommandType(string identifier)
        {
            Type result = null;

            if (_commands.ContainsKey(identifier))
                result = _commands[identifier];

            return result;
        }
        public static Type GetAssemblerType(Type dtoType)
        {
            Type result = null;

            if (_assemblers.ContainsKey(dtoType))
                result = _assemblers[dtoType];

            return result;
        }
        public static void InitializeCommand(ICommand command)
        {
            if (command != null && command.CommandId == Guid.Empty)
                command.CommandId = Guid.NewGuid();
        }
    }
    #endregion

    public class CommandContext<TDomain> : ICommandContext<TDomain>
    {
        private TDomain _domain;
        private List<IResponseObserver<ICommandEvent>> _observers;
        public CommandContext(TDomain domain)
        {
            _observers = new List<IResponseObserver<ICommandEvent>>();
            _domain = domain;
        }
        public async Task ExecuteAsync(ICommand command)
        {
            ICommandHandler handler = CreateHandler(command.CommandName);

            await Task.Factory.StartNew(() => handler.Handle(command));
        }
        public void Execute(ICommand command)
        {
            ICommandHandler handler = CreateHandler(command.CommandName);
            handler.Handle(command);
        }
        public ICommandHandler CreateHandler(string commandName)
        {
            ICommandHandler handler = CommandContext.CreateHandlerImpl(commandName, _domain);

            foreach (IResponseObserver<ICommandEvent> obs in _observers)
            {
                handler.Subscribe(obs);
            }

            return handler;
        }
        public IDisposable Subscribe(IResponseObserver<ICommandEvent> observer)
        {
            if (!_observers.Contains(observer))
                _observers.Add(observer);

            return new Unsubscriber(_observers, observer);
        }
        public void Dispose()
        {
            // TODO: dispose?
            Debug.WriteLine("dispose called");
        }

        private class Unsubscriber : IDisposable
        {
            private List<IResponseObserver<ICommandEvent>> _innerObservers;
            private IResponseObserver<ICommandEvent> _innerObserver;

            public Unsubscriber(List<IResponseObserver<ICommandEvent>> observers, IResponseObserver<ICommandEvent> observer)
            {
                this._innerObservers = observers;
                this._innerObserver = observer;
            }

            public void Dispose()
            {
                if (!(_innerObserver == null)) _innerObservers.Remove(_innerObserver);
            }
        }
    }
        
}
