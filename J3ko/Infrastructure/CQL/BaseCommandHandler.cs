﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.CQL
{
    public abstract class BaseCommandHandler<TCommand, TDomain> 
        : ICommandHandler where TCommand : class, ICommand
    {
        List<IResponseObserver<ICommandEvent>> _observers = new List<IResponseObserver<ICommandEvent>>();
        private ICommand _command;

        internal BaseCommandHandler() {}

        public BaseCommandHandler(TDomain domain)
        {
            DomainModel = domain;
        }
        public bool IsComposite { get; internal set; }

        public abstract CommandCompletedEvent CommandResult { get; }

        protected void HandleComposite(ICommand command)
        {
            HandleCompositeImpl((dynamic)command);
        }

        private void HandleCompositeImpl<CType>(CType command)
            where CType : class, ICommand
        {
            BaseCommandHandler<CType, TDomain> chandle = CommandContext.CreateHandler(command.GetType(), DomainModel) as BaseCommandHandler<CType, TDomain>;
            chandle.IsComposite = true;

            foreach (var obs in _observers)
            {
                chandle.Subscribe(obs);
            }

            if (command.CommandId == Guid.Empty)
                command.CommandId = _command.CommandId;

            chandle.Handle(command);
        }

        protected internal TDomain DomainModel { get; private set; }
        
        public virtual void Handle(ICommand command)
        {
            _command = command;

            CommandResult.SetCommand(_command);
            
            try
            {
                OnHandle(command as TCommand);
            }
            catch (Exception ex)
            {
                foreach (var observer in _observers.ToArray())
                {
                    if (observer != null)
                        observer.OnError(ex);
                }

                throw;
            }
            finally
            {
                foreach (var observer in _observers.ToArray())
                {
                    if (observer != null)
                    {
                        observer.OnCompleted(CommandResult);
                    }
                }
            }
        }
        public abstract void OnHandle(TCommand command);

        public void Notify(ICommandEvent cevent)
        {
            foreach (var observer in _observers.ToArray())
            {
                if (observer != null)
                    observer.OnNext(cevent);
            }
        }

        public IDisposable Subscribe(IResponseObserver<ICommandEvent> observer)
        {
            if (!_observers.Contains(observer))
                _observers.Add(observer);
            
            return new Unsubscriber(_observers, observer);
        }

        private class Unsubscriber : IDisposable
        {
            private List<IResponseObserver<ICommandEvent>> _innerObservers;
            private IResponseObserver<ICommandEvent> _innerObserver;

            public Unsubscriber(List<IResponseObserver<ICommandEvent>> observers, IResponseObserver<ICommandEvent> observer)
            {
                this._innerObservers = observers;
                this._innerObserver = observer;
            }

            public void Dispose()
            {
                if (!(_innerObserver == null)) _innerObservers.Remove(_innerObserver);
            }
        }

    }
}
