﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.CQL
{
    public abstract class CommandCompletedEvent : ICommandEvent
    {
        public Guid CommandId { get; set; }

        public string CommandName { get; set; }

        public void SetCommand(ICommand command)
        {
            this.CommandId = command.CommandId;
            this.CommandName = command.CommandName;
        }

        public bool IsGlobal { get { return false; } }
    }
}
