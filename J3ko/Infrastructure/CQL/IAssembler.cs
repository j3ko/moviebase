﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.CQL
{
    public interface IAssembler<TEntity, TDomain> : IEntityQueryable<TEntity> 
        where TEntity : class, IDto
    {
        TDomain DomainModel { get; }
    }
}
