﻿using J3ko.Infrastructure.CQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using J3ko.Common;

namespace J3ko.Infrastructure.Exceptions
{
    [Serializable()]
    public class HandlerException : Exception
    {
        private readonly IDictionary<string, object> _command;

        public HandlerException()
        { }

        public HandlerException(string message)
            : base(message)
        { }

        public HandlerException(string message, Exception innerException)
            : base(message, innerException)
        { }

        public HandlerException(ICommand command)
            : base(string.Format("{0}:({1})", command.CommandName, command.CommandId))
        {
            if (command != null)
            {
                _command = command.ToDictionary();
            }
        }

        public HandlerException(ICommand command, Exception innerException)
            : base(string.Format("{0}:({1})", command.CommandName, command.CommandId), innerException)
        {
            if (command != null)
            {
                _command = command.ToDictionary();
            }
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        protected HandlerException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            this._command = (IDictionary<string, object>)info.GetValue("Command", typeof(IDictionary<string, object>));
        }

        public IDictionary<string, object> Command
        {
            get
            {
                return this._command;
            }
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
            {
                throw new ArgumentNullException("info");
            }

            info.AddValue("Command", this.Command, typeof(IDictionary<string, object>));

            base.GetObjectData(info, context);
        }
    }
}
