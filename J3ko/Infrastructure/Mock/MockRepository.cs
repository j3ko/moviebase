﻿using J3ko.Infrastructure.DAL;
using J3ko.Infrastructure.DAL.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.Mock
{
    public class MockRepository<TEntity> : EfRepository<TEntity>
        where TEntity : class
    {
        public MockRepository(IDbContext context)
            : base(context)
        { }
    }
}
