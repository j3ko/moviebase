﻿using J3ko.Infrastructure.DAL.EntityFramework;
using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Core.Objects;

namespace J3ko.Infrastructure.Mock
{
    public class MockDatabaseContext : EfDatabaseContext
    {
        /// <summary>
        /// For Database First
        /// </summary>
        /// <param name="nameOrConnectionString"></param>
        /// <param name="security"></param>
        public MockDatabaseContext(string nameOrConnectionString, ISecurityContext security)
            : base(Effort.EntityConnectionFactory.CreateTransient("name=" + nameOrConnectionString), true, security)
        {
            FixEfProviderServicesProblem();
            //this.Database.CreateIfNotExists();
        }

        /// <summary>
        /// For Code First
        /// </summary>
        /// <param name="security"></param>
        /// <param name="configTypes"></param>
        public MockDatabaseContext(ISecurityContext security, params Type[] configTypes)
            : base(Effort.DbConnectionFactory.CreateTransient(), true, security, configTypes)
        {
            FixEfProviderServicesProblem();
            this.Database.CreateIfNotExists();
        }


        // http://stackoverflow.com/questions/14033193/entity-framework-provider-type-could-not-be-loaded
        private void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

    }
}
