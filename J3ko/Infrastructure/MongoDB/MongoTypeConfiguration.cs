﻿using MongoDB.Bson.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.DAL.MongoDB
{
    public abstract class MongoTypeConfiguration<TEntity> : BsonClassMap<TEntity>
    {
    }
}
