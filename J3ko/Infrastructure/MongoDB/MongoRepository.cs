﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;
using MongoDB.Driver;
using MongoDB.Bson;

namespace J3ko.Infrastructure.DAL.MongoDB
{
    public class MongoRepository<TEntity> 
        : BaseDbRepository<TEntity, MongoDatabaseContext, MongoWorkUnit<MongoDatabaseContext>> where TEntity : class
    {
        private MongoDatabaseContext _context;
        private string _className;
        public MongoRepository(IDbContext context)
        {
            _context = context as MongoDatabaseContext;
            _className = typeof(TEntity).Name;
        }

        protected override IWorkUnit CreateWorkUnit()
        {
            return _context.CreateWorkUnit();
        }

        protected override MongoDatabaseContext RepositoryContext
        {
            get { return _context; }
        }

        protected override void OnSave(TEntity entity, MongoWorkUnit<MongoDatabaseContext> work)
        {
            work.QueueWork(CommandType.Upsert, _className, typeof(TEntity), entity);
            //work.Context.MongoDatabase.GetCollection<TEntity>(_className).Save(entity);
        }

        protected override void OnDelete(TEntity entity, MongoWorkUnit<MongoDatabaseContext> work)
        {
            work.QueueWork(CommandType.Delete, _className, typeof(TEntity), entity);
            //IMongoQuery query = new QueryDocument(entity.ToBsonDocument());
            //work.Context.MongoDatabase.GetCollection<TEntity>(_className).Remove(query);
        }

        protected override IQueryable<TEntity> OnGetAll(MongoWorkUnit<MongoDatabaseContext> work)
        {
            return RepositoryContext.MongoDatabase.GetCollection<TEntity>(_className).AsQueryable<TEntity>();
        }
    }
}
