﻿using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver.Linq;
using J3ko.Common;
using MongoDB.Bson;
using Mongo = MongoDB;
using System.Collections;
using System.Collections.Concurrent;
using MongoDB.Driver.Builders;
using System.Reflection;

namespace J3ko.Infrastructure.DAL.MongoDB
{
    public class MongoDatabaseContext : IDbContext, IDisposable
    {
        private Type[] _exampleConfigTypes;
        private IWorkUnit _work;
        private MongoServer _server;
        private MongoDatabase _database;
        private bool _isDisposed;
        private static volatile bool _isconfigured;
        private static ConcurrentDictionary<Type, BsonClassMap> _classMaps = new ConcurrentDictionary<Type, BsonClassMap>();

        public MongoDatabaseContext(string connectionStringOrName, params Type[] configTypes)
        {
            string connection = connectionStringOrName;
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[connectionStringOrName];

            if (settings != null)
                connection = settings.ConnectionString;

            MongoClient client = new MongoClient(connection);
            _server = client.GetServer();
            _database = _server.GetDatabase(new MongoUrl(connection).DatabaseName);

            _exampleConfigTypes = configTypes;

            if (!_isconfigured)
                CreateModel();
            _isconfigured = true;
            
        }

        private void CreateModel()
        {
            IEnumerable<Tuple<Type, Type>> typesToRegister = new List<Tuple<Type, Type>>();

            foreach (Type example in _exampleConfigTypes)
            {
                IEnumerable<Tuple<Type, Type>> temp = System.Reflection.Assembly.GetAssembly(example).GetTypes()
                 .Where(type => type.InheritsFrom(typeof(BsonClassMap<>))
                     || type.InheritsFrom(typeof(MongoTypeConfiguration<>))
                     && type.Namespace.Equals(example.Namespace, StringComparison.Ordinal))
                     .Select(x => new Tuple<Type, Type>(x.GetParentTypes().Where(
                         y => y.IsGenericType &&
                              y.GetGenericTypeDefinition() == (typeof(BsonClassMap<>))).First().GetGenericArguments()[0], x));

                typesToRegister = typesToRegister.Union(temp);
            }

            foreach (Tuple<Type, Type> config in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(config.Item2);
                if (!BsonClassMap.IsClassMapRegistered(config.Item2))
                {
                    BsonClassMap.RegisterClassMap(configurationInstance);


                    _classMaps.TryAdd(config.Item1, configurationInstance);
                }
            }
            
        }

        public IWorkUnit CreateWorkUnit()
        {
            if (_work == null)
                _work = new MongoWorkUnit<MongoDatabaseContext>(this);
            return _work;
        }

        public bool HasWork { get { return _work != null; } }

        public void Execute()
        {
            MongoWorkUnit<MongoDatabaseContext> work = _work as MongoWorkUnit<MongoDatabaseContext>;

            if (work == null) return;

            List<Tuple<CommandType, string, Type, object>> buffer = new List<Tuple<CommandType, string, Type, object>>();

            do
            {
                Tuple<CommandType, string, Type, object> next = work.Queue.Dequeue();
                buffer.Add(next);

                if (work.Queue.Count == 0 ||
                    work.Queue.Peek().Item1 != next.Item1 || 
                    work.Queue.Peek().Item2 != next.Item2)
                {
                    ExecuteWork(buffer);
                    buffer.Clear();
                }

            } while (work.Queue.Count > 0);
        }

        private void ExecuteWork(List<Tuple<CommandType, string, Type, object>> workList)
        {
            if (workList == null || workList.Count <= 0) return;

            CommandType cmd = workList.First().Item1;

            if (cmd == CommandType.Upsert)
                ExecuteUpsert(workList);
            else if (cmd == CommandType.Delete)
                ExecuteDelete(workList);
        }

        private void ExecuteUpsert(List<Tuple<CommandType, string, Type, object>> workList)
        {
            Tuple<CommandType, string, Type, object> first = workList.First();

            if (workList.Count > 1)
            {
                // bulk save = delete existing + batch insert
                DeleteBulkImpl(
                    (dynamic)first.Item4, 
                    workList.Select(x => x.Item4).ToList(), 
                    first.Item2);
                SaveBulkImpl(
                    (dynamic)first.Item4, 
                    workList.Select(x => x.Item4).ToList(), 
                    first.Item2);
            }
            else
            {
                SaveSingleImpl((dynamic)first.Item4, first.Item2);
            }
        }

        private void SaveBulkImpl<TEntity>(TEntity first, List<object> entity, string collection)
        {
            List<TEntity> entities = entity.ConvertAll<TEntity>(x => (TEntity)x);
            _database.GetCollection<TEntity>(collection).InsertBatch<TEntity>(entities);
        }


        private void SaveSingleImpl<TEntity>(TEntity entity, string collection)
        {
            _database.GetCollection<TEntity>(collection).Save<TEntity>(entity);
        }

        private void ExecuteDelete(List<Tuple<CommandType, string, Type, object>> workList)
        {
            Tuple<CommandType, string, Type, object> first = workList.First();

            if (workList.Count > 1)
            {
                DeleteBulkImpl(first.Item4, workList.Select(x => x.Item4).ToList(), first.Item2);
            }
            else
            {
                DeleteSingleImpl(first.Item4, first.Item2);
            }
        }

        private void DeleteBulkImpl<TEntity>(TEntity first, List<object> values, string collection)
        {
            BsonClassMap bson = _classMaps[typeof(TEntity)];
            List<IMongoQuery> docs = values.Select(
                x => Query.EQ(bson.IdMemberMap.ElementName, BsonValue.Create(x.GetPropValue(bson.IdMemberMap.MemberName))))
                .ToList();

            IMongoQuery query = Mongo.Driver.Builders.Query.Or(docs);
            _database.GetCollection<TEntity>(collection).Remove(query);
        }


        private void DeleteSingleImpl<TEntity>(TEntity entity, string collection)
        {
            BsonClassMap bson = _classMaps[typeof(TEntity)];
            object obj = entity.GetPropValue(bson.IdMemberMap.MemberName);

            _database.GetCollection<TEntity>(collection).Remove(Query.EQ(bson.IdMemberMap.ElementName, BsonValue.Create(obj)));
        }

        internal MongoDatabase MongoDatabase { get { return _database; } }

        protected void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Code to dispose the managed resources of the class
                    if (this._work != null)
                    {
                        this._work.Dispose();
                        this._work = null;
                    }

                }
            }
            // Code to dispose the un-managed resources of the class

            _isDisposed = true;
        }

        public void Dispose()
        {
            
        }
    }
}
