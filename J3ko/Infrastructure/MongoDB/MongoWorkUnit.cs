﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.DAL.MongoDB
{
    internal enum CommandType
    {
        Upsert,
        Delete
    }
    public class MongoWorkUnit<TContext> : IWorkUnit<TContext>
        where TContext : MongoDatabaseContext
    {
        private Queue<Tuple<CommandType, string, Type, object>> _workQueue;

        public TContext Context { get; set; }

        public MongoWorkUnit(TContext dbContext)
        {
            this._workQueue = new Queue<Tuple<CommandType, string, Type, object>>();
            this.Context = dbContext;
        }

        internal Queue<Tuple<CommandType, string, Type, object>> Queue
        {
            get
            {
                return this._workQueue;
            }
        }

        internal void QueueWork(CommandType cmd, string collection, Type type, object entity)
        {
            _workQueue.Enqueue(new Tuple<CommandType, string, Type, object>(cmd, collection, type, entity));
        }

        public void Commit()
        {
            // TODO: implement mongo transactions
            throw new NotImplementedException();
        }

        public void Rollback()
        {
            // TODO: implement mongo transactions
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            
        }
    }
}
