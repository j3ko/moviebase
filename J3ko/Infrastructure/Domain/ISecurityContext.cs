﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.Domain
{
    public interface ISecurityContext : IDisposable
    {
        string Username { get; }
    }
}
