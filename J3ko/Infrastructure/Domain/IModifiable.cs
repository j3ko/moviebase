﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.Domain
{
    public interface IModifiable
    {
        DateTime Modified { get; set; }
        string ModifiedBy { get; set; }
    }
}
