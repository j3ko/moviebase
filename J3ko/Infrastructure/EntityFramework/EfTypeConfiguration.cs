﻿using J3ko.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.DAL.EntityFramework
{
    public class EfTypeConfiguration<TEntity> : EntityTypeConfiguration<TEntity> where TEntity : class
    {
        public EfTypeConfiguration()
            : this(true)
        {
        }

        public EfTypeConfiguration(bool isConcurrencyToken)
        {
            if (typeof(IConcurrencyControllable).IsAssignableFrom(typeof(TEntity)))
            {
                var parameter = Expression.Parameter(typeof(TEntity), "x");
                var property = Expression.Property(parameter, "RowVersion");
                var lmbd = Expression.Lambda<Func<TEntity, byte[]>>(property, parameter);
                
                Property(lmbd)
                    .IsRowVersion()
                    .IsConcurrencyToken(isConcurrencyToken);
            }
        }
    }
}
