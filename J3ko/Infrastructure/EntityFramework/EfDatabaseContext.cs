﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq.Expressions;
using System.Data.Common;
using J3ko.Infrastructure.Domain;

namespace J3ko.Infrastructure.DAL.EntityFramework
{
    public abstract class EfDatabaseContext : DbContext, IDbContext, IDisposable
    {
        private Type[] _exampleConfigTypes;
        private IWorkUnit _work;
        private bool _isDisposed;
        private ISecurityContext _security;

        public EfDatabaseContext(string nameOrConnectionString, ISecurityContext security, params Type[] configTypes)
            : base(nameOrConnectionString)
        {
            _security = security;

            _exampleConfigTypes = configTypes;

            Extensions.Includer = new DbIncluder();

            Database.SetInitializer<EfDatabaseContext>(null);
        }

        public EfDatabaseContext(DbConnection existingConnection, bool contextOwnsConnection, ISecurityContext security, params Type[] configTypes)
            : base(existingConnection, contextOwnsConnection)
        {
            _security = security;

            _exampleConfigTypes = configTypes;

            Extensions.Includer = new DbIncluder();

            Database.SetInitializer<EfDatabaseContext>(null);
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            IEnumerable<Type> typesToRegister = new List<Type>();

            foreach (Type example in _exampleConfigTypes)
            {
                IEnumerable<Type> temp = System.Reflection.Assembly.GetAssembly(example).GetTypes()
                 .Where(type => type.BaseType != null
                     && type.BaseType.IsGenericType
                     && (type.BaseType.GetGenericTypeDefinition() == typeof(EntityTypeConfiguration<>)
                     || type.BaseType.GetGenericTypeDefinition() == typeof(EfTypeConfiguration<>))
                     && type.Namespace.Equals(example.Namespace, StringComparison.Ordinal));

                typesToRegister = typesToRegister.Union(temp);
            }

            foreach (Type configurationType in typesToRegister)
            {
                dynamic configurationInstance = Activator.CreateInstance(configurationType);
                modelBuilder.Configurations.Add(configurationInstance);
            }

            base.OnModelCreating(modelBuilder);
        }

        public IWorkUnit CreateWorkUnit()
        {
            if (_work == null)
                _work = new EfWorkUnit<EfDatabaseContext>(this, true);
            return _work;
        }

        internal IWorkUnit CreateWorkUnit(bool withTransaction)
        {
            if (_work == null)
                _work = new EfWorkUnit<EfDatabaseContext>(this, withTransaction);
            return _work;
        }

        public bool HasWork { get { return _work != null; } }

        public void Execute()
        {
            base.SaveChanges();
        }

        ~EfDatabaseContext()
        {
            Dispose(false);
        }

        protected override void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    // Code to dispose the managed resources of the class
                    if (this._work != null)
                    {
                        this._work.Dispose();
                        this._work = null;
                    }

                }
            }
            // Code to dispose the un-managed resources of the class

            _isDisposed = true;
        }

        private class DbIncluder : Extensions.IIncluder
        {
            public IQueryable<T> Include<T, TProperty>(IQueryable<T> source, Expression<Func<T, TProperty>> path)
                where T : class
            {
                return QueryableExtensions.Include(source, path);
            }
        }
    }
}
