﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.DAL.EntityFramework
{
    public class EfWorkUnit<TContext> : IWorkUnit<TContext>
        where TContext : DbContext
    {
        private bool _disposed = false;
        private bool _complete = false;
        public TContext Context { get; set; }
        private DbTransaction DbTransaction { get; set; }

        public EfWorkUnit(TContext dbContext, bool withTransaction)
        {
            this.Context = dbContext;

            if (this.Context.Database.Connection.State == System.Data.ConnectionState.Closed)
            {
                this.Context.Database.Connection.Open();
            }

            if (withTransaction)
            {
                this.DbTransaction = this.Context.Database.Connection.BeginTransaction();
                this.Context.Database.UseTransaction(this.DbTransaction);
            }
        }

        public void Commit()
        {
            if (this.DbTransaction != null && !this._complete)
            {
                this.DbTransaction.Commit();
                this._complete = true;
            }
        }

        public void Rollback()
        {
            if (this.DbTransaction != null && !this._complete)
            {
                this.DbTransaction.Rollback();
                this._complete = true;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this._disposed)
            {
                // If disposing equals true, dispose all managed
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    if (this.DbTransaction != null)
                    {
                        if (!this._complete)
                            this.DbTransaction.Commit();
                        this.DbTransaction.Dispose();
                        this.DbTransaction = null;
                    }
                }

                // Call the appropriate methods to clean up
                // unmanaged resources here.
                // If disposing is false,
                // only the following code is executed.

                // Note disposing has been done.
                _disposed = true;

            }
        }
    }
}
