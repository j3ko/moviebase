﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Migrations;

namespace J3ko.Infrastructure.DAL.EntityFramework
{
    public class EfRepository<TEntity> 
        : BaseDbRepository<TEntity, EfDatabaseContext, EfWorkUnit<EfDatabaseContext>> where TEntity : class
    {
        private EfDatabaseContext _context;

        public EfRepository(IDbContext context)
        {
            _context = context as EfDatabaseContext;
        }

        protected override IWorkUnit CreateWorkUnit()
        {
            return _context.CreateWorkUnit(false);
        }

        protected override EfDatabaseContext RepositoryContext
        {
            get { return _context; }
        }

        protected override void OnSave(TEntity entity, EfWorkUnit<EfDatabaseContext> work)
        {
            _context.Set<TEntity>().AddOrUpdate(entity);
        }

        protected override void OnDelete(TEntity entity, EfWorkUnit<EfDatabaseContext> work)
        {
            _context.Set<TEntity>().Remove(entity);
        }

        protected override IQueryable<TEntity> OnGetAll(EfWorkUnit<EfDatabaseContext> work)
        {
            return _context.Set<TEntity>();
        }
    }
}
