﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.DAL
{
    public interface IRepository<TEntity> 
        : IEntityQueryable<TEntity> where TEntity : class
    {
        void Save(TEntity entity);
        void Save(TEntity entity, IWorkUnit workUnit);
        void Delete(TEntity entity);
        void Delete(TEntity entity, IWorkUnit workUnit);
        IQueryable<TEntity> GetAll(IWorkUnit workUnit);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause, IWorkUnit workUnit);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause, Expression<Func<TEntity, bool>> orderClause, IWorkUnit workUnit);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> whereClause, Expression<Func<TEntity, bool>> orderClause, int page, int pageSize, IWorkUnit workUnit);
        TEntity FindSingle(Expression<Func<TEntity, bool>> whereClause, IWorkUnit workUnit);
    }
}
