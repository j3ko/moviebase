﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.DAL
{
    public interface IWorkUnit : IDisposable
    {
        void Commit();
        void Rollback();
    }
    public interface IWorkUnit<TContext> : IWorkUnit
        where TContext : class
    {
        TContext Context { get; }
    }
}
