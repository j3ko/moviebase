﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.DAL
{
    public abstract class BaseDbRepository<TEntity, TContext, TWorkUnit> : IRepository<TEntity>
        where TEntity : class
        where TContext : class
        where TWorkUnit : class, IWorkUnit<TContext>
    {
        #region IRepository<TEntity> Members

        public void Save(TEntity entity)
        {
            TWorkUnit work = CreateWorkUnit() as TWorkUnit;
            Save(entity, work);
        }

        public void Save(TEntity entity, IWorkUnit workUnit)
        {
            TWorkUnit work = workUnit as TWorkUnit;

            try
            {
                if (workUnit == null)
                    throw new ArgumentNullException("workUnit");
                if (work == null)
                    throw new RepositoryException(string.Format("Expecting IWorkUnit of type {0} but {1} was found.", typeof(TWorkUnit).FullName, workUnit.GetType().FullName));

                OnSave(entity, work);
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                work.Rollback();
                throw ex;
            }
        }

        public void Delete(TEntity entity)
        {
            TWorkUnit work = CreateWorkUnit() as TWorkUnit;
            Delete(entity, work);
        }

        public void Delete(TEntity entity, IWorkUnit workUnit)
        {
            TWorkUnit work = workUnit as TWorkUnit;

            try
            {
                if (workUnit == null)
                    throw new ArgumentNullException("workUnit");
                if (work == null)
                    throw new RepositoryException(string.Format("Expecting IWorkUnit of type {0} but {1} was found.", typeof(TWorkUnit).FullName, workUnit.GetType().FullName));

                OnDelete(entity, work);
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                work.Rollback();
                throw ex;
            }
        }

        public IQueryable<TEntity> GetAll()
        {
            TWorkUnit work = CreateWorkUnit() as TWorkUnit;
            return GetAll(work);
        }

        public IQueryable<TEntity> GetAll(IWorkUnit workUnit)
        {
            TWorkUnit work = workUnit as TWorkUnit;

            try
            {
                if (workUnit == null)
                    throw new ArgumentNullException("workUnit");
                if (work == null)
                    throw new RepositoryException(string.Format("Expecting IWorkUnit of type {0} but {1} was found.", typeof(TWorkUnit).FullName, workUnit.GetType().FullName));

                return OnGetAll(work);
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                work.Rollback();
                throw ex;
            }
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause)
        {
            return GetAll()
                .Where(whereClause)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, IWorkUnit workUnit)
        {
            return GetAll(workUnit)
                .Where(whereClause)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, System.Linq.Expressions.Expression<Func<TEntity, bool>> orderClause)
        {
            return GetAll()
                .Where(whereClause)
                .OrderBy(orderClause)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, System.Linq.Expressions.Expression<Func<TEntity, bool>> orderClause, IWorkUnit workUnit)
        {
            return GetAll(workUnit)
                .Where(whereClause)
                .OrderBy(orderClause)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, System.Linq.Expressions.Expression<Func<TEntity, bool>> orderClause, int page, int pageSize)
        {
            return GetAll()
                .Where(whereClause)
                .OrderBy(orderClause)
                .Skip(pageSize)
                .Take(page)
                .ToList();
        }

        public IEnumerable<TEntity> Find(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, System.Linq.Expressions.Expression<Func<TEntity, bool>> orderClause, int page, int pageSize, IWorkUnit workUnit)
        {
            return GetAll(workUnit)
                .Where(whereClause)
                .OrderBy(orderClause)
                .Skip(pageSize)
                .Take(page)
                .ToList();
        }

        public TEntity FindSingle(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause)
        {
            return GetAll()
                .SingleOrDefault(whereClause);
        }

        public TEntity FindSingle(System.Linq.Expressions.Expression<Func<TEntity, bool>> whereClause, IWorkUnit workUnit)
        {
            return GetAll(workUnit)
                .SingleOrDefault(whereClause);
        }
        #endregion

        protected abstract IWorkUnit CreateWorkUnit();

        protected abstract TContext RepositoryContext { get; }

        protected abstract void OnSave(TEntity entity, TWorkUnit work);

        protected abstract void OnDelete(TEntity entity, TWorkUnit work);

        protected abstract IQueryable<TEntity> OnGetAll(TWorkUnit work);
    }
}
