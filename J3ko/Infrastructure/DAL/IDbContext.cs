﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Infrastructure.DAL
{
    public interface IDbContext : IDisposable
    {
        /// <summary>
        /// Creates a Transaction
        /// </summary>
        /// <returns></returns>
        IWorkUnit CreateWorkUnit();
        bool HasWork { get; }
        void Execute();
    }
}
