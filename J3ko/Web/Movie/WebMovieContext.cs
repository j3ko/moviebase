﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Web.Movie
{
    public class WebMovieContext : IWebMovieContext
    {
        private string _tmdbKey;
        public WebMovieContext(string tmdbKey)
        {
            _tmdbKey = tmdbKey;
        }
        public IWebMovieRepository TmdbRepository
        {
            get { return new TmdbRepository(_tmdbKey); }
        }
    }
}
