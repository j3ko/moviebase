﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Web.Movie
{
    public class WebMoviePoster
    {
        public string PosterUrl { get; set; }
        public string SmallPosterUrl { get; set; }
    }
}
