﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Web.Movie
{
    public class WebMovieListing
    {
        public int TmdbId { get; set; }
        public string ImdbId { get; set; }
        public string Title { get; set; }
        public string Plot { get; set; }
        public List<WebMoviePoster> Posters { get; set; }
    }
}
