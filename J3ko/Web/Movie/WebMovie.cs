﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Web.Movie
{
    public class WebMovie
    {
        public int TmdbId { get; set; }
        public string ImdbId { get; set; }
        public string Title { get; set; }
        public string Plot { get; set; }
        public DateTime? Released { get; set; }
        public List<string> AltTitles { get; set; }
        public List<int> Genres { get; set; }
        public List<WebMovieActor> Actors { get; set; }
    }
}
