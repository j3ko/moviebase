﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Web.Movie
{
    public interface IWebMovieRepository
    {
        IEnumerable<WebMovieListing> Search(string term);
        WebMovie Find(string identifier);
    }
}
