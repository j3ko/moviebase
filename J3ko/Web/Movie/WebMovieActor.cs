﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Web.Movie
{
    public enum ActorRole
    {
        Other,
        Actor,
        Director,
        Producer
    }
    public class WebMovieActor
    {
        public int TmdbId { get; set; }
        public ActorRole Role { get; set; }
        public string ThumbnailUrl { get; set; }
        public string CharacterName { get; set; }
        public string RealName { get; set; }
        public int Ordinal { get; set; }
    }
}
