﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMDbLib.Client;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Search;

namespace J3ko.Web.Movie
{
    public class TmdbRepository : IWebMovieRepository
    {
        private string BASE_URL = "http://cf2.imgobject.com/t/p/";
        private string SMALL_POSTER_SIZE = "w154";
        private string POSTER_SIZE = "w342";
        private string PROFILE_SIZE = "w45";

        string _tmdbKey;

        public TmdbRepository(string tmdbKey)
        {
            _tmdbKey = tmdbKey;
        }

        public IEnumerable<WebMovieListing> Search(string term)
        {
            TMDbClient client = new TMDbClient(_tmdbKey);
            SearchContainer<SearchMovie> searchResults = client.SearchMovie(term);

            List<WebMovieListing> result = new List<WebMovieListing>();

            foreach (SearchMovie sr in searchResults.Results)
            {
                TMDbLib.Objects.Movies.Movie movie = client.GetMovie(sr.Id, MovieMethods.Images);

                result.Add(new WebMovieListing
                {
                    TmdbId = sr.Id,
                    Title = sr.Title,
                    Posters = GetPosters(movie),
                    ImdbId = movie.ImdbId,
                    Plot = movie.Overview
                });
            }

            return result;
        }

        private List<WebMoviePoster> GetPosters(TMDbLib.Objects.Movies.Movie movie)
        {
            List<WebMoviePoster> result = new List<WebMoviePoster>();

            foreach (ImageData img in movie.Images.Posters)
            {
                result.Add(new WebMoviePoster
                    {
                        PosterUrl = string.Format("{0}{1}{2}", BASE_URL, POSTER_SIZE, img.FilePath),
                        SmallPosterUrl = string.Format("{0}{1}{2}", BASE_URL, SMALL_POSTER_SIZE, img.FilePath),
                    });
            }

            return result;
        }

        public WebMovie Find(string identifier)
        {
            TMDbClient client = new TMDbClient(_tmdbKey);

            TMDbLib.Objects.Movies.Movie movie = client.GetMovie(identifier, MovieMethods.AlternativeTitles | MovieMethods.Credits);

            return new WebMovie()
            {
                TmdbId = movie.Id,
                ImdbId = movie.ImdbId,
                Title = movie.Title,
                Plot = movie.Overview,
                Released = movie.ReleaseDate,
                AltTitles = movie.AlternativeTitles.Titles.Select(x => x.Title).ToList(),
                Genres = movie.Genres.Select(x => x.Id).ToList(),
                Actors = GetActors(movie)
            };
        }

        private List<WebMovieActor> GetActors(TMDbLib.Objects.Movies.Movie movie)
        {
            List<WebMovieActor> result = new List<WebMovieActor>();
            int order = 0;

            foreach (Cast actor in movie.Credits.Cast.OrderBy(x => x.Order))
            {
                result.Add(new WebMovieActor
                {
                    TmdbId = actor.Id,
                    CharacterName = actor.Character,
                    Ordinal = order++,
                    RealName = actor.Name,
                    Role = ActorRole.Actor,
                    ThumbnailUrl = string.IsNullOrWhiteSpace(actor.ProfilePath) 
                                   ? null 
                                   : string.Format("{0}{1}{2}", BASE_URL, PROFILE_SIZE, actor.ProfilePath),
                });
            }

            foreach (Crew crew in movie.Credits.Crew)
            {
                result.Add(new WebMovieActor
                {
                    TmdbId = crew.Id,
                    Ordinal = order++,
                    RealName = crew.Name,
                    Role = GetRole(crew.Department, crew.Job),
                    ThumbnailUrl = string.IsNullOrWhiteSpace(crew.ProfilePath) 
                                   ? null 
                                   : string.Format("{0}{1}{2}", BASE_URL, PROFILE_SIZE, crew.ProfilePath),
                });
            }

            return result;
        }

        private ActorRole GetRole(string department, string job)
        {
            ActorRole result = ActorRole.Other;

            if (job == null)
                return result;

            switch (job.ToLower())
            {
                case "actor":
                    result = ActorRole.Actor;
                    break;
                case "director":
                    result = ActorRole.Director;
                    break;
                case "producer":
                    result = ActorRole.Producer;
                    break;
                case "executive producer":
                    result = ActorRole.Producer;
                    break;
            }

            return result;
        }

    }
}
