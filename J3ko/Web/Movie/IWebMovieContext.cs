﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Web.Movie
{
    public interface IWebMovieContext
    {
        IWebMovieRepository TmdbRepository { get; }
    }
}
