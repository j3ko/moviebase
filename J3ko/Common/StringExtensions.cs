﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace J3ko.Common
{
    public enum EmailValidationMethod
    {
        /// <summary>
        /// Internal .NET validation.
        /// </summary>
        NET,
        /// <summary>
        /// Relaxed regex validation.
        /// </summary>
        RELAXED,
        /// <summary>
        /// RFC2822 compliance.
        /// </summary>
        RFC2822,
        /// <summary>
        /// RFC2822 compliance without prefixed names (i.e. "Jeffrey Ko" jeffreyk@procom.ca).
        /// </summary>
        RFC2822_OMIT_NAME,
        /// <summary>
        /// RFC2822 compliance without prefixed names (i.e. "Jeffrey Ko" jeffreyk@procom.ca).  Additionally checking valid ICANN TLDs (updated 2/14/2011).
        /// </summary>
        RFC2822_ICANN_DOMAIN,
    }

    public static class StringExtensions
    {
        //private const string _EMAIL_REGEX_RELAXED = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$";
        //private const string _EMAIL_REGEX_RFC2822 = @"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])";
        //private const string _EMAIL_REGEX_RFC2822_OMIT_PREFIX = @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
        //private const string _EMAIL_REGEX_RFC2822_ICANN_DOMAIN = @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|aero|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel)\b";

        private static readonly Dictionary<EmailValidationMethod, string> _EMAIL_REGEX = new Dictionary<EmailValidationMethod, string>()
        {            
            {EmailValidationMethod.RELAXED, @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$"}, // taken from http://www.regular-expressions.info/email.html
            {EmailValidationMethod.RFC2822, @"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|""(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*"")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"}, // taken from http://www.regular-expressions.info/email.html
            //{EmailValidationMethod.RFC2822, @"^((?>[a-zA-Z\d!#$%&'*+\-/=?^_`{|}~]+\x20*|""((?=[\x01-\x7f])[^""\\]|\\[\x01-\x7f])*""\x20*)*(?<angle><))?((?!\.)(?>\.?[a-zA-Z\d!#$%&'*+\-/=?^_`{|}~]+)+|""((?=[\x01-\x7f])[^""\\]|\\[\x01-\x7f])*"")@(((?!-)[a-zA-Z\d\-]+(?<!-)\.)+[a-zA-Z]{2,}|\[(((?(?<!\[)\.)(25[0-5]|2[0-4]\d|[01]?\d?\d)){4}|[a-zA-Z\d\-]*[a-zA-Z\d]:((?=[\x01-\x7f])[^\\\[\]]|\\[\x01-\x7f])+)\])(?(angle)>)$"}, // taken from http://www.regexlib.com
            {EmailValidationMethod.RFC2822_OMIT_NAME, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"}, // taken from http://www.regular-expressions.info/email.html
            {EmailValidationMethod.RFC2822_ICANN_DOMAIN, @"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|aero|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel)\b"}, // taken from http://www.regular-expressions.info/email.html
        };

        public static string NormalizeWhitespace(this string str)
        {
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"[ ]{2,}", options);
            return regex.Replace(str, @" ");
        }

        public static bool IsValidEmail(this string str)
        {
            return str.IsValidEmail(EmailValidationMethod.RFC2822);
        }

        public static bool IsValidEmail(this string str, EmailValidationMethod method)
        {
            if (method == EmailValidationMethod.NET)
                return IsValidNetEmail(str);
            return IsValidRegexEmail(str, GetEmailRegex(method));
        }

        /// <summary>
        /// Damerau-Levenshtein Distance
        /// </summary>
        /// <param name="s"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static int GetDistance(this string s, string t)
        {
            if (s == null || t == null)
            {
                throw new ArgumentException("Strings must not be null");
            }
            int[,] d; // matrix
            int n, m; // length of s, t
            int i, j; // iterates through s, t
            char s_i, t_j; // ith character of s, t
            int cost; // cost

            n = s.Length;
            m = t.Length;
            if (n == 0) return m;
            if (m == 0) return n;
            d = new int[n + 1, m + 1];

            for (i = 0; i <= n; i++)
                d[i, 0] = i;
            for (j = 0; j <= m; j++)
                d[0, j] = j;

            for (i = 1; i <= n; i++)
            {
                s_i = s[i - 1];
                for (j = 1; j <= m; j++)
                {
                    t_j = t[j - 1];

                    if (s_i == t_j)
                        cost = 0;
                    else
                        cost = 1;

                    d[i, j] = min(d[i - 1, j] + 1,
                                  d[i, j - 1] + 1,
                                  d[i - 1, j - 1] + cost);
                    //Damerau-Extension
                    if (i > 2 && j > 2)
                        if (s[i - 1] == t[j - 2] &&
                            s[i - 2] == t[j - 1])
                            d[i, j] = Math.Min(d[i, j], d[i - 2, j - 2] + cost);   // transposition
                }
            }
            return d[n, m];
        }

        public static string Truncate(this string sourceText, string ellipsis, int min, int max)
        {
            if (sourceText == null)
                return null;
            //If text is shorter than preview length
            if (sourceText.Length <= max)
            {
                return sourceText; //@RETURN break out early if too short
            }

            //Grab the char at the last position allowed
            char cutOffChar = sourceText[max];

            int lastPosition = max;

            //While the last char isn't a space, cut back until we hit a space or minimum
            while (cutOffChar != ' ' && lastPosition > min)
            {
                lastPosition--;
                cutOffChar = sourceText[lastPosition];
            }

            //Crop text and add some dots
            string outText = sourceText.Substring(0, lastPosition);
            outText += ellipsis;

            return outText;
        }

        /// <summary>
        /// Takes a URL string and returns a filename
        /// </summary>
        /// <param name="sourceText"></param>
        /// <returns></returns>
        public static string GetFileName(this string sourceText)
        {
            Uri uri = new Uri(sourceText);
            return System.IO.Path.GetFileName(uri.LocalPath);
        }

        public static string GetFileExtension(this string filename)
        {
            return Path.GetExtension(filename);
        }

        #region Private Helpers
        private static int min(int a, int b, int c)
        {
            if (b < a) a = b;
            if (c < a) a = c;
            return a;
        }

        private static string GetEmailRegex(EmailValidationMethod method)
        {
            return _EMAIL_REGEX[method];
        }

        private static bool IsValidRegexEmail(string str, string regex)
        {
            Regex re = new Regex(regex, RegexOptions.IgnoreCase);

            if (!string.IsNullOrWhiteSpace(str))
                if (re.IsMatch(str))
                    return true;
            return false;
        }

        private static bool IsValidNetEmail(string str)
        {
            try
            {
                MailAddress addr = new MailAddress(str);
                return true;
            }
            catch { }
            return false;
        }
        #endregion
    }
}
