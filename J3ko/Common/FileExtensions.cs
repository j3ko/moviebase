﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace J3ko.Common
{
    public static class FileExtensions
    {
        public static byte[] ToByteArray(string filename)
        {
            byte[] result;

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                // Create a byte array of file stream length
                result = new byte[fs.Length];

                //Read block of bytes from stream into the byte array
                fs.Read(result, 0, System.Convert.ToInt32(fs.Length));

                //Close the File Stream
                fs.Close();
            }

            return result; //return the byte data
        }

        [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
        private extern static System.UInt32 FindMimeFromData(
            System.UInt32 pBC,
            [MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
            System.UInt32 cbSize,
            [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
            System.UInt32 dwMimeFlags,
            out System.UInt32 ppwzMimeOut,
            System.UInt32 dwReserverd
        );

        public static string GetMime(this Stream stream)
        {

            using (stream)
            {
                byte[] buffer = new byte[256];

                if (stream.Length >= 256)
                    stream.Read(buffer, 0, 256);
                else
                    stream.Read(buffer, 0, (int)stream.Length);

                try
                {
                    System.UInt32 mimetype;
                    FindMimeFromData(0, null, buffer, 256, null, 0, out mimetype, 0);
                    System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                    string mime = Marshal.PtrToStringUni(mimeTypePtr);
                    Marshal.FreeCoTaskMem(mimeTypePtr);
                    return mime;
                }
                catch
                {
                    //Logger.Log(e);
                    return "unknown/unknown";
                }
            }
        }

        public static string GetMime(this byte[] bytes)
        {

            byte[] buffer = new byte[256];

            if (bytes.Length >= 256)
                buffer = bytes.Take(256).ToArray();
            else
                buffer = bytes;

            try
            {
                System.UInt32 mimetype;
                FindMimeFromData(0, null, buffer, 256, null, 0, out mimetype, 0);
                System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                string mime = Marshal.PtrToStringUni(mimeTypePtr);
                Marshal.FreeCoTaskMem(mimeTypePtr);
                return mime;
            }
            catch
            {
                //Logger.Log(e);
                return "unknown/unknown";
            }
        }

        public static string GetMime(string filename)
        {
            if (!File.Exists(filename))
                throw new FileNotFoundException(filename + " not found");

            string result = string.Empty;

            using (FileStream fs = new FileStream(filename, FileMode.Open))
            {
                result = GetMime(fs);
            }

            return result;
        }
    }
}
