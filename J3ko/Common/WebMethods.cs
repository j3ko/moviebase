﻿using System;
using System.Net;
using System.IO;
using System.Drawing;

namespace J3ko.Common
{
    public static class WebMethods
    {
        private const string _REQUEST_HEADER_ACCEPT = @"image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/x-ms-application, application/x-ms-xbap, application/vnd.ms-xpsdocument, application/xaml+xml, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*";
        private const string _REQUEST_HEADER_ACCEPT_LANGUAGE = @"en-ca";
        private const string _REQUEST_HEADER_USER_AGENT = @"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET4.0C; .NET4.0E)";
        private const string _REQUEST_HEADER_ACCEPT_ENCODING = @"gzip, deflate";
        private const string _REQUEST_HEADER_CONNECTION = @"Keep-Alive";

        public static byte[] GetImage(string url)
        {
            byte[] result = null;
            HttpWebRequest request = null;
            WebResponse response = null;
            Stream stream = null;

            try
            {
                // Open a connection
                request = (HttpWebRequest)HttpWebRequest.Create(url);

                request.AllowWriteStreamBuffering = true;

                // You can also specify additional header values like the user agent or the referer: (Optional)
                request.UserAgent = _REQUEST_HEADER_USER_AGENT;
                // set timeout for 20 seconds (Optional)
                request.Timeout = 20000;
                request.Accept = _REQUEST_HEADER_ACCEPT;
                request.Method = "GET";

                // Request response:
                response = request.GetResponse();

                // Open data stream:
                stream = response.GetResponseStream();

                //// convert webstream to image
                result = stream.ToByteArray();
            }
            catch
            {
                throw;
            }
            finally
            {
                // Cleanup
                if (response != null)
                    response.Close();
            }

            return result;
        }

    }
}
